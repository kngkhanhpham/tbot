<?php

namespace App;

use App\Btx1;
use App\Btx2;
use App\BtxCalc;
use App\BtxFailOrderLogs;
use App\BtxOrderLogs;
use App\BtxUserSymbols;
use App\User;
use Illuminate\Support\Facades\Log;

class BtxBb
{
    /**
     * @param array $users
     * @param $symbols
     */
    public function handle($users = [], $symbol, $timeFrame, $candle = [])
    {
        if (empty($users) || empty($candle)) {
            return;
        }

        foreach ($users as $user) {

            // // Only run for test
            // if ($user->id != 1) {
            //     continue;
            // }

            $exchange = $user->exchanges[0];
            if ($exchange->status != 1) {
                continue;
            }            

            // Get user's api key
            $apiKey    = $exchange->api_key;
            $apiSecret = $exchange->api_secret;

            // Get user's api key
            $apiKey    = $user->exchanges[0]->api_key;
            $apiSecret = $user->exchanges[0]->api_secret;

            // Init object class
            $btx1 = new Btx1($apiKey, $apiSecret);
            $btx2 = new Btx2($apiKey, $apiSecret);

            // Init data
            $data               = [];
            $data['user_id']    = $user->id;
            $data['symbol']     = $symbol;
            $data['time_frame'] = $timeFrame;        

            // Get user's symbol setting
            $setting = BtxUserSymbols::where(
                [
                    'user_id'    => $data['user_id'],
                    'symbol'     => $symbol,
                    'time_frame' => $timeFrame
                ]
            )->first();

            // // Debug
            // if ($data['user_id'] == 1) {
            //     Log::channel('btx')->debug('setting');
            //     Log::channel('btx')->debug($setting);
            // }          

            if (! is_null($setting) && $setting->status == 1) {
                
                // Set base amount
                $data['base_amount_limit'] = $setting->base_amount_limit;
                $data['base_amount']       = $setting->base_amount;

                $preUserInfoLog = $data['user_id'] . ' : ' . $symbol . ' : ' . $timeFrame;

                $priceOpen    = number_format($candle[0]['O'], 8, '.', '');
                $priceHigh    = number_format($candle[0]['H'], 8, '.', '');
                $priceCurrent = number_format($candle[0]['C'], 8, '.', '');
                $priceLow     = number_format($candle[0]['L'], 8, '.', '');
                $volCurrent   = round($candle[0]['BV'], 1);

                // use BB to check make order
                // Set price rate cond
                $priceLowerRateCond = -5.0;
                $priceUpperRateCond   = 5.0;
                // Calc price rate
                $priceRate = round(($priceCurrent - $priceOpen) / $priceOpen * 100, 2);
                // Calc profit rate
                $profitRate = round(abs($priceRate) / env('BTX_BB_CALC_PROFIT_RATE', 2), 2);

                // Set bb cond list
                $listBbCond = [
                    [
                        'std'         => 5,
                        'profit_rate' => 3.0,
                    ],
                    [
                        'std'         => 4,
                        'profit_rate' => 2.5,
                    ],
                    [
                        'std'         => 3,
                        'profit_rate' => 2.0,
                    ],
                    [
                        'std'         => 2,
                        'profit_rate' => 1.5,
                    ],
                ];

                foreach ($listBbCond as $key => $bbCond) {
                    // Get BB data
                    $bb = BtxCalc::bb($candle, 20, $bbCond['std']);

                    // // Debug
                    // if ($data['user_id'] == 1) {
                    //     Log::channel('btx')->debug('data');
                    //     Log::channel('btx')->debug($data);
                    // }                    

                    // Buy when current price < BB lower then sell
                    // if ($data['user_id'] == 1) {
                    if ($priceCurrent < $bb['lower'] && $priceRate <= $priceLowerRateCond) {
                        // Set buy price
                        $data['buy_price'] = $priceCurrent;
                        // Set profit rate
                        // $data['profit_rate'] = $bbCond['profit_rate'];
                        $data['profit_rate'] = $profitRate;
                        // Set sell price
                        $data['sell_price'] = round($data['buy_price'] + $data['buy_price'] * ($data['profit_rate'] + env('TRADING_FEE', 0.25)) / 100, 8);
                        $data['sell_price'] = number_format($data['sell_price'], 8, '.', '');
                        // Set amount
                        $data['amount'] = round(($data['base_amount'] - ($data['base_amount'] * env('TRADING_FEE', 0.25) / 100)) / $priceCurrent, 8, PHP_ROUND_HALF_DOWN);
                        // Set condition type
                        $data['condition_type'] = 'lower BB' . $bbCond['std'];
                        // Set candle range
                        $data['candle_range'] = 1;
                        // Set price rate
                        $data['price_rate'] = $priceRate;
                        // Set price rate cond
                        $data['price_rate_cond'] = $priceLowerRateCond;
                        // Set status
                        $data['status'] = 1;

                        // Out Info Log
                        $bb['-------------'] = '-------------';
                        $bb['amount']        = $data['amount'];
                        $bb['buy_price']     = $data['buy_price'];
                        $bb['sell_price']    = $data['sell_price'];
                        $bb['profit_rate']   = $data['profit_rate'];
                        $bb['price_rate']    = $data['price_rate'];
                        $bb['price_rate_cond'] = $data['price_rate_cond'];

                        Log::channel('btx')->info($preUserInfoLog . ' : --> BB Lower!!!');
                        Log::channel('btx')->info($bb);
                        $this->buyToSell($btx1, $data);
                        break;
                    }

                    // Sell when current price > BB upper then buy
                    // if ($data['user_id'] == 1) {
                    if ($priceCurrent > $bb['upper'] && $priceRate >= $priceUpperRateCond) {
                        // Set sell price
                        $data['sell_price'] = $priceCurrent;
                        // Set profit rate
                        // $data['profit_rate'] = $bbCond['profit_rate'];
                        $data['profit_rate'] = $profitRate;
                        // Set buy price
                        $data['buy_price'] = round($data['sell_price'] - $data['sell_price'] * ($data['profit_rate'] + env('TRADING_FEE', 0.25)) / 100, 8);
                        $data['buy_price'] = number_format($data['buy_price'], 8, '.', '');
                        // Set amount
                        $data['amount'] = round(($data['base_amount'] - ($data['base_amount'] * 0.3 / 100)) / $priceCurrent, 8, PHP_ROUND_HALF_DOWN);
                        // Set condition type
                        $data['condition_type'] = 'upper BB' . $bbCond['std'];
                        // Set candle range
                        $data['candle_range'] = 1;
                        // Set price rate
                        $data['price_rate'] = $priceRate;
                        // Set price rate cond
                        $data['price_rate_cond'] = $priceUpperRateCond;
                        // Set status
                        $data['status'] = 1;

                        // Out Info Log
                        $bb['-------------'] = '-------------';
                        $bb['amount']        = $data['amount'];
                        $bb['buy_price']     = $data['buy_price'];
                        $bb['sell_price']    = $data['sell_price'];
                        $bb['profit_rate']   = $data['profit_rate'];
                        $bb['price_rate']    = $data['price_rate'];
                        $bb['price_rate_cond'] = $data['price_rate_cond'];

                        Log::channel('btx')->info($preUserInfoLog . ' : --> BB Upper !!!');
                        Log::channel('btx')->info($bb);
                        $this->sellToBuy($btx1, $data);
                        break;
                    }
                }                
            }                    
        }
    }

    /**
     * @param $btx1
     * @param $data
     * @return null
     */
    protected function buyToSell($btx1, $data)
    {
        // Get Balances of user
        $res = $btx1->getBalances();
        if (isset($res['message'])) {
            Log::channel('btx')->info('get balances fail !');
            Log::channel('btx')->info($res);
            return;
        }

        $balances = [];
        foreach ($res as $value) {
            $balances[$value['Currency']] = $value;
        }

        $btxOrderLogs = new BtxOrderLogs();
        $btxFailOrderLogs   = new BtxFailOrderLogs();

        $tmp          = explode('-', $data['symbol']);
        $tCurrency    = $tmp[1];
        $baseCurrency = $tmp[0];

        // Check Balance >= Base Amount Limit
        if (isset($balances[$tCurrency])) {
            if ($balances[$tCurrency]['Balance'] * $data['buy_price'] >= $data['base_amount_limit']) {
                $data['message'] = 'balance amount > base amount limit';
                $btxFailOrderLogs->fill($data);
                $btxFailOrderLogs->save();
                return;
            }
        }

        if (isset($balances[$baseCurrency]['Balance'])) {
            // Check minimum BTC balance
            if ($baseCurrency == 'BTC' && $balances[$baseCurrency]['Balance'] < 0.0006) {
                $data['message'] = 'The minimum order size is 0.0006 BTC';
                $btxFailOrderLogs->fill($data);
                $btxFailOrderLogs->save();
                return;
            }

            // Check minimum USD balance
            if ($baseCurrency == 'USD' && $balances[$baseCurrency]['Balance'] < 20) {
                $data['message'] = 'The minimum order size is 20 USD';
                $btxFailOrderLogs->fill($data);
                $btxFailOrderLogs->save();
                return;
            }

            // Check base_amount > base_amount balance
            if ($balances[$baseCurrency]['Balance'] < $data['base_amount']) {
                $data['base_amount'] = $balances[$baseCurrency]['Balance'];
                // Calc amount again
                $data['amount'] = round(($data['base_amount'] - ($data['base_amount'] * 0.3 / 100)) / $data['buy_price'], 8, PHP_ROUND_HALF_DOWN);
            }
        }

        // Make buy order
        $buyOrder = $btx1->buyLimit($data['symbol'], $data['amount'], $data['buy_price']);
        Log::channel('btx')->info($buyOrder);

        if (isset($buyOrder['uuid'])) {
            // Set buy order uuid
            $data['buy_order_id'] = $buyOrder['uuid'];
            // Save buy order
            $btxOrderLogs->fill($data);
            $btxOrderLogs->save();

            // Check buy order status
            $checkBuyOrder = $btx1->getOrder($buyOrder['uuid']);
            if ($checkBuyOrder['IsOpen']) {
                return;
            }

            // Make sell order
            $sellOrder = $btx1->sellLimit($data['symbol'], $data['amount'], $data['sell_price']);
            Log::channel('btx')->info($sellOrder);

            if (isset($sellOrder['uuid'])) {
                // Set sell order uuid
                $data['sell_order_id'] = $sellOrder['uuid'];
                $btxOrderLogs->fill($data);
                $btxOrderLogs->save();
            }

        } else {
            $data['message'] = $buyOrder['message'];
            $btxFailOrderLogs->fill($data);
            $btxFailOrderLogs->save();
        }
    }

    /**
     * @param $btx1
     * @param $data
     * @return null
     */
    protected function sellToBuy($btx1, $data)
    {
        // Get Balances of user
        $res = $btx1->getBalances();
        if (isset($res['message'])) {
            Log::channel('btx')->info('get balances fail !');
            Log::channel('btx')->info($res);
            return;
        }

        $balances = [];
        foreach ($res as $value) {
            $balances[$value['Currency']] = $value;
        }

        $btxOrderLogs = new BtxOrderLogs();
        $btxFailOrderLogs   = new BtxFailOrderLogs();

        $tmp          = explode('-', $data['symbol']);
        $tCurrency    = $tmp[1];
        $baseCurrency = $tmp[0];

        // Check Currency balance
        if (isset($balances[$tCurrency])) {
            if ($balances[$tCurrency]['Available'] < $data['amount']) {
                if ($balances[$tCurrency]['Available'] * $data['sell_price'] < 0.0006) {
                    $data['message'] = 'The minimum order size is 0.0006 BTC';
                    $btxFailOrderLogs->fill($data);
                    $btxFailOrderLogs->save();
                    return;
                }
                $data['amount'] = $balances[$tCurrency]['Available'];
            }
        } else {
            $data['message'] = 'balances no available !';
            $btxFailOrderLogs->fill($data);
            $btxFailOrderLogs->save();
            return;
        }

        // Make sell order
        $sellOrder = $btx1->sellLimit($data['symbol'], $data['amount'], $data['sell_price']);
        Log::channel('btx')->info($sellOrder);

        if (isset($sellOrder['uuid'])) {
            // Set Save sell order
            $data['sell_order_id'] = $sellOrder['uuid'];
            $btxOrderLogs->fill($data);
            $btxOrderLogs->save();

            // Check sell order status
            $checkSellOrder = $btx1->getOrder($sellOrder['uuid']);
            if ($checkSellOrder['IsOpen']) {
                return;
            }

            // Make buy order
            $buyOrder = $btx1->buyLimit($data['symbol'], $data['amount'], $data['buy_price']);
            Log::channel('btx')->info($buyOrder);

            if (isset($buyOrder['uuid'])) {
                // Set buy order uuid
                $data['buy_order_id'] = $buyOrder['uuid'];
                $btxOrderLogs->fill($data);
                $btxOrderLogs->save();
            }

        } else {
            // Save fail order
            $data['message'] = $sellOrder['message'];
            $btxFailOrderLogs->fill($data);
            $btxFailOrderLogs->save();
        }
    }
}
