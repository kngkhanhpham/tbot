<?php

namespace App;

class Btx2
{
    /**
     * @var string
     */
    // private $baseUrl = 'https://bittrex.com/Api/';
    private $baseUrl = 'https://global.bittrex.com/Api/';
    /**
     * @var string
     */
    private $apiVersion = 'v2.0';
    /**
     * @var string
     */
    private $apiKey = '';
    /**
     * @var string
     */
    private $apiSecret = '';

    /**
     * @param $apiKey
     * @param $apiSecret
     */
    public function __construct($apiKey, $apiSecret)
    {
        $this->apiKey    = $apiKey;
        $this->apiSecret = $apiSecret;
    }

    /**
     * Invoke API
     * @param string $method API method to call
     * @param array $params parameters
     * @param bool $apiKey  use apikey or not
     * @return object
     */
    private function sendRequest($method, $params = [], $auth_request = false)
    {
        $uri = $this->baseUrl . $this->apiVersion . '/' . $method;

        if ($auth_request == true) {
            $params['apikey'] = $this->apiKey;
            $params['nonce']  = time();
        }

        if (!empty($params)) {
            $uri .= '?' . http_build_query($params);
        }

        $sign = hash_hmac('sha512', $uri, $this->apiSecret);

        $ch = curl_init($uri);
        curl_setopt($ch, CURLOPT_HTTPHEADER, ['apisign: ' . $sign]);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 60);

        $result = curl_exec($ch);

        if (curl_errno($ch)) {
            throw new \Exception(curl_error($ch));
        }

        $response = json_decode($result, true);

        if ($response['success'] == false) {
            return ['error' => 1, 'message' => $response['message']];   
        }

        return $response['result'];
    }

    /**
     * Get the open and available trading markets at Bittrex along with other meta data.
     * @return array
     */
    public function getMarkets()
    {
        return $this->sendRequest('pub/markets/GetMarkets');
    }

    /**
     * Get all supported currencies at Bittrex along with other meta data.
     * @return array
     */
    public function getCurrencies()
    {
        return $this->sendRequest('/pub/currencies/GetCurrencies');
    }

    /**
     * Get the current tick values for a market.
     * @param string $marketName     literal for the market (ex: BTC-LTC)
     * @param string $tickInterval   must be in [“oneMin”, “fiveMin”, “thirtyMin”, “hour”, “day”]
     * @return array
     */
    public function getTicks($marketName, $tickInterval = 'thirtyMin')
    {
        $res = $this->sendRequest('pub/market/GetTicks', ['marketName' => $marketName, 'tickInterval' => $tickInterval]);
        // if (isset($res['error']) || empty($res)) {
        //     return [];
        // }

        if (is_array($res)) {
            return array_reverse($res);  
        } else {
            return [];
        }
    }

    /**
     * Get the last 24 hour summary of all active exchanges
     * @return array
     */
    public function getMarketSummaries()
    {
        return $this->sendRequest('/pub/markets/GetMarketSummaries');
    }

    /**
     * Get the last 24 hour summary of all active exchanges
     * @param string $marketName literal for the market (ex: BTC-LTC)
     * @return array
     */
    public function getMarketSummary($marketName)
    {
        return $this->sendRequest('pub/market/GetMarketSummary', ['marketName' => $marketName]);
    }

    /**
     * Get the orderbook for a given market
     * @param string $market  literal for the market (ex: BTC-LTC)
     * @param string $type    "buy", "sell" or "both" to identify the type of orderbook to return
     * @param integer $depth  how deep of an order book to retrieve. Max is 50.
     * @return array
     */
    public function getOrderBook($market, $type, $depth = 20)
    {
        $params = [
            'market' => $market,
            'type'   => $type,
            'depth'  => $depth,
        ];
        return $this->sendRequest('public/getorderbook', $params);
    }

    /**
     * Get the latest trades that have occured for a specific market
     * @param string $market  literal for the market (ex: BTC-LTC)
     * @param integer $count  number of entries to return. Max is 50.
     * @return array
     */
    public function getMarketHistory($marketName, $count = 20)
    {
        $params = [
            'marketName' => $marketName,
            'count'  => $count,
        ];
        return $this->sendRequest('pub/market/GetMarketHistory', $params);
    }

    /**
     * Place a limit buy order in a specific market.
     * Make sure you have the proper permissions set on your API keys for this call to work
     * @param string $market  literal for the market (ex: BTC-LTC)
     * @param float $quantity the amount to purchase
     * @param float $rate     the rate at which to place the order
     * @return array
     */
    public function buyLimit($market, $quantity, $rate)
    {
        $params = [
            'market'   => $market,
            'quantity' => $quantity,
            'rate'     => $rate,
        ];
        return $this->sendRequest('market/buylimit', $params, true);
    }

    /**
     * Place a buy order in a specific market.
     * Make sure you have the proper permissions set on your API keys for this call to work
     * @param string $market  literal for the market (ex: BTC-LTC)
     * @param float $quantity the amount to purchase
     * @return array
     */
    public function buyMarket($market, $quantity)
    {
        $params = [
            'market'   => $market,
            'quantity' => $quantity,
        ];
        return $this->sendRequest('market/buymarket', $params, true);
    }

    /**
     * Place a limit sell order in a specific market.
     * Make sure you have the proper permissions set on your API keys for this call to work
     * @param string $market  literal for the market (ex: BTC-LTC)
     * @param float $quantity the amount to sell
     * @param float $rate     the rate at which to place the order
     * @return array
     */
    public function sellLimit($market, $quantity, $rate)
    {
        $params = [
            'market'   => $market,
            'quantity' => $quantity,
            'rate'     => $rate,
        ];
        return $this->sendRequest('market/selllimit', $params, true);
    }

    /**
     * Place a sell order in a specific market.
     * Make sure you have the proper permissions set on your API keys for this call to work
     * @param string $market  literal for the market (ex: BTC-LTC)
     * @param float $quantity the amount to sell
     * @return array
     */
    public function sellMarket($market, $quantity)
    {
        $params = [
            'market'   => $market,
            'quantity' => $quantity,
        ];
        return $this->sendRequest('market/sellmarket', $params, true);
    }

    /**
     * Cancel a buy or sell order
     * @param string $uuid id of sell or buy order
     * @return array
     */
    public function cancel($uuid)
    {
        $params = ['uuid' => $uuid];
        return $this->sendRequest('market/cancel', $params, true);
    }

    /**
     * Get all orders that you currently have opened. A specific market can be requested
     * @param string $market  literal for the market (ex: BTC-LTC)
     * @return array
     */
    public function getOpenOrders($market = null)
    {
        $params = ['market' => $market];
        return $this->sendRequest('market/getopenorders', $params, true);
    }

    /**
     * Retrieve all balances from your account
     * @return array
     */
    public function getBalances()
    {
        return $this->sendRequest('account/getbalances', [], true);
    }

    /**
     * Retrieve the balance from your account for a specific currency
     * @param string $currency literal for the currency (ex: LTC)
     * @return array
     */
    public function getBalance($currency)
    {
        $params = ['currency' => $currency];
        return $this->sendRequest('account/getbalance', $params, true);
    }

    /**
     * Retrieve or generate an address for a specific currency. If one
     * does not exist, the call will fail and return ADDRESS_GENERATING
     * until one is available.
     * @param string $currency literal for the currency (ex: LTC)
     * @return array
     */
    public function getDepositAddress($currency)
    {
        $params = ['currency' => $currency];
        return $this->sendRequest('account/getdepositaddress', $params, true);
    }

    /**
     * Withdraw funds from your account. note: please account for txfee.
     * @param string $currency  literal for the currency (ex: LTC)
     * @param float $quantity   the quantity of coins to withdraw
     * @param float $address    the address where to send the funds
     * @param float $paymentid  (optional) used for CryptoNotes/BitShareX/Nxt optional field (memo/paymentid)
     * @return array
     */
    public function withdraw($currency, $quantity, $address, $paymentid = null)
    {
        $params = [
            'currency' => $currency,
            'quantity' => $quantity,
            'address'  => $address,
        ];

        if ($paymentid) {
            $params['paymentid'] = $paymentid;
        }

        return $this->sendRequest('account/withdraw', $params, true);
    }

    /**
     * Retrieve a single order by uuid
     * @param string $uuid  the uuid of the buy or sell order
     * @return array
     */
    public function getOrder($uuid)
    {
        $params = ['uuid' => $uuid];
        return $this->sendRequest('account/getorder', $params, true);
    }

    /**
     * Retrieve your order history
     * @param string $market  (optional) a string literal for the market (ie. BTC-LTC). If ommited, will return for all markets
     * @param integer $count  (optional) the number of records to return
     * @return array
     */
    public function getOrderHistory($market = null, $count = null)
    {
        $params = [];

        if ($market) {
            $params['market'] = $market;
        }

        if ($count) {
            $params['count'] = $count;
        }

        return $this->sendRequest('account/getorderhistory', $params, true);
    }

    /**
     * Retrieve your withdrawal history
     * @param string $currency  (optional) a string literal for the currecy (ie. BTC). If omitted, will return for all currencies
     * @param integer $count    (optional) the number of records to return
     * @return array
     */
    public function getWithdrawalHistory($currency = null, $count = null)
    {
        $params = [];

        if ($currency) {
            $params['currency'] = $currency;
        }

        if ($count) {
            $params['count'] = $count;
        }

        return $this->sendRequest('account/getwithdrawalhistory', $params, true);
    }

    /**
     * Retrieve your deposit history
     * @param string $currency  (optional) a string literal for the currecy (ie. BTC). If omitted, will return for all currencies
     * @param integer $count    (optional) the number of records to return
     * @return array
     */
    public function getDepositHistory($currency = null, $count = null)
    {
        $params = [];

        if ($currency) {
            $params['currency'] = $currency;
        }

        if ($count) {
            $params['count'] = $count;
        }

        return $this->sendRequest('account/getdeposithistory', $params, true);
    }

    /**
     * @param $num
     * @param $decimal
     */
    public function num2string($num, $decimal = 8)
    {
        return number_format($num, $decimal, '.', '');
    }        
}
