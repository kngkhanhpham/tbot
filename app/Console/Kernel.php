<?php

namespace App\Console;

use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;

class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
        Commands\Bfx::class,
        Commands\BfxUpdateOrdersV5::class,
        Commands\Btx::class,
        Commands\BtxUpdateOrdersV5::class,
        Commands\DelOrderLogs::class,
        // Commands\StkLogs::class,
    ];

    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {
        // Bfx
        $schedule->command('command:Bfx')->everyMinute();
        $schedule->command('command:BfxUpdateOrdersV5')->everyMinute();
        
        // Btx
        $schedule->command('command:Btx')->everyMinute();
        $schedule->command('command:BtxUpdateOrdersV5')->everyMinute();

        // Del Order Logs
        $schedule->command('command:DelOrderLogs')->daily();

        // Stk Logs
        // $schedule->command('command:StkLogs')->dailyAt('15:00');     
        // $schedule->command('command:StkLogs')->dailyAt('16:00');     
        // $schedule->command('command:StkLogs')->dailyAt('17:00'); 
    }

    /**
     * Register the commands for the application.
     *
     * @return void
     */
    protected function commands()
    {
        $this->load(__DIR__.'/Commands');

        require base_path('routes/console.php');
    }
}
