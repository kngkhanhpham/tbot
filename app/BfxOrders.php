<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BfxOrders extends Model
{
    /**
     * @var string
     */
    protected $table = 'bfx_orders';

    /**
     * @var array
     */
    public static $rules = [
    ];

    /**
     * @var array
     */
    protected $fillable = [
        'id',
        'user_id',
        'symbol',
        'side',
        'order_id',
        'price',
        'amount',
        'status',
        'message',
    ];

}
