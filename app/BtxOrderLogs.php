<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BtxOrderLogs extends Model
{
    /**
     * @var string
     */
    protected $table = 'btx_order_logs';

    /**
     * @var array
     */
    public static $rules = [
    ];

    /**
     * @var array
     */
    protected $fillable = [
        'id',
        'user_id',
        'symbol',
        'time_frame',
        'buy_order_id',
        'buy_price',
        'buy_status',
        'sell_order_id',
        'sell_price',        
        'sell_status',        
        'amount',
        'condition_type',
        'candle_range',
        'profit_rate',
        'vol_current',
        'vol_current_cond',
        'vol_rate',
        'vol_rate_cond',        
        'price_rate',
        'price_rate_cond',
        'status',
        'message',
    ];
}
