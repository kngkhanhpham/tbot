<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BtxOrders extends Model
{
    /**
     * @var string
     */
    protected $table = 'btx_orders';

    /**
     * @var array
     */
    public static $rules = [
    ];

    /**
     * @var array
     */
    protected $fillable = [
        'id',
        'user_id',
        'symbol',
        'side',
        'order_id',
        'price',
        'amount',
        'status',
        'message',
    ];
}
