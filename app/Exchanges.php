<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Exchanges extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'exchanges';

    /**
     * @var array
     */
    public static $rules = [
        'exchange'    => 'required',
        'api_key'     => 'required|string|max:255',
        'api_secret'  => 'required|string|max:255',
        'api_key1'    => 'max:255',
        'api_secret1' => 'max:255',
        'api_key2'    => 'max:255',
        'api_secret2' => 'max:255',
        'api_key3'    => 'max:255',
        'api_secret3' => 'max:255',
        'status'  => 'integer|in:1,2',
    ];

    /**
     * @var array
     */
    protected $fillable = [
        'id',
        'user_id',
        'exchange',
        'api_key',
        'api_secret',
        'api_key1',
        'api_secret1',
        'api_key2',
        'api_secret2',
        'api_key3',
        'api_secret3',
        'status',
    ];
}
