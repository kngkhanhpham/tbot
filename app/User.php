<?php

namespace App;

use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Laravel\Passport\HasApiTokens;

class User extends Authenticatable
{
    use SoftDeletes, HasApiTokens, Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * Get user's exchanges.
     */
    public function exchanges()
    {
        return $this->hasMany('App\Exchanges');
    }

    /**
     * Get user's bfx symbols.
     */
    public function bfx_symbols()
    {
        return $this->hasMany('App\BfxUserSymbols');
    }

    /**
     * Get user's btx symbols.
     */
    public function btx_symbols()
    {
        return $this->hasMany('App\BtxUserSymbols');
    }  
}
