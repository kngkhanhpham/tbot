<?php

namespace App;

use App\Btx1;
use App\Btx2;
use App\BtxCalc;
use App\BtxFailOrderLogs;
use App\BtxOrderLogs;
use App\BtxUserSymbols;
use App\User;
use Illuminate\Support\Facades\Log;

class BtxBbV2
{
    /**
     * @param array $users
     * @param $symbols
     */
    public function handle($users = [], $symbol, $timeFrame, $candle = [])
    {
        if (empty($users) || empty($candle)) {
            return;
        }

        // Get open, high, low, current price, vol
        $priceOpen    = number_format($candle[0]['O'], 8, '.', '');
        $priceHigh    = number_format($candle[0]['H'], 8, '.', '');
        $priceCurrent = number_format($candle[0]['C'], 8, '.', '');
        $priceLow     = number_format($candle[0]['L'], 8, '.', '');
        $volCurrent   = round($candle[0]['BV'], 1);

        if ($priceOpen == 0 || $priceCurrent == 0 || $priceHigh == 0) {
            // Log::channel('btx_dev')->info($preUserInfoLog . ' : get price = 0');
            return;
        }         

        // Init data
        $data               = [];
        $data['symbol']     = $symbol;
        $data['time_frame'] = $timeFrame;

        // use BB to check make order
        // Set price upper rate cond
        $priceUpperRateCond = env('BTX_BB_PRICE_UPPER_RATE_COND', 5.0);
        // Set price lower rate cond
        $priceLowerRateCond = env('BTX_BB_PRICE_LOWER_RATE_COND', -5.0);
        // Calc price rate
        $priceRate = round(($priceCurrent - $priceOpen) / $priceOpen * 100, 2);
        // Calc profit rate
        $profitRate = round(abs($priceRate) / env('BTX_BB_CALC_PROFIT_RATE', 2), 2);

        // Set bb cond list
        $listBbCond = [
            [
                'std'         => 5,
                'profit_rate' => 3.0,
            ],
            [
                'std'         => 4,
                'profit_rate' => 2.5,
            ],
            [
                'std'         => 3,
                'profit_rate' => 2.0,
            ],
            [
                'std'         => 2,
                'profit_rate' => 1.5,
            ],
        ]; 

        foreach ($listBbCond as $key => $bbCond) {
            // Get BB data
            $bb = BtxCalc::bb($candle, 20, $bbCond['std']);

            // $bbPrev = [];
            // for ($i = 1; $i <= env('BTX_BB_CHECK_CANDLE_RANGE', 3) ; $i++) { 
            //     $bbPrev[$i] = BtxCalc::bb($candle, 20, 2, $i);
            // }    
            // Log::channel('btx_dev')->debug($bbPrev);               

            // Sell when current price > BB upper then buy
            // if ($symbol == 'BTC-XRP') {
            if ($priceCurrent > $bb['upper']) {
                $onFlg = false;
                // Case 1 : Check price rate (current/open)
                if ($priceRate >= $priceUpperRateCond) {
                    $onFlg = true;
                    // Set candle range
                    $data['candle_range'] = 1;                    
                }

                // Case 2 : Check price rate (current/bb['mid'])
                if ($onFlg == false) {
                    // Set price Upper Rate Cond
                    $priceUpperRateCond = $priceUpperRateCond + 2;                     
                    // Calc price rate
                    $priceRate = round(($priceCurrent - $bb['mid']) / $bb['mid'] * 100, 2);
                    // Calc profit rate
                    $profitRate = round(abs($priceRate) / env('BTX_BB_CALC_PROFIT_RATE', 2), 2);

                    if ($priceRate >= $priceUpperRateCond) {
                        $onFlg = true;
                    }

                    // Set candle range
                    $data['candle_range'] = 0;                    
                }

                // Case 3 : Check price rate (current/open of prev candle)
                if ($onFlg == false) {
                    // Set price Upper Rate Cond
                    $priceUpperRateCond = $priceUpperRateCond + 2;     
                    // Get candle idx               
                    $idx = env('BTX_BB_CHECK_CANDLE_RANGE', 3);
                    $prevCandle = $candle[$idx];
                    // Calc price rate
                    $priceRate = round(($priceCurrent - $prevCandle['C']) / $prevCandle['C'] * 100, 2);
                    // Calc profit rate
                    $profitRate = round(abs($priceRate) / env('BTX_BB_CALC_PROFIT_RATE', 2), 2);

                    if ($priceRate >= $priceUpperRateCond) {
                        $onFlg = true;
                    }

                    // Set candle range
                    $data['candle_range'] = $idx + 1;                    
                }                

                if ($onFlg == true) {
                    // Set current price
                    $data['price_current'] = $priceCurrent;
                    // Set sell price
                    $data['sell_price'] = $priceCurrent;
                    // Set profit rate
                    // $data['profit_rate'] = $bbCond['profit_rate'];
                    $data['profit_rate'] = $profitRate;
                    // Set buy price
                    $data['buy_price'] = round($data['sell_price'] - $data['sell_price'] * ($data['profit_rate'] + env('TRADING_FEE', 0.25)) / 100, 8);
                    $data['buy_price'] = number_format($data['buy_price'], 8, '.', '');
                    // Set condition type
                    $data['condition_type'] = 'upper BB' . $bbCond['std'];
                    // Set price rate
                    $data['price_rate'] = $priceRate;
                    // Set price rate cond
                    $data['price_rate_cond'] = $priceUpperRateCond;
                    // Set status
                    $data['status'] = 1;

                    // Out Info Log
                    Log::channel('btx')->info('--> BB Upper !!!');
                    Log::channel('btx')->info($bb);

                    $this->runUserList($users, $data);
                    break;
                }
            }                       

            // Buy when current price < BB lower then sell
            // if ($symbol == 'BTC-XRP') {
            if ($priceCurrent < $bb['lower']) {
                $onFlg = false;
                // Case 1 : Check price rate (current/open)
                if ($priceRate <= $priceLowerRateCond) {
                    $onFlg = true;
                    // Set candle range
                    $data['candle_range'] = 1;                    
                }

                // Case 2 : Check price rate (current/bb['mid'])
                if ($onFlg == false) {
                    // Set price Lower Rate Cond
                    $priceLowerRateCond = $priceLowerRateCond - 2;
                    // Calc price rate
                    $priceRate = round(($priceCurrent - $bb['mid']) / $bb['mid'] * 100, 2);
                    // Calc profit rate
                    $profitRate = round(abs($priceRate) / env('BTX_BB_CALC_PROFIT_RATE', 2), 2);


                    if ($priceRate <= $priceLowerRateCond) {
                        $onFlg = true;
                    }

                    // Set candle range
                    $data['candle_range'] = 0;                    
                }

                // Case 3 : Check price rate (current/open of prev candle)
                if ($onFlg == false) {
                    // Set price Lower Rate Cond
                    $priceLowerRateCond = $priceLowerRateCond - 2;   
                    // Get candle idx                 
                    $idx = env('BTX_BB_CHECK_CANDLE_RANGE', 3);
                    $prevCandle = $candle[$idx];
                    // Calc price rate
                    $priceRate = round(($priceCurrent - $prevCandle['C']) / $prevCandle['C'] * 100, 2);
                    // Calc profit rate
                    $profitRate = round(abs($priceRate) / env('BTX_BB_CALC_PROFIT_RATE', 2), 2);

                    if ($priceRate <= $priceLowerRateCond) {
                        $onFlg = true;
                    }

                    // Set candle range
                    $data['candle_range'] = $idx + 1;                    
                }    

                if ($onFlg == true) {
                    // Set current price
                    $data['price_current'] = $priceCurrent;                
                    // Set buy price
                    $data['buy_price'] = $priceCurrent;
                    // Set profit rate
                    // $data['profit_rate'] = $bbCond['profit_rate'];
                    $data['profit_rate'] = $profitRate;
                    // Set sell price
                    $data['sell_price'] = round($data['buy_price'] + $data['buy_price'] * ($data['profit_rate'] + env('TRADING_FEE', 0.25)) / 100, 8);
                    $data['sell_price'] = number_format($data['sell_price'], 8, '.', '');
                    // Set condition type
                    $data['condition_type'] = 'lower BB' . $bbCond['std'];
                    // Set price rate
                    $data['price_rate'] = $priceRate;
                    // Set price rate cond
                    $data['price_rate_cond'] = $priceLowerRateCond;
                    // Set status
                    $data['status'] = 1;

                    // Out Info Log
                    Log::channel('btx')->info('--> BB Lower !!!');
                    Log::channel('btx')->info($bb);

                    $this->runUserList($users, $data);
                    break;                    
                }  
            }
        }               
    }

    /**
     * @param $users
     * @param $data
     * @return null
     */
    protected function runUserList($users, $data)
    {
        foreach ($users as $key => $user) {
            // Only run for test
            // if ($user->id != 1) {
            //     continue;
            // }

            $exchange = $user->exchanges[0];
            if ($exchange->status != 1) {
                continue;
            }

            // Get user's api key
            $apiKey    = $exchange->api_key;
            $apiSecret = $exchange->api_secret;

            // Init object class
            $btx1 = new Btx1($apiKey, $apiSecret);

            $data['user_id'] = $user->id;
            // Get user's symbol setting
            $setting = BtxUserSymbols::where(
                [
                    'user_id'    => $data['user_id'],
                    'symbol'     => $data['symbol'],
                    'time_frame' => $data['time_frame']
                ]
            )->first();

            // // Debug
            // if ($data['user_id'] == 1) {
            //     Log::channel('btx_dev')->debug('setting');
            //     Log::channel('btx_dev')->debug($setting);
            // }

            if (! is_null($setting) && $setting->status == 1) {

                $preUserInfoLog = $data['user_id'] . ' : ' . $data['symbol'] . ' : ' . $data['time_frame'];
                // Log::channel('btx_dev')->info($preUserInfoLog);                

                // Set base amount
                $data['base_amount_limit'] = $setting->base_amount_limit;
                $data['base_amount']       = $setting->base_amount;
                // Set amount
                $data['amount'] = round(($data['base_amount'] - ($data['base_amount'] * env('TRADING_FEE', 0.25) / 100)) / $data['price_current'], 8, PHP_ROUND_HALF_DOWN);                     

                Log::channel('btx')->info($preUserInfoLog);

                // Debug
                if ($data['user_id'] == 1) {
                    Log::channel('btx')->debug($data);
                }  

                if (substr($data['condition_type'], 0, 8) == 'upper BB') {
                    // Log::channel('btx_dev')->debug('sellToBuy');
                    $this->sellToBuy($btx1, $data);                                
                } else {
                    // Log::channel('btx_dev')->debug('buyToSell');
                    $this->buyToSell($btx1, $data);        
                }
            }
        }
    }    

    /**
     * @param $btx1
     * @param $data
     * @return null
     */
    protected function buyToSell($btx1, $data)
    {
        // Get Balances of user
        $res = $btx1->getBalances();
        if (isset($res['message'])) {
            Log::channel('btx')->info('get balances fail !');
            Log::channel('btx')->info($res);
            return;
        }

        $balances = [];
        foreach ($res as $value) {
            $balances[$value['Currency']] = $value;
        }

        $btxOrderLogs = new BtxOrderLogs();
        $btxFailOrderLogs   = new BtxFailOrderLogs();

        $tmp          = explode('-', $data['symbol']);
        $tCurrency    = $tmp[1];
        $baseCurrency = $tmp[0];

        // Check Balance >= Base Amount Limit
        if (isset($balances[$tCurrency])) {
            if ($balances[$tCurrency]['Balance'] * $data['buy_price'] >= $data['base_amount_limit']) {
                $data['message'] = 'balance amount > base amount limit';
                $btxFailOrderLogs->fill($data);
                $btxFailOrderLogs->save();
                return;
            }
        }

        if (isset($balances[$baseCurrency]['Balance'])) {
            // Check minimum BTC balance
            if ($baseCurrency == 'BTC' && $balances[$baseCurrency]['Balance'] < env('BTC_ORDER_AMOUNT_MIN', 0.0006)) {
                $data['message'] = 'The minimum order size is ' . env('BTC_ORDER_AMOUNT_MIN', 0.0006) . ' BTC';
                $btxFailOrderLogs->fill($data);
                $btxFailOrderLogs->save();
                return;
            }

            // Check minimum USD balance
            if ($baseCurrency == 'USD' && $balances[$baseCurrency]['Balance'] < env('USD_ORDER_AMOUNT_MIN', 20)) {
                $data['message'] = 'The minimum order size is ' . env('USD_ORDER_AMOUNT_MIN', 20) . ' USD';
                $btxFailOrderLogs->fill($data);
                $btxFailOrderLogs->save();
                return;
            }

            // Check base_amount > base_amount balance
            if ($balances[$baseCurrency]['Balance'] < $data['base_amount']) {
                $data['base_amount'] = $balances[$baseCurrency]['Balance'];
                // Calc amount again
                $data['amount'] = round(($data['base_amount'] - ($data['base_amount'] * 0.3 / 100)) / $data['buy_price'], 8, PHP_ROUND_HALF_DOWN);
            }
        }

        // Make buy order
        $buyOrder = $btx1->buyLimit($data['symbol'], $data['amount'], $data['buy_price']);
        Log::channel('btx')->info($buyOrder);

        if (isset($buyOrder['uuid'])) {
            // Set buy order uuid
            $data['buy_order_id'] = $buyOrder['uuid'];
            // Save buy order
            $btxOrderLogs->fill($data);
            $btxOrderLogs->save();

            // Check buy order status
            $checkBuyOrder = $btx1->getOrder($buyOrder['uuid']);
            if ($checkBuyOrder['IsOpen']) {
                return;
            }

            // Make sell order
            $sellOrder = $btx1->sellLimit($data['symbol'], $data['amount'], $data['sell_price']);
            Log::channel('btx')->info($sellOrder);

            if (isset($sellOrder['uuid'])) {
                // Set sell order uuid
                $data['sell_order_id'] = $sellOrder['uuid'];
                $btxOrderLogs->fill($data);
                $btxOrderLogs->save();
            }

        } else {
            $data['message'] = $buyOrder['message'];
            $btxFailOrderLogs->fill($data);
            $btxFailOrderLogs->save();
        }
    }

    /**
     * @param $btx1
     * @param $data
     * @return null
     */
    protected function sellToBuy($btx1, $data)
    {
        // Get Balances of user
        $res = $btx1->getBalances();
        if (isset($res['message'])) {
            Log::channel('btx')->info('get balances fail !');
            Log::channel('btx')->info($res);
            return;
        }

        $balances = [];
        foreach ($res as $value) {
            $balances[$value['Currency']] = $value;
        }

        $btxOrderLogs = new BtxOrderLogs();
        $btxFailOrderLogs   = new BtxFailOrderLogs();

        $tmp          = explode('-', $data['symbol']);
        $tCurrency    = $tmp[1];
        $baseCurrency = $tmp[0];

        // Check Currency balance
        if (isset($balances[$tCurrency])) {
            if ($balances[$tCurrency]['Available'] < $data['amount']) {
                if ($balances[$tCurrency]['Available'] * $data['sell_price'] < env('BTC_ORDER_AMOUNT_MIN', 0.0006)) {
                    $data['message'] = 'The minimum order size is ' . env('BTC_ORDER_AMOUNT_MIN', 0.0006) . ' BTC';
                    $btxFailOrderLogs->fill($data);
                    $btxFailOrderLogs->save();
                    return;
                }
                $data['amount'] = $balances[$tCurrency]['Available'];
            }
        } else {
            $data['message'] = 'balances no available !';
            $btxFailOrderLogs->fill($data);
            $btxFailOrderLogs->save();
            return;
        }

        // Make sell order
        $sellOrder = $btx1->sellLimit($data['symbol'], $data['amount'], $data['sell_price']);
        Log::channel('btx')->info($sellOrder);

        if (isset($sellOrder['uuid'])) {
            // Set Save sell order
            $data['sell_order_id'] = $sellOrder['uuid'];
            $btxOrderLogs->fill($data);
            $btxOrderLogs->save();

            // Check sell order status
            $checkSellOrder = $btx1->getOrder($sellOrder['uuid']);
            if ($checkSellOrder['IsOpen']) {
                return;
            }

            // Make buy order
            $buyOrder = $btx1->buyLimit($data['symbol'], $data['amount'], $data['buy_price']);
            Log::channel('btx')->info($buyOrder);

            if (isset($buyOrder['uuid'])) {
                // Set buy order uuid
                $data['buy_order_id'] = $buyOrder['uuid'];
                $btxOrderLogs->fill($data);
                $btxOrderLogs->save();
            }

        } else {
            // Save fail order
            $data['message'] = $sellOrder['message'];
            $btxFailOrderLogs->fill($data);
            $btxFailOrderLogs->save();
        }
    }
}
