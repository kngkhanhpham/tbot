<?php

namespace App;

use App\Btx1;
use App\Btx2;
use App\BtxCalc;
use App\BtxFailOrderLogs;
use App\BtxOrderLogs;
use App\BtxUserSymbols;
use App\User;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Log;

class BtxBbV5
{
    /**
     * @param array $users
     * @param $symbols
     */
    public function handle($users = [], $symbol, $timeFrame, $candle = [])
    {
        if (empty($users) || empty($candle)) {
            return;
        }

        // Get open, high, low, current price, vol
        $priceOpen    = number_format($candle[0]['O'], 8, '.', '');
        $priceHigh    = number_format($candle[0]['H'], 8, '.', '');
        $priceCurrent = number_format($candle[0]['C'], 8, '.', '');
        $priceLow     = number_format($candle[0]['L'], 8, '.', '');
        $volCurrent   = round($candle[0]['BV'], 1);

        if ($priceOpen == 0 || $priceCurrent == 0 || $priceHigh == 0) {
            // Log::channel('btx')->info($preUserInfoLog . ' : get price = 0');
            return;
        }         

        // Init data
        $data               = [];
        $data['symbol']     = $symbol;
        $data['time_frame'] = $timeFrame;

        $symbolList = BtxUserSymbols::getList(1);

        if (! isset($symbolList[$symbol][$timeFrame])) {
            return;
        }

        $optionSetting = $symbolList[$symbol][$timeFrame]['options'];
        
        // if ($symbol == 'BTC-RVN') {
        //     \Log::channel('btx')->info($optionSetting);
        // }

        // use BB to check make order
        // Set upper rate cond
        $upperPriceRateCond1 = $optionSetting['opt_bb_upper_price_rate_cond1'];
        $upperPriceRateCond2 = $optionSetting['opt_bb_upper_price_rate_cond2'];
        // Set lower rate cond
        $lowerPriceRateCond1 = $optionSetting['opt_bb_lower_price_rate_cond1'];
        $lowerPriceRateCond2 = $optionSetting['opt_bb_lower_price_rate_cond2'];
        // Set min profit rate
        $data['upper_min_profit_rate_cond'] = $optionSetting['opt_bb_upper_min_profit_rate_cond'];
        $data['lower_min_profit_rate_cond'] = $optionSetting['opt_bb_lower_min_profit_rate_cond'];

        // Check buy/sell available
        // Set sell/buy price
        $data['sell_price'] = number_format($priceCurrent, 8, '.', '');
        $data['buy_price']  = number_format($priceCurrent, 8, '.', '');
        $this->runUserList2($users, $data);

        // Get BB data
        $bb = BtxCalc::bb($candle, 20, 2);            

        // Sell when current price > BB upper then buy
        $keyST = $symbol. '_' . $timeFrame . '_upper' ;
        if ($optionSetting['opt_bb_upper_status'] == 1 && ($priceCurrent > $bb['upper'] || Cache::has($keyST))) {
            $onFlg = false;

            $cachePrices = [];
            if (Cache::has($keyST)) {
                $cachePrices = Cache::get($keyST);
                if (!empty($cachePrices)) {
                    $cachePrices = json_decode($cachePrices, true);
                    if (!empty($cachePrices)) {
                        if ($cachePrices[0] < $priceCurrent) {
                            $priceStop = round($priceCurrent - $priceCurrent * env('PRICE_STOP_PERCENT', 2) / 100, 8);
                            if ($priceStop > $cachePrices[0]) {
                                $cachePrices = array_merge([$priceStop], $cachePrices);
                                Cache::put($keyST, json_encode($cachePrices), 172800);
                                Log::channel('btx')->info($keyST . ' : ' . Cache::get($keyST));
                            }
                            return;
                        } else {
                            // Set price current
                            $priceCurrent = $cachePrices[0];
                            // Calc price rate
                            $priceRate = round(($priceCurrent - $bb['mid']) / $bb['mid'] * 100, 2);
                            
                            Log::channel('btx')->info($keyST . ' rate : ' . $priceRate . '/' .  $upperPriceRateCond2);
                            
                            if ($priceRate >= $upperPriceRateCond2) {
                                $onFlg = true;
                            }                   

                            Cache::forget($keyST);
                        }
                    }
                } else {
                    Cache::forget($keyST);
                }
            }

            // Check price rate (current/bb['mid'])
            if ($onFlg == false) {
                // Calc price rate
                $priceRate = round(($priceCurrent - $bb['mid']) / $bb['mid'] * 100, 2);
                if ($priceRate >= $upperPriceRateCond1) {
                    if (empty($cachePrices)) {
                        $priceStop = round($priceCurrent - $priceCurrent * env('PRICE_STOP_PERCENT', 2) / 100, 8);
                        if ($priceStop > $bb['mid']) {
                            $cachePrices = [$priceStop, $bb['mid']];
                        } else {
                            $cachePrices = [$bb['mid']];
                        }                        

                        if (Cache::has($keyST)) {
                            Cache::put($keyST, json_encode($cachePrices), 172800);
                        } else {
                            Cache::add($keyST, json_encode($cachePrices), 172800);
                        }
                        
                        Log::channel('btx')->info($keyST . ' first : ' . Cache::get($keyST));
                        
                        return;
                    }
                }                 
            }
            
            // Match condition
            if ($onFlg == true) {
                // Set current price
                $data['price_current'] = $priceCurrent;
                // Set sell price
                $data['sell_price'] = number_format($priceCurrent, 8, '.', '');
                // Set profit rate
                $data['profit_rate'] = null;
                // Set buy price
                // $data['buy_price'] = round($data['sell_price'] - $data['sell_price'] * ($data['profit_rate'] + env('TRADING_FEE', 0.25)) / 100, 8);
                // $data['buy_price'] = number_format($data['buy_price'], 8, '.', '');
                $data['buy_price'] = null;
                // Set condition type
                $data['condition_type'] = 'upper BB2';
                // Set price rate
                $data['price_rate'] = $priceRate;
                // Set price rate cond
                $data['price_rate_cond'] = $upperPriceRateCond1;
                // Set status
                $data['status'] = 1;

                // Out Info Log
                // Log::channel('btx')->info($bb);

                $this->runUserList($users, $data);
                return;
            }
        }                       

        // Buy when current price < BB lower then sell
        $keyST = $symbol. '_' . $timeFrame . '_lower' ;
        if ($optionSetting['opt_bb_lower_status'] == 1 && ($priceCurrent < $bb['lower'] || Cache::has($keyST))) {
            $onFlg = false;

            $cachePrices = [];
            if (Cache::has($keyST)) {
                $cachePrices = Cache::get($keyST);
                if (!empty($cachePrices)) {
                    $cachePrices = json_decode($cachePrices, true);
                    if (!empty($cachePrices)) {
                        if ($cachePrices[0] > $priceCurrent) {
                            $priceStop = round($priceCurrent + $priceCurrent * env('PRICE_STOP_PERCENT', 2) / 100, 8);
                            if ($priceStop < $cachePrices[0]) {
                                $cachePrices = array_merge([$priceStop], $cachePrices);
                                Cache::put($keyST, json_encode($cachePrices), 172800);
                                Log::channel('btx')->info($keyST . ' : ' . Cache::get($keyST));
                            }
                            return;
                        } else {
                            // Set price current
                            $priceCurrent = $cachePrices[0];                            
                            // Calc price rate
                            $priceRate = round(abs($priceCurrent - $bb['mid']) / $bb['mid'] * 100, 2);
                            
                            Log::channel('btx')->info($keyST . ' rate : ' . $priceRate . '/' .  $lowerPriceRateCond2);

                            if ($priceRate >= $lowerPriceRateCond2) {
                                $onFlg = true;
                            }    
                            Cache::forget($keyST);
                        }
                    }
                } else {
                    Cache::forget($keyST);
                }                
            } 

            // Check price rate (current/bb['mid'])
            if ($onFlg == false) {
                // Calc price rate
                $priceRate = round(abs($priceCurrent - $bb['mid']) / $bb['mid'] * 100, 2);
                if ($priceRate >= $lowerPriceRateCond1) {
                    if (empty($cachePrices)) {                        
                        $priceStop = round($priceCurrent + $priceCurrent * env('PRICE_STOP_PERCENT', 2) / 100, 8);
                        if ($priceStop < $bb['mid']) {
                            $cachePrices = [$priceStop, $bb['mid']];
                        } else {
                            $cachePrices = [$bb['mid']];
                        }

                        if (Cache::has($keyST)) {
                            Cache::put($keyST, json_encode($cachePrices), 172800);
                        } else {
                            Cache::add($keyST, json_encode($cachePrices), 172800);
                        }

                        Log::channel('btx')->info($keyST . ' first : ' . Cache::get($keyST));

                        return;
                    }                    
                }
            }

            // Match condition
            if ($onFlg == true) {
                // Set current price
                $data['price_current'] = $priceCurrent;                
                // Set buy price
                $data['buy_price'] = number_format($priceCurrent, 8, '.', '');
                // Set profit rate
                $data['profit_rate'] = null;
                // Set sell price
                // $data['sell_price'] = round($data['buy_price'] + $data['buy_price'] * ($data['profit_rate'] + env('TRADING_FEE', 0.25)) / 100, 8);
                // $data['sell_price'] = number_format($data['sell_price'], 8, '.', '');
                $data['sell_price'] = null;
                // Set condition type
                $data['condition_type'] = 'lower BB2';
                // Set price rate
                $data['price_rate'] = $priceRate;
                // Set price rate cond
                $data['price_rate_cond'] = $lowerPriceRateCond1;
                // Set status
                $data['status'] = 1;

                // Out Info Log
                // Log::channel('btx')->info($bb);

                $this->runUserList($users, $data);
                return;                    
            }  
        }
    }

    /**
     * @param $users
     * @param $data
     * @return null
     */
    protected function runUserList($users, $data)
    {
        foreach ($users as $key => $user) {
            // Only run for test
            // if ($user->id != 1) {
            //     continue;
            // }

            $exchange = $user->exchanges[0];
            if ($exchange->status != 1) {
                continue;
            }

            // Get user's api key
            $apiKey    = $exchange->api_key;
            $apiSecret = $exchange->api_secret;

            // Init object class
            $btx1 = new Btx1($apiKey, $apiSecret);

            $data['user_id'] = $user->id;
            // Get user's symbol setting
            $setting = BtxUserSymbols::where(
                [
                    'user_id'    => $data['user_id'],
                    'symbol'     => $data['symbol'],
                    'time_frame' => $data['time_frame']
                ]
            )->first();

            // // Debug
            // if ($data['user_id'] == 1) {
            //     Log::channel('btx')->debug('setting');
            //     Log::channel('btx')->debug($setting);
            // }

            if (! is_null($setting) && $setting->status == 1) {

                $preUserInfoLog = $data['user_id'] . ' : ' . $data['symbol'] . ' : ' . $data['time_frame'];
                // Log::channel('btx')->info($preUserInfoLog);                

                // Set base amount
                $data['base_amount_limit'] = $setting->base_amount_limit;
                $data['base_amount']       = $setting->base_amount;
                // Set amount
                $data['amount'] = round(($data['base_amount'] - ($data['base_amount'] * env('TRADING_FEE', 0.25) / 100)) / $data['price_current'], 2, PHP_ROUND_HALF_DOWN);                     

                // Log::channel('btx')->info($preUserInfoLog);

                // Debug
                if ($data['user_id'] == 1) {
                    Log::channel('btx')->debug($data);
                }  

                if (substr($data['condition_type'], 0, 8) == 'lower BB') {
                    $this->buyAvailable($btx1, $data);     

                    if (! in_array($data['symbol'], ['BTC-ETC', 'BTC-ZEN', 'BTC-RVN'])) {
                        $this->buy($btx1, $data);
                    }

                } else {
                    $this->sellAvailable($btx1, $data);                                
                    $this->sell($btx1, $data);
                }               
            }
        }
    }    

    /**
     * @param $btx1
     * @param $data
     * @return null
     */
    protected function buy($btx1, $data)
    {
        // Get Balances of user
        $res = $btx1->getBalances();
        if (isset($res['message'])) {
            Log::channel('btx')->info('get balances fail !');
            Log::channel('btx')->info($res);
            return;
        }

        $balances = [];
        foreach ($res as $value) {
            $balances[$value['Currency']] = $value;
        }

        $btxOrderLogs = new BtxOrderLogs();
        $btxFailOrderLogs   = new BtxFailOrderLogs();

        $tmp          = explode('-', $data['symbol']);
        $tCurrency    = $tmp[1];
        $baseCurrency = $tmp[0];

        // Check Balance >= Base Amount Limit
        if (isset($balances[$tCurrency])) {
            if ($balances[$tCurrency]['Balance'] * $data['buy_price'] >= $data['base_amount_limit']) {
                $data['message'] = 'balance amount > base amount limit';
                $btxFailOrderLogs->fill($data);
                $btxFailOrderLogs->save();
                return;
            }
        }

        if (isset($balances[$baseCurrency]['Balance'])) {
            // Check minimum USD balance
            if ($baseCurrency == 'USD') {
                if ($balances[$baseCurrency]['Available'] < env('USD_ORDER_AMOUNT_MIN', 10)) {
                    $data['message'] = 'The minimum order size is ' . env('USD_ORDER_AMOUNT_MIN', 10) . ' USD';
                    $btxFailOrderLogs->fill($data);
                    $btxFailOrderLogs->save();
                    return;
                }
            } else {
                // Check minimum BTC balance
                if ($balances[$baseCurrency]['Available'] < env('BTC_ORDER_AMOUNT_MIN', 0.0006)) {
                    $data['message'] = 'The minimum order size is ' . env('BTC_ORDER_AMOUNT_MIN', 0.0006) . ' BTC';
                    $btxFailOrderLogs->fill($data);
                    $btxFailOrderLogs->save();
                    return;
                }     
            }

            // Check base_amount > base_amount balance
            if ($balances[$baseCurrency]['Available'] < $data['base_amount']) {
                $data['base_amount'] = $balances[$baseCurrency]['Available'];
                // Calc amount again
                $data['amount'] = round(($data['base_amount'] - ($data['base_amount'] * 0.3 / 100)) / $data['buy_price'], 2, PHP_ROUND_HALF_DOWN);
            }
        }

        if ($data['amount'] > 99) {
            $data['amount'] = round($data['amount'], 0, PHP_ROUND_HALF_DOWN);
        }

        // Make buy order
        $buyOrder = $btx1->buyLimit($data['symbol'], $data['amount'], $data['buy_price']);
        if (isset($buyOrder['uuid'])) {
            // Set buy order uuid
            $data['buy_order_id'] = $buyOrder['uuid'];
            // Save buy order
            $btxOrderLogs->fill($data);
            $btxOrderLogs->save();
            // Set pre info log
            $preInfoLog = $data['user_id'] . ' : ' . $data['symbol'] . ' : ' . $data['buy_order_id'] . ' : ';
            Log::channel('btx_order')->info($preInfoLog . 'set buy order success !');
            Log::channel('btx_order')->info($buyOrder);             

        } else {
            $data['message'] = $buyOrder['message'];
            $btxFailOrderLogs->fill($data);
            $btxFailOrderLogs->save();
        }
    }

    /**
     * @param $btx1
     * @param $data
     * @return null
     */
    protected function sell($btx1, $data)
    {
        // Get Balances of user
        $res = $btx1->getBalances();
        if (isset($res['message'])) {
            Log::channel('btx')->info('get balances fail !');
            Log::channel('btx')->info($res);
            return;
        }

        $balances = [];
        foreach ($res as $value) {
            $balances[$value['Currency']] = $value;
        }

        $btxOrderLogs = new BtxOrderLogs();
        $btxFailOrderLogs   = new BtxFailOrderLogs();

        $tmp          = explode('-', $data['symbol']);
        $tCurrency    = $tmp[1];
        $baseCurrency = $tmp[0];

        // Check Currency balance
        if (isset($balances[$tCurrency])) {
            if ($balances[$tCurrency]['Available'] == 0) {
                $data['message'] = 'Balances no available !';
                $btxFailOrderLogs->fill($data);
                $btxFailOrderLogs->save();
                return;
            }
            
            if ($balances[$tCurrency]['Available'] < $data['amount']) {     
                // Check minimum USD balance
                if ($baseCurrency == 'USD') {
                    if ($balances[$tCurrency]['Available'] * $data['sell_price'] < env('USD_ORDER_AMOUNT_MIN', 10)) {
                        $data['message'] = 'The minimum order size is ' . env('USD_ORDER_AMOUNT_MIN', 10) . ' USD';
                        $btxFailOrderLogs->fill($data);
                        $btxFailOrderLogs->save();
                        return;
                    }
                } else {
                    // Check minimum BTC balance
                   if ($balances[$tCurrency]['Available'] * $data['sell_price'] < env('BTC_ORDER_AMOUNT_MIN', 0.0006)) {
                        $data['message'] = 'The minimum order size is ' . env('BTC_ORDER_AMOUNT_MIN', 0.0006) . ' BTC';
                        $btxFailOrderLogs->fill($data);
                        $btxFailOrderLogs->save();
                        return;
                    } 
                }

                $data['amount'] = $balances[$tCurrency]['Available'];
            }
        } else {
            $data['message'] = 'Balances no available !';
            $btxFailOrderLogs->fill($data);
            $btxFailOrderLogs->save();
            return;
        }

        if ($data['amount'] > 99) {
            $data['amount'] = round($data['amount'], 0, PHP_ROUND_HALF_DOWN);
        }

        // Make sell order
        $sellOrder = $btx1->sellLimit($data['symbol'], $data['amount'], $data['sell_price']);
        if (isset($sellOrder['uuid'])) {
            // Set Save sell order
            $data['sell_order_id'] = $sellOrder['uuid'];
            $btxOrderLogs->fill($data);
            $btxOrderLogs->save();
            // Set pre info log
            $preInfoLog = $data['user_id'] . ' : ' . $data['symbol'] . ' : ' . $data['sell_order_id'] . ' : ';
            Log::channel('btx_order')->info($preInfoLog . 'set sell order success !');
            Log::channel('btx_order')->info($sellOrder);            
        } else {
            // Save fail order
            $data['message'] = $sellOrder['message'];
            $btxFailOrderLogs->fill($data);
            $btxFailOrderLogs->save();
        }
    }

    /**
     * @param $btx1
     * @param $data
     * @return null
     */
    protected function buyAvailable($btx1, $data)
    {
        $minProfitRateCond = $data['upper_min_profit_rate_cond'];
        $orderList = BtxOrderLogs::where('user_id', '=', $data['user_id'])
            ->where('symbol', '=', $data['symbol'])
            ->where('buy_order_id', '=', null)
            ->where('sell_order_id', '!=', null)
            ->where('sell_status', '!=', null)
            ->whereRaw("sell_price - (sell_price * $minProfitRateCond / 100 ) > ?", $data['buy_price'])
            ->where('status', 1)
            ->get();

        if (count($orderList) > 0) {
            // Check update available order 
            foreach ($orderList as $key => $order) {
                // Set pre info log
                $preInfoLog = $data['user_id'] . ' : ' . $order->symbol . ' : ' . $order->id . ' : ';

                // Make buy order
                $buyOrder = $btx1->buyLimit($order->symbol, $order->amount, $data['buy_price']);

                if (isset($buyOrder['uuid'])) {
                    // Set buy order uuid
                    $order->buy_order_id = $buyOrder['uuid'];
                    $order->buy_price    = $data['buy_price'];
                    $order->message       = null;
                    $order->save();
                    Log::channel('btx_order')->info($preInfoLog . 'set available buy order success !');
                } else {
                    $order->message = $buyOrder['message'];
                    Log::channel('btx_order')->info($preInfoLog . 'set available buy order fail !');
                }

                Log::channel('btx_order')->info($buyOrder);
            }
        }       
    }    

    /**
     * @param $btx1
     * @param $data
     * @return null
     */
    protected function sellAvailable($btx1, $data)
    {
        $minProfitRateCond = $data['lower_min_profit_rate_cond'];
        $orderList = BtxOrderLogs::where('user_id', '=', $data['user_id'])
            ->where('symbol', '=', $data['symbol'])
            ->where('buy_order_id', '!=', null)
            ->where('buy_status', '!=', null)
            ->where('sell_order_id', '=', null)
            ->whereRaw("buy_price + (buy_price * $minProfitRateCond / 100) < ?", $data['sell_price'])
            ->where('status', 1)
            ->get();

        // if ($data['symbol'] == 'BTC-ETH') {
        //     Log::channel('btx')->info($orderList);
        // }

        if (count($orderList) > 0) {
            // Check update available order
            foreach ($orderList as $key => $order) {
                // Set pre info log
                $preInfoLog = $data['user_id'] . ' : ' . $order->symbol . ' : ' . $order->id . ' : ';

                // Make sell order
                $sellOrder = $btx1->sellLimit($order->symbol, $order->amount, $data['sell_price']);

                if (isset($sellOrder['uuid'])) {
                    // Set sell order uuid
                    $order->sell_order_id = $sellOrder['uuid'];
                    $order->sell_price    = $data['sell_price'];
                    $order->message       = null;
                    $order->save();
                    Log::channel('btx_order')->info($preInfoLog . 'set available sell order success !');
                } else {
                    Log::channel('btx_order')->info($preInfoLog . 'set available sell order fail !');
                }

                Log::channel('btx_order')->info($sellOrder);
            }
        }        
    }    

    /**
     * @param $users
     * @param $data
     * @return null
     */
    protected function runUserList2($users, $data)
    {
        foreach ($users as $key => $user) {
            // Only run for test
            // if ($user->id != 1) {
            //     continue;
            // }

            $exchange = $user->exchanges[0];
            // if ($exchange->status != 1) {
            //     continue;
            // }

            // Get user's api key
            $apiKey    = $exchange->api_key;
            $apiSecret = $exchange->api_secret;

            // Init object class
            $btx1 = new Btx1($apiKey, $apiSecret);

            $data['user_id'] = $user->id;

            $this->buyAvailable($btx1, $data);
            $this->sellAvailable($btx1, $data);                                            
        }
    }     
}
