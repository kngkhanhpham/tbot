<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Cache;

class BtxSymbols extends Model
{
    /**
     * @var string
     */
    protected $table = 'btx_symbols';

    /**
     * @var array
     */
    public static $rules = [
        'base_currency' => 'string|max:10',
        'status'        => 'integer|in:1,2',
    ];

    /**
     * @var array
     */
    protected $fillable = [
        'id',
        'base_currency',
        'symbol',
        'status',
    ];

    /**
     * @param $baseCurrency
     */
    public static function getList($baseCurrency = null)
    {
        if ($baseCurrency == null) {
            $baseCurrency = 'BTC';
        }

        $key  = 'btx_symbols_' . $baseCurrency;
        $data = Cache::rememberForever($key, function () use ($baseCurrency) {
            $res   = [];
            $query = BtxSymbols::where('status', 1);
            if ($baseCurrency != null) {
                $query->where('base_currency', $baseCurrency);
            }

            // $query->orderBy('base_currency')
            //       ->orderBy('symbol');

            $symbols = $query->get();

            foreach ($symbols as $key => $value) {
                $res[$value->symbol] = $value->symbol;
            }
            return json_encode($res);
        });

        return json_decode($data, true);
    }
}
