<?php

namespace App;

use App\Btx1;
use App\Btx2;
use App\BtxCalc;
use App\BtxFailOrderLogs;
use App\BtxOrderLogs;
use App\BtxUserSymbols;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Log;

class BtxMa
{

    /**
     * @param $users, $symbol, $timeFrame, $candle
     * @return 
     */
    public function handle($users = [], $symbol, $timeFrame, $candle = [])
    {
        if (empty($users) || empty($candle)) {
            return;
        }

        if ($symbol != 'BTC-RVN' || $timeFrame != 'thirtyMin') {
            return;
        }
        
        // $this->strategyBB($users, $symbol, $timeFrame, $candle);
        $this->strategyMAVol($users, $symbol, $timeFrame, $candle);
    }

    /**
     * @param $users, $symbol, $timeFrame, $candle
     * @return 
     */
    public function strategyMAVol($users = [], $symbol, $timeFrame, $candle = []) 
    {
        // Get open, high, low, current price, vol
        $priceOpen    = number_format($candle[0]['O'], 8, '.', '');
        $priceHigh    = number_format($candle[0]['H'], 8, '.', '');
        $priceCurrent = number_format($candle[0]['C'], 8, '.', '');
        $priceLow     = number_format($candle[0]['L'], 8, '.', '');
        $volCurrent   = round($candle[0]['BV'], 1);

        if ($priceOpen == 0 || $priceCurrent == 0 || $priceHigh == 0) {
            // Log::channel('btx_dev')->info($preUserInfoLog . ' : get price = 0');
            return;
        }             

        // Init data
        $data               = [];
        $data['symbol']     = $symbol;
        $data['time_frame'] = $timeFrame;

        $symbolList = BtxUserSymbols::getList(1);
        $optionSetting = $symbolList[$symbol][$timeFrame]['options'];

        $sma9 = [];
        $sma20 = [];

        $count = 20;
        for ($i = 0; $i < $count ; $i++) { 
            $sma9[] = BtxCalc::sma($candle, 9, $i, 10);
            $sma20[] = BtxCalc::sma($candle, 20, $i, 10);
        }

        // Log::channel('btx_dev')->info('============================');

        // xác định xu hướng SMA9
        $sma9Trend = 'sideway';
        if ($sma9[1] > $sma9[2] && $sma9[2] > $sma9[3]  
            && $sma9[3] > $sma9[4] && $sma9[4] > $sma9[5] 
            && $sma9[5] > $sma9[6] && $sma9[6] > $sma9[7]
        ) {
            if ($sma9[0] >= $sma9[1]) {
                // Đang tăng
                $sma9Trend = 'bull';
            } elseif ($sma9[0] <= $sma9[2]) {
                // Đảo chiều bắt đầu giảm
                $sma9Trend = 'bear_start';
                // Log::channel('btx_dev')->info('current price : ' . $priceCurrent);
                // Log::channel('btx_dev')->info('current vol : ' . $volCurrent);                
            }
        }

        if ($sma9[1] < $sma9[2] && $sma9[2] < $sma9[3]  
            && $sma9[3] < $sma9[4] && $sma9[4] < $sma9[5] 
            && $sma9[5] < $sma9[6] && $sma9[6] < $sma9[7]
        ) {
            if ($sma9[0] <= $sma9[1]) {
                // Đang giảm
                $sma9Trend = 'bear';
            } elseif ($sma9[0] >= $sma9[2]) {
                // Đảo chiều bắt đầu tăng
                $sma9Trend = 'bull_start';
                // Log::channel('btx_dev')->info('current price : ' . $priceCurrent);
                // Log::channel('btx_dev')->info('current vol : ' . $volCurrent);                
            }
        }    

        // Log::channel('btx_dev')->info($timeFrame . ' sma9 : ' . $sma9Trend);
        // Log::channel('btx_dev')->info('sma9 : ' . $sma9Trend . ' : ' . json_encode(array_slice($sma9, 0, 8)));        

        // xác định xu hướng SMA20
        $sma20Trend = 'sideway';
        if ($sma20[1] > $sma20[2] && $sma20[2] > $sma20[3]  
            && $sma20[3] > $sma20[4] && $sma20[4] > $sma20[5] 
            && $sma20[5] > $sma20[6] && $sma20[6] > $sma20[7]
        ) {
            if ($sma20[0] >= $sma20[1]) {
                // Đang tăng
                $sma20Trend = 'bull';
            } elseif ($sma20[0] <= $sma20[2]) {
                // Đảo chiều bắt đầu giảm
                $sma20Trend = 'bear_start';
            }
        }

        if ($sma20[1] < $sma20[2] && $sma20[2] < $sma20[3]  
            && $sma20[3] < $sma20[4] && $sma20[4] < $sma20[5] 
            && $sma20[5] < $sma20[6] && $sma20[6] < $sma20[7]
        ) {
            if ($sma20[0] <= $sma20[1]) {
                // Đang giảm
                $sma20Trend = 'bear';
            } elseif ($sma20[0] >= $sma20[2]) {
                // Đảo chiều bắt đầu tăng
                $sma20Trend = 'bull_start';
            }
        }

        // Log::channel('btx_dev')->info($timeFrame . ' sma20 : ' . $sma20Trend);
        // Log::channel('btx_dev')->info('sma20 : ' . $sma20Trend . ' : ' . json_encode(array_slice($sma20, 0, 8)));


        // Xác định điểm giao cắt
        if ($sma9[3] >= $sma20[3] && $sma9[2] >= $sma20[2] 
            && $sma9[1] < $sma20[1] && $sma9[0] < $sma20[0]) {
            Log::channel('btx_dev')->info('============================');
            Log::channel('btx_dev')->info('sma9 : ' . $sma9Trend . ' : ' . json_encode(array_slice($sma9, 0, 8)));        
            Log::channel('btx_dev')->info('sma20 : ' . $sma20Trend . ' : ' . json_encode(array_slice($sma20, 0, 8)));
            Log::channel('btx_dev')->info('sma9 cut sma20 (DOWN)');
            Log::channel('btx_dev')->info('current price : ' . $priceCurrent);
            Log::channel('btx_dev')->info('current vol : ' . $volCurrent);
        }

        if ($sma9[3] <= $sma20[3] && $sma9[2] <= $sma20[2] 
            && $sma9[1] > $sma20[1] && $sma9[0] > $sma20[0]) {
            Log::channel('btx_dev')->info('============================');
            Log::channel('btx_dev')->info('sma9 : ' . $sma9Trend . ' : ' . json_encode(array_slice($sma9, 0, 8)));        
            Log::channel('btx_dev')->info('sma20 : ' . $sma20Trend . ' : ' . json_encode(array_slice($sma20, 0, 8)));
            Log::channel('btx_dev')->info('sma9 cut sma20 (UP)');
            Log::channel('btx_dev')->info('current price : ' . $priceCurrent);
            Log::channel('btx_dev')->info('current vol : ' . $volCurrent);            
        }        
        
    }     

    /**
     * @param $users
     * @param $data
     * @return null
     */
    protected function runUserList($users, $data)
    {
        foreach ($users as $key => $user) {
            // Only run for test
            // if ($user->id != 1) {
            //     continue;
            // }

            $exchange = $user->exchanges[0];
            if ($exchange->status != 1) {
                continue;
            }

            // Get user's api key
            $apiKey    = $exchange->api_key;
            $apiSecret = $exchange->api_secret;

            // Init object class
            $btx1 = new Btx1($apiKey, $apiSecret);

            $data['user_id'] = $user->id;
            // Get user's symbol setting
            $setting = BtxUserSymbols::where(
                [
                    'user_id'    => $data['user_id'],
                    'symbol'     => $data['symbol'],
                    'time_frame' => $data['time_frame']
                ]
            )->first();

            // // Debug
            // if ($data['user_id'] == 1) {
            //     Log::channel('btx_dev')->debug('setting');
            //     Log::channel('btx_dev')->debug($setting);
            // }

            if (! is_null($setting) && $setting->status == 1) {

                $preUserInfoLog = $data['user_id'] . ' : ' . $data['symbol'] . ' : ' . $data['time_frame'];
                // Log::channel('btx_dev')->info($preUserInfoLog);                

                // Set base amount
                $data['base_amount_limit'] = $setting->base_amount_limit;
                $data['base_amount']       = $setting->base_amount;
                // Set amount
                $data['amount'] = round(($data['base_amount'] - ($data['base_amount'] * env('TRADING_FEE', 0.25) / 100)) / $data['price_current'], 8, PHP_ROUND_HALF_DOWN);                     

                Log::channel('btx_dev')->info($preUserInfoLog);

                // Debug
                if ($data['user_id'] == 1) {
                    Log::channel('btx_dev')->debug($data);
                }  

                if (substr($data['condition_type'], 0, 8) == 'upper BB') {
                    // Log::channel('btx_dev')->debug('sellToBuy');
                    $this->sellToBuy($btx1, $data);                                
                } else {
                    // Log::channel('btx_dev')->debug('buyToSell');
                    $this->buyToSell($btx1, $data);        
                }
            }
        }
    }

    /**
     * @param $btx1
     * @param $data
     * @return null
     */
    protected function buyToSell($btx1, $data)
    {
        $key = 'btx_balances_' . $data['user_id'];
        if (Cache::has($key)) {
            $balances = Cache::get($key);
        } else {
            // Get user's balances
            $balancesRes = $btx1->getBalances();

            if (empty($balancesRes) || isset($balancesRes['error'])) {
                Log::channel('btx_dev')->error('get balances error');
                Log::channel('btx_dev')->error($balancesRes);
            } else {

                $balances = [];
                foreach ($balancesRes as $key => $value) {
                    $balances[strtoupper($value['currency'])]['amount']    = $value['amount'];
                    $balances[strtoupper($value['currency'])]['available'] = $value['available'];
                }

                if (!empty($balances)) {
                    Cache::put($key, $balances, 1);
                }
            }
        }

        $tCurrency    = substr($data['symbol'], 0, 3);
        $baseCurrency = substr($data['symbol'], -3);
        // Log::channel('btx_dev')->info($baseCurrency);
        // Log::channel('btx_dev')->info('amount    : ' . $balances[$baseCurrency]['amount']);
        // Log::channel('btx_dev')->info('available : ' . $balances[$baseCurrency]['available']);

        // Balances amount > config limit amount (values calculate on base_currency)
        if (isset($balances[$tCurrency]) && $balances[$tCurrency]['amount'] * $data['buy_price'] >= $data['base_amount_limit']) {
            $btxFailOrderLogs = new BtxFailOrderLogs();
            $data['message'] = 'balances >= base amount limit';
            $btxFailOrderLogs->fill($data);
            $btxFailOrderLogs->save();

            Log::channel('btx_dev')->info($data['message']);

            return;
        }

        if (isset($balances[$baseCurrency]['available'])) {
            // Balances base_currency < config amount base_currency
            if ($balances[$baseCurrency]['available'] < env('USD_ORDER_AMOUNT_MIN', 20)) {
                $btxFailOrderLogs = new BtxFailOrderLogs();
                $data['message'] = 'The minimum order size is ' . env('USD_ORDER_AMOUNT_MIN', 20) . ' USD';
                $btxFailOrderLogs->fill($data);
                $btxFailOrderLogs->save();
                return;
            }

            if ($balances[$baseCurrency]['available'] < $data['base_amount']) {
                $data['base_amount'] = $balances[$baseCurrency]['available'];
                // Calc amount again
                $data['amount'] = round(($data['base_amount'] - ($data['base_amount'] * env('TRADING_FEE', 0.25) / 100)) / $data['buy_price'], 8, PHP_ROUND_HALF_DOWN);
            }
        }

        // Make buy order
        // $buyOrder = $btx1->newOrder($data['symbol'], $data['amount'], 1.23, 'bitfinex', 'buy', 'exchange market');
        $buyOrder = $btx1->newOrder($data['symbol'], $data['amount'], $data['buy_price'], 'bitfinex', 'buy', 'exchange limit');

        Log::channel('btx_dev')->info($buyOrder);

        // Calc amount again
        $data['amount'] = round($data['amount'] - ($data['amount'] * env('TRADING_FEE', 0.25) / 100), 8, PHP_ROUND_HALF_DOWN);

        // Make sell order
        if (isset($buyOrder['order_id'])) {
            // Save buy order logs
            $btxOrderLogs         = new BtxOrderLogs();
            $data['buy_order_id'] = $buyOrder['order_id'];
            $data['buy_price']    = $buyOrder['price'];
            $btxOrderLogs->fill($data);
            $btxOrderLogs->save();

            if ($buyOrder['is_live']) {
                return;
            }

            // Calc sell price
            $data['sell_price'] = round($data['buy_price'] + $data['buy_price'] * ($data['profit_rate'] + env('TRADING_FEE', 0.25)) / 100, 8);

            // Make sell order
            // $sellOrder = $btx1->newOrder($data['symbol'], $buyOrder['original_amount'], $sellPrice, 'bitfinex', 'sell', 'exchange limit');
            $sellOrder = $btx1->newOrder($data['symbol'], $data['amount'], $data['sell_price'], 'bitfinex', 'sell', 'exchange limit');

            Log::channel('btx_dev')->info($sellOrder);

            if (isset($sellOrder['order_id'])) {
                // Save sell order
                $data['sell_order_id'] = $sellOrder['order_id'];
                $btxOrderLogs->fill($data);
                $btxOrderLogs->save();
            }
        } else {
            $btxFailOrderLogs = new BtxFailOrderLogs();
            $data['message'] = $buyOrder['message'];
            $btxFailOrderLogs->fill($data);
            $btxFailOrderLogs->save();
        }
    }

    /**
     * @param $btx1
     * @param $data
     * @return null
     */
    protected function sellToBuy($btx1, $data)
    {
        $key = 'btx_balances_' . $data['user_id'];
        if (Cache::has($key)) {
            $balances = Cache::get($key);
        } else {
            // Get user's balances
            $balancesRes = $btx1->getBalances();

            if (empty($balancesRes) || isset($balancesRes['error'])) {
                Log::channel('btx_dev')->error(get_class($this) . ' :: ' . $data['user_id'] . ' : get balances error');
                Log::channel('btx_dev')->error($balancesRes);
            } else {

                $balances = [];
                foreach ($balancesRes as $key => $value) {
                    $balances[strtoupper($value['currency'])]['amount']    = $value['amount'];
                    $balances[strtoupper($value['currency'])]['available'] = $value['available'];
                }

                if (!empty($balances)) {
                    Cache::put($key, $balances, 1);
                }
            }
        }

        $tCurrency    = substr($data['symbol'], 0, 3);
        $baseCurrency = substr($data['symbol'], -3);
        // Log::channel('btx_dev')->info($baseCurrency);
        // Log::channel('btx_dev')->info('amount    : ' . $balances[$baseCurrency]['amount']);
        // Log::channel('btx_dev')->info('available : ' . $balances[$baseCurrency]['available']);

        // Check Currency balance
        if (isset($balances[$tCurrency])) {
            if ($balances[$tCurrency]['available'] < $data['amount']) {
                if ($balances[$tCurrency]['available'] * $data['sell_price'] < env('USD_ORDER_AMOUNT_MIN', 20)) {
                    $data['message'] = 'The minimum order size is ' . env('USD_ORDER_AMOUNT_MIN', 20) . ' USD';
                    $btxFailOrderLogs = new BtxFailOrderLogs();
                    $btxFailOrderLogs->fill($data);
                    $btxFailOrderLogs->save();
                    return;
                }
                $data['amount'] = $balances[$tCurrency]['available'];
            }
        } else {
            $data['message'] = 'balances no available !';
            $btxFailOrderLogs = new BtxFailOrderLogs();
            $btxFailOrderLogs->fill($data);
            $btxFailOrderLogs->save();
            return;
        }

        // Make sell order
        $sellOrder = $btx1->newOrder($data['symbol'], $data['amount'], $data['sell_price'], 'bitfinex', 'sell', 'exchange limit');

        Log::channel('btx_dev')->info($sellOrder);

        if (isset($sellOrder['order_id'])) {
            // Save sell order
            $data['sell_order_id'] = $sellOrder['order_id'];
            // Save buy order logs
            $btxOrderLogs    = new BtxOrderLogs();
            $btxOrderLogs->fill($data);
            $btxOrderLogs->save();

            if ($sellOrder['is_live']) {
                return;
            }

            $data['amount_inc_fee'] = round(($data['amount'] + ($data['amount'] * env('TRADING_FEE', 0.25) / 100)), 8);

            // Make buy order
            $buyOrder = $btx1->newOrder($data['symbol'], $data['amount_inc_fee'], $data['buy_price'], 'bitfinex', 'buy', 'exchange limit');

            Log::channel('btx_dev')->info($buyOrder);

            if (isset($buyOrder['order_id'])) {
                // Save buy order logs
                $btxOrderLogs    = new BtxOrderLogs();
                $data['buy_order_id'] = $buyOrder['order_id'];
                $btxOrderLogs->fill($data);
                $btxOrderLogs->save();
            }
        } else {
            $btxFailOrderLogs = new BtxFailOrderLogs();
            $data['message'] = $sellOrder['message'];
            $btxFailOrderLogs->fill($data);
            $btxFailOrderLogs->save();
        }
    }
}
