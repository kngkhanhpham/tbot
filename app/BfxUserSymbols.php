<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Cache;

class BfxUserSymbols extends Model
{
    /**
     * @var string
     */
    protected $table = 'bfx_user_symbols';

    /**
     * @var array
     */
    public static $rules = [
        'symbol'            => 'required|string|max:255',
        'time_frame'        => 'required|string|max:10',
        'base_amount_limit' => 'required|numeric|min:20|max:999999',
        'base_amount'       => 'required|numeric|min:20|max:999999',
        'stoploss_rate'     => 'max:20',
        'status'            => 'integer|in:1,2',
    ];

    /**
     * @var array
     */
    protected $fillable = [
        'id',
        'user_id',
        'symbol',
        'time_frame',
        'base_amount_limit',
        'base_amount',
        'stoploss_rate',
        'options',
        'status',
    ];

    /**
     * @param $userId
     */
    public static function getList($userId)
    {
        $key  = 'bfx_user_symbols'; 
        // Cache::forget('bfx_user_symbols');     
        $data = Cache::rememberForever($key, function () use ($userId) {
            $res   = [];
            $query = BfxUserSymbols::where('status', 1);
            if ($userId) {
                $query->where('user_id', $userId);
            }

            $symbols = $query->get()->toArray();

            foreach ($symbols as $key => $value) {
                $value['options'] = json_decode(base64_decode($value['options']), true);
                $res[$value['symbol']][$value['time_frame']] = $value;
            }
            return json_encode($res);
        });

        return json_decode($data, true);
    }    
}
