<?php

namespace App;

use App\Btx1;
use App\Btx2;
use App\BtxCalc;
use App\BtxFailOrderLogs;
use App\BtxOrderLogs;
use App\BtxUserSymbols;
use App\User;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Log;

class BtxBbV4
{
    /**
     * @param array $users
     * @param $symbols
     */
    public function handle($users = [], $symbol, $timeFrame, $candle = [])
    {
        if (empty($users) || empty($candle)) {
            return;
        }

        // Get open, high, low, current price, vol
        $priceOpen    = number_format($candle[0]['O'], 8, '.', '');
        $priceHigh    = number_format($candle[0]['H'], 8, '.', '');
        $priceCurrent = number_format($candle[0]['C'], 8, '.', '');
        $priceLow     = number_format($candle[0]['L'], 8, '.', '');
        $volCurrent   = round($candle[0]['BV'], 1);

        if ($priceOpen == 0 || $priceCurrent == 0 || $priceHigh == 0) {
            // Log::channel('btx')->info($preUserInfoLog . ' : get price = 0');
            return;
        }         

        // Init data
        $data               = [];
        $data['symbol']     = $symbol;
        $data['time_frame'] = $timeFrame;

        $symbolList = BtxUserSymbols::getList(1);

        if (! isset($symbolList[$symbol][$timeFrame])) {
            return;
        }

        $optionSetting = $symbolList[$symbol][$timeFrame]['options'];
        
        // if ($symbol == 'BTC-RVN') {
        //     \Log::channel('btx')->info($optionSetting);
        // }

        // use BB to check make order
        // Set price upper rate cond
        $priceUpperRateCond = $optionSetting['opt_bb_upper_price_rate_cond'];
        // Set price lower rate cond
        $priceLowerRateCond = $optionSetting['opt_bb_lower_price_rate_cond'];
        // Calc min upper profit rate
        $upperMinProfitRate = round($priceUpperRateCond / $optionSetting['opt_bb_upper_calc_profit_rate'], 2);
        // $upperMinProfitRate = env('UPPER_MIN_PROFIT_RATE', 0.5);
        // Calc min lower profit rate
        $lowerMinProfitRate = round(abs($priceLowerRateCond) / $optionSetting['opt_bb_lower_calc_profit_rate'], 2);
        // $lowerMinProfitRate = env('LOWER_MIN_PROFIT_RATE', 0.5);

        // Get BB data
        $bb = BtxCalc::bb($candle, 20, 2);            

        // Sell when current price > BB upper then buy
        // if ($symbol == 'BTC-XRP') {
        $keyST = $symbol.$timeFrame. 'Upper';
        if ($optionSetting['opt_bb_upper_status'] == 1 && ($priceCurrent > $bb['upper'] || Cache::has($keyST))) {
            $onFlg = false;

            $cachePrices = [];
            if (Cache::has($keyST)) {
                $cachePrices = Cache::get($keyST);
                if (!empty($cachePrices)) {
                    $cachePrices = json_decode($cachePrices, true);
                    if (!empty($cachePrices)) {
                        if ($cachePrices[0] <= $priceCurrent) {
                            $priceStop = round($priceCurrent - $priceCurrent * env('PRICE_STOP_PERCENT', 0.5) / 100, 8);
                            if ($priceStop > $cachePrices[0]) {
                                $cachePrices = array_merge([$priceStop], $cachePrices);
                            }
                            Cache::put($keyST, json_encode($cachePrices), 3600);
                            Log::channel('btx')->info($keyST . ' cachePrices : ' . Cache::get($keyST));
                            return;
                        } else {
                            // Set price current
                            $priceCurrent = $cachePrices[0];
                            // Calc price rate
                            $priceRate = round(($priceCurrent - $bb['mid']) / $bb['mid'] * 100, 2);
                            // Calc profit rate
                            $upperProfitRate = round($priceRate / $optionSetting['opt_bb_upper_calc_profit_rate'], 2);      
                            
                            Log::channel('btx')->info($keyST . ' upperProfitRate : ' . $upperProfitRate);
                            
                            if ($upperProfitRate >= $upperMinProfitRate) {
                                // Set profit rate
                                if ($upperProfitRate < 1) {
                                    $upperProfitRate = 1;
                                }       
                                // Set candle range
                                $data['candle_range'] = 9;
                                $onFlg = true;
                            }                   

                            Cache::forget($keyST);
                        }
                    }
                } else {
                    Cache::forget($keyST);
                }
            }

            // Case 1 : Check price rate (current/open)
            // if ($priceRate >= $priceUpperRateCond) {
            //     $onFlg = true;
            //     // Set candle range
            //     $data['candle_range'] = 1;                    
            // }

            // Case 2 : Check price rate (current/bb['mid'])
            if ($onFlg == false) {
                // Set price Upper Rate Cond
                // $priceUpperRateCond = $priceUpperRateCond;                     
                // Calc price rate
                $priceRate = round(($priceCurrent - $bb['mid']) / $bb['mid'] * 100, 2);
                // Calc profit rate
                $upperProfitRate = round(abs($priceRate) / $optionSetting['opt_bb_upper_calc_profit_rate'], 2);

                if ($priceRate >= $priceUpperRateCond) {
                    if (empty($cachePrices)) {
                        $priceStop = round($priceCurrent - $priceCurrent * env('PRICE_STOP_PERCENT', 0.5) / 100, 8);
                        $cachePrices = [$priceStop, $bb['mid']];
                        if (Cache::has($keyST)) {
                            Cache::put($keyST, json_encode($cachePrices), 3600);
                        } else {
                            Cache::add($keyST, json_encode($cachePrices), 3600);
                        }
                        
                        Log::channel('btx')->info($keyST . ' cachePrices : ' . Cache::get($keyST) . ' first');
                        
                        return;
                    }
                }                 
            }

            // // Case 3 : Check price rate (current/open of prev candle)
            // if ($onFlg == false) {
            //     // Get candle idx               
            //     $idx = $optionSetting['opt_bb_upper_check_candle_range'];
            //     $prevCandle = $candle[$idx];
            //     // Calc price rate
            //     $priceRate = round(($priceCurrent - $prevCandle['C']) / $prevCandle['C'] * 100, 2);

            //     if ($priceRate >= $priceUpperRateCond) {
            //         if (empty($cachePrices)) {
            //             $priceStop = round($priceCurrent - $priceCurrent * env('PRICE_STOP_PERCENT', 0.5) / 100, 8);
            //             $cachePrices = [$priceStop, $bb['mid']];
            //             if (Cache::has($keyST)) {
            //                 Cache::put($keyST, json_encode($cachePrices), 3600);
            //             } else {
            //                 Cache::add($keyST, json_encode($cachePrices), 3600);
            //             }
                        
            //             Log::channel('btx')->info($keyST . ' cachePrices : ' . Cache::get($keyST) . ' first');
                        
            //             return;
            //         }
            //     }

            //     // Set candle range
            //     $data['candle_range'] = $idx + 1;                    
            // }                

            if ($onFlg == true) {
                // Set current price
                $data['price_current'] = $priceCurrent;
                // Set sell price
                $data['sell_price'] = number_format($priceCurrent, 8, '.', '');
                // Set profit rate
                $data['profit_rate'] = $upperProfitRate;
                // Set buy price
                $data['buy_price'] = round($data['sell_price'] - $data['sell_price'] * ($data['profit_rate'] + env('TRADING_FEE', 0.25)) / 100, 8);
                $data['buy_price'] = number_format($data['buy_price'], 8, '.', '');
                // Set condition type
                $data['condition_type'] = 'upper BB2';
                // Set price rate
                $data['price_rate'] = $priceRate;
                // Set price rate cond
                $data['price_rate_cond'] = $priceUpperRateCond;
                // Set status
                $data['status'] = 1;

                // Out Info Log
                Log::channel('btx')->info('--> BB Upper !!!');
                Log::channel('btx')->info($bb);

                $this->runUserList($users, $data);
                return;
            }
        }                       

        // Buy when current price < BB lower then sell
        // if ($symbol == 'BTC-XRP') {
        $keyST = $symbol.$timeFrame. 'Lower';
        if ($optionSetting['opt_bb_lower_status'] == 1 && ($priceCurrent < $bb['lower'] || Cache::has($keyST))) {
            $onFlg = false;

            $cachePrices = [];
            if (Cache::has($keyST)) {
                $cachePrices = Cache::get($keyST);
                if (!empty($cachePrices)) {
                    $cachePrices = json_decode($cachePrices, true);
                    if (!empty($cachePrices)) {
                        if ($cachePrices[0] > $priceCurrent) {
                            $priceStop = round($priceCurrent - $priceCurrent * ($priceLowerRateCond / 5 / 100), 8);
                            if ($priceStop < $cachePrices[0]) {
                                $cachePrices = array_merge([$priceStop], $cachePrices);
                            }
                            Cache::put($keyST, json_encode($cachePrices), 3600);
                            Log::channel('btx')->info($keyST . ' cachePrices : ' . Cache::get($keyST));
                            return;
                        } else {
                            // Set price current
                            $priceCurrent = $cachePrices[0];                            
                            // Calc price rate
                            $priceRate = round(($priceCurrent - $bb['mid']) / $bb['mid'] * 100, 2);
                            // Calc profit rate
                            $lowerProfitRate = round(abs($priceRate) / $optionSetting['opt_bb_lower_calc_profit_rate'], 2);
                            
                            Log::channel('btx')->info($keyST . ' lowerProfitRate : ' . $lowerProfitRate);

                            if ($lowerProfitRate >= $lowerMinProfitRate) {
                                // Set profit rate
                                if ($lowerProfitRate < 1) {
                                    $lowerProfitRate = 1;
                                }
                                // Set candle range
                                $data['candle_range'] = 9;
                                $onFlg = true;
                            }    
                            Cache::forget($keyST);
                        }
                    }
                } else {
                    Cache::forget($keyST);
                }                
            } 

            // Case 1 : Check price rate (current/open)
            // if ($priceRate <= $priceLowerRateCond) {
            //     $onFlg = true;
            //     // Set candle range
            //     $data['candle_range'] = 1;                    
            // }

            // Case 2 : Check price rate (current/bb['mid'])
            if ($onFlg == false) {
                // Set price Lower Rate Cond
                // $priceLowerRateCond = $priceLowerRateCond;
                // Calc price rate
                $priceRate = round(($priceCurrent - $bb['mid']) / $bb['mid'] * 100, 2);
                // Calc profit rate
                $lowerProfitRate = round(abs($priceRate) / $optionSetting['opt_bb_lower_calc_profit_rate'], 2);
                
                if ($priceRate <= $priceLowerRateCond) {
                    if (empty($cachePrices)) {                        
                        $priceStop = round($priceCurrent - $priceCurrent * ($priceLowerRateCond / 5 / 100), 8);
                        $cachePrices = [$priceStop, $bb['mid']];
                        if (Cache::has($keyST)) {
                            Cache::put($keyST, json_encode($cachePrices), 3600);
                        } else {
                            Cache::add($keyST, json_encode($cachePrices), 3600);
                        }

                        Log::channel('btx')->info($keyST . ' cachePrices : ' . Cache::get($keyST) . ' first');

                        return;
                    }                    
                }
            }

            // // Case 3 : Check price rate (current/open of prev candle)
            // if ($onFlg == false) {
            //     // Get candle idx                 
            //     $idx = $optionSetting['opt_bb_lower_check_candle_range'];
            //     $prevCandle = $candle[$idx];
            //     // Calc price rate
            //     $priceRate = round(($priceCurrent - $prevCandle['C']) / $prevCandle['C'] * 100, 2);
            //     // Calc profit rate
            //     // $lowerProfitRate = round(abs($priceRate) / $optionSetting['opt_bb_lower_calc_profit_rate'], 2);
            //     $priceAvg = ($priceCurrent + $prevCandle['C'] + $bb['mid']) / 3;
            //     $lowerProfitRate = round(abs($priceCurrent - $priceAvg) / $priceAvg * 100, 2);

            //     if ($priceRate <= $priceLowerRateCond && $lowerProfitRate >= $lowerMinProfitRate) {
            //         $onFlg = true;
            //     }

            //     // Set candle range
            //     $data['candle_range'] = $idx + 1;                    
            // }    

            if ($onFlg == true) {

                // $range = 24;
                // if ($timeFrame == 'fiveMin') {
                //     $range = 36;
                // }

                // for ($i = 1; $i < $range; $i++) { 
                //     if ($priceCurrent > $candle[$i]['C']) {
                //         Log::channel('btx')->info('priceCurrent > priceIndex ' . $i);
                //         return;
                //     }
                // }

                // Set current price
                $data['price_current'] = $priceCurrent;                
                // Set buy price
                $data['buy_price'] = number_format($priceCurrent, 8, '.', '');
                // Set profit rate
                $data['profit_rate'] = $lowerProfitRate;
                // Set sell price
                $data['sell_price'] = round($data['buy_price'] + $data['buy_price'] * ($data['profit_rate'] + env('TRADING_FEE', 0.25)) / 100, 8);
                $data['sell_price'] = number_format($data['sell_price'], 8, '.', '');
                // Set condition type
                $data['condition_type'] = 'lower BB2';
                // Set price rate
                $data['price_rate'] = $priceRate;
                // Set price rate cond
                $data['price_rate_cond'] = $priceLowerRateCond;
                // Set status
                $data['status'] = 1;

                // Out Info Log
                Log::channel('btx')->info('--> BB Lower !!!');
                Log::channel('btx')->info($bb);

                $this->runUserList($users, $data);
                return;                    
            }  
        }
    }

    /**
     * @param $users
     * @param $data
     * @return null
     */
    protected function runUserList($users, $data)
    {
        foreach ($users as $key => $user) {
            // Only run for test
            // if ($user->id != 1) {
            //     continue;
            // }

            $exchange = $user->exchanges[0];
            if ($exchange->status != 1) {
                continue;
            }

            // Get user's api key
            $apiKey    = $exchange->api_key;
            $apiSecret = $exchange->api_secret;

            // Init object class
            $btx1 = new Btx1($apiKey, $apiSecret);

            $data['user_id'] = $user->id;
            // Get user's symbol setting
            $setting = BtxUserSymbols::where(
                [
                    'user_id'    => $data['user_id'],
                    'symbol'     => $data['symbol'],
                    'time_frame' => $data['time_frame']
                ]
            )->first();

            // // Debug
            // if ($data['user_id'] == 1) {
            //     Log::channel('btx')->debug('setting');
            //     Log::channel('btx')->debug($setting);
            // }

            if (! is_null($setting) && $setting->status == 1) {

                $preUserInfoLog = $data['user_id'] . ' : ' . $data['symbol'] . ' : ' . $data['time_frame'];
                // Log::channel('btx')->info($preUserInfoLog);                

                // Set base amount
                $data['base_amount_limit'] = $setting->base_amount_limit;
                $data['base_amount']       = $setting->base_amount;
                // Set amount
                $data['amount'] = round(($data['base_amount'] - ($data['base_amount'] * env('TRADING_FEE', 0.25) / 100)) / $data['price_current'], 8, PHP_ROUND_HALF_DOWN);                     

                Log::channel('btx')->info($preUserInfoLog);

                // Debug
                if ($data['user_id'] == 1) {
                    Log::channel('btx')->debug($data);
                }  

                if (substr($data['condition_type'], 0, 8) == 'upper BB') {
                    // Log::channel('btx')->debug('sellToBuy');
                    $this->sellToBuy($btx1, $data);                                
                } else {
                    // Log::channel('btx')->debug('buyToSell');
                    $this->buyToSell($btx1, $data);        
                }
            }
        }
    }    

    /**
     * @param $btx1
     * @param $data
     * @return null
     */
    protected function buyToSell($btx1, $data)
    {
        // Get Balances of user
        $res = $btx1->getBalances();
        if (isset($res['message'])) {
            Log::channel('btx')->info('get balances fail !');
            Log::channel('btx')->info($res);
            return;
        }

        $balances = [];
        foreach ($res as $value) {
            $balances[$value['Currency']] = $value;
        }

        $btxOrderLogs = new BtxOrderLogs();
        $btxFailOrderLogs   = new BtxFailOrderLogs();

        $tmp          = explode('-', $data['symbol']);
        $tCurrency    = $tmp[1];
        $baseCurrency = $tmp[0];

        // Check Balance >= Base Amount Limit
        if (isset($balances[$tCurrency])) {
            if ($balances[$tCurrency]['Balance'] * $data['buy_price'] >= $data['base_amount_limit']) {
                $data['message'] = 'balance amount > base amount limit';
                $btxFailOrderLogs->fill($data);
                $btxFailOrderLogs->save();
                return;
            }
        }

        if (isset($balances[$baseCurrency]['Balance'])) {
            // Check minimum BTC balance
            if ($baseCurrency == 'BTC' && $balances[$baseCurrency]['Available'] < env('BTC_ORDER_AMOUNT_MIN', 0.0006)) {
                $data['message'] = 'The minimum order size is ' . env('BTC_ORDER_AMOUNT_MIN', 0.0006) . ' BTC';
                $btxFailOrderLogs->fill($data);
                $btxFailOrderLogs->save();
                return;
            }

            // Check minimum USD balance
            if ($baseCurrency == 'USD' && $balances[$baseCurrency]['Available'] < env('USD_ORDER_AMOUNT_MIN', 20)) {
                $data['message'] = 'The minimum order size is ' . env('USD_ORDER_AMOUNT_MIN', 20) . ' USD';
                $btxFailOrderLogs->fill($data);
                $btxFailOrderLogs->save();
                return;
            }

            // Check base_amount > base_amount balance
            if ($balances[$baseCurrency]['Available'] < $data['base_amount']) {
                $data['base_amount'] = $balances[$baseCurrency]['Available'];
                // Calc amount again
                $data['amount'] = round(($data['base_amount'] - ($data['base_amount'] * 0.3 / 100)) / $data['buy_price'], 8, PHP_ROUND_HALF_DOWN);
            }
        }

        // Make buy order
        $buyOrder = $btx1->buyLimit($data['symbol'], $data['amount'], $data['buy_price']);
        Log::channel('btx')->info($buyOrder);

        if (isset($buyOrder['uuid'])) {
            // Set buy order uuid
            $data['buy_order_id'] = $buyOrder['uuid'];
            // Save buy order
            $btxOrderLogs->fill($data);
            $btxOrderLogs->save();

            // Check buy order status
            $checkBuyOrder = $btx1->getOrder($buyOrder['uuid']);
            if ($checkBuyOrder['IsOpen']) {
                return;
            }

            // Make sell order
            $sellOrder = $btx1->sellLimit($data['symbol'], $data['amount'], $data['sell_price']);
            Log::channel('btx')->info($sellOrder);

            if (isset($sellOrder['uuid'])) {
                // Set sell order uuid
                $data['sell_order_id'] = $sellOrder['uuid'];
                $btxOrderLogs->fill($data);
                $btxOrderLogs->save();
            }

        } else {
            $data['message'] = $buyOrder['message'];
            $btxFailOrderLogs->fill($data);
            $btxFailOrderLogs->save();
        }
    }

    /**
     * @param $btx1
     * @param $data
     * @return null
     */
    protected function sellToBuy($btx1, $data)
    {
        // Get Balances of user
        $res = $btx1->getBalances();
        if (isset($res['message'])) {
            Log::channel('btx')->info('get balances fail !');
            Log::channel('btx')->info($res);
            return;
        }

        $balances = [];
        foreach ($res as $value) {
            $balances[$value['Currency']] = $value;
        }

        $btxOrderLogs = new BtxOrderLogs();
        $btxFailOrderLogs   = new BtxFailOrderLogs();

        $tmp          = explode('-', $data['symbol']);
        $tCurrency    = $tmp[1];
        $baseCurrency = $tmp[0];

        // Check Currency balance
        if (isset($balances[$tCurrency])) {
            if ($balances[$tCurrency]['Available'] == 0) {
                $data['message'] = 'Balances no available !';
                $btxFailOrderLogs->fill($data);
                $btxFailOrderLogs->save();
                return;
            }
            
            if ($balances[$tCurrency]['Available'] < $data['amount']) {               
                if ($balances[$tCurrency]['Available'] * $data['sell_price'] < env('BTC_ORDER_AMOUNT_MIN', 0.0006)) {
                    $data['message'] = 'The minimum order size is ' . env('BTC_ORDER_AMOUNT_MIN', 0.0006) . ' BTC';
                    $btxFailOrderLogs->fill($data);
                    $btxFailOrderLogs->save();
                    return;
                }
                $data['amount'] = $balances[$tCurrency]['Available'];
            }
        } else {
            $data['message'] = 'Balances no available !';
            $btxFailOrderLogs->fill($data);
            $btxFailOrderLogs->save();
            return;
        }

        // Make sell order
        $sellOrder = $btx1->sellLimit($data['symbol'], $data['amount'], $data['sell_price']);
        Log::channel('btx')->info($sellOrder);

        if (isset($sellOrder['uuid'])) {
            // Set Save sell order
            $data['sell_order_id'] = $sellOrder['uuid'];
            $btxOrderLogs->fill($data);
            $btxOrderLogs->save();

            // Check sell order status
            $checkSellOrder = $btx1->getOrder($sellOrder['uuid']);
            if ($checkSellOrder['IsOpen']) {
                return;
            }

            // Make buy order
            $buyOrder = $btx1->buyLimit($data['symbol'], $data['amount'], $data['buy_price']);
            Log::channel('btx')->info($buyOrder);

            if (isset($buyOrder['uuid'])) {
                // Set buy order uuid
                $data['buy_order_id'] = $buyOrder['uuid'];
                $btxOrderLogs->fill($data);
                $btxOrderLogs->save();
            }

        } else {
            // Save fail order
            $data['message'] = $sellOrder['message'];
            $btxFailOrderLogs->fill($data);
            $btxFailOrderLogs->save();
        }
    }
}
