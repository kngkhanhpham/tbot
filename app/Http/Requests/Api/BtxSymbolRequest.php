<?php

namespace App\Http\Requests\Api;

use App\BfxUserSymbols;
use App\Http\Requests\Api\BaseApiRequest;

class BtxSymbolRequest extends BaseApiRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $excepts      = [];
        $custom_rules = [
            'symbol'     => 'required|string|max:255',
            'time_frame' => 'required|string|max:255',
        ];

        // $base_rules = array_except(BfxUserSymbols::$rules, $excepts);
        // $rules      = array_merge($base_rules, $custom_rules);

        $rules = $custom_rules;

        return $rules;     
    }
}
