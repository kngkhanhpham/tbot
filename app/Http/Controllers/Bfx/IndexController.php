<?php

namespace App\Http\Controllers\Bfx;

use App\Bfx1;
use App\BfxOrderLogs;
use App\Http\Controllers\Controller;
use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Input;
use App\User;
use Illuminate\Support\Arr;

class IndexController extends Controller
{
    
    private $tickers;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
        $this->tickers = [];

        $userList       = User::select('id AS user_id', 'name')->get();
        $this->userList = Arr::pluck($userList, 'name', 'user_id');        
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(\Illuminate\Http\Request $request)
    {
        $input = Input::get();
        $user_id = Auth::id();                    

        if ($user_id == 1) {
            if (array_key_exists('user_id', $input)) {
                $request->session()->put('bfx_user_id', $input['user_id']);
            } else {
                $input['user_id'] = $request->session()->get('bfx_user_id');
            }
        } else {
            unset($input['user_id']);
        }

        if (array_key_exists('year', $input)) {
            $request->session()->put('bfx_year', $input['year']);
        } else {
            $input['year'] = $request->session()->get('year', date('Y'));
        }   

        if (array_key_exists('month', $input)) {
            $request->session()->put('bfx_month', $input['month']);
        } else {
            $input['month'] = $request->session()->get('month');
        } 

        if (array_key_exists('day', $input)) {
            $request->session()->put('bfx_day', $input['day']);
        } else {
            $input['day'] = $request->session()->get('day');
        }          

        $query = BfxOrderLogs::where('amount', '>', 0)
            // ->whereIn('status', [1, 2])
            ->where('status', 2)
            ->orderBy('updated_at', 'DESC');

        if (isset($input['user_id']) && !empty($input['user_id'])) {
            $query->where('user_id', $input['user_id']);
        } else {
            $query->where('user_id', $user_id);
        } 

        if (!empty($input['year'])) {
            $query->where(\DB::raw('YEAR(updated_at)'), $input['year']);
        }      

        if (!empty($input['month'])) {
            $query->where(\DB::raw('MONTH(updated_at)'), $input['month']);
        }  

        if (!empty($input['day'])) {
            $query->where(\DB::raw('DAY(updated_at)'), $input['day']);
        }          

        // \Log::debug($query->toSql());          

        $data = $query->get();

        $summary = [];
        $bfx1 = new Bfx1('xxx', 'yyy');
        foreach ($data as $row) {
            if ($row->status == 1) {
                if (! isset($this->tickers[$row->symbol])) {
                    $this->tickers[$row->symbol] = $bfx1->getTicker($row->symbol);
                }

                $row->sell_price = $this->tickers[$row->symbol]['last_price'];
            }

            $amount_buy  = ($row->amount * $row->buy_price) + ($row->amount * $row->buy_price) * env('TRADING_FEE', 0.25) / 100;
            $amount_sell = ($row->amount * $row->sell_price) - ($row->amount * $row->sell_price) * env('TRADING_FEE', 0.25) / 100;

            $summary[$row->symbol]['orders'][$row->id] = ['amount_buy' => $amount_buy, 'amount_sell' => $amount_sell];
            if (!isset($summary[$row->symbol]['sub_total_buy'])) {
                $summary[$row->symbol]['sub_total_buy'] = 0;
            }

            if (!isset($summary[$row->symbol]['sub_total_sell'])) {
                $summary[$row->symbol]['sub_total_sell'] = 0;
            }

            $summary[$row->symbol]['sub_total_buy'] += $amount_buy;
            $summary[$row->symbol]['sub_total_sell'] += $amount_sell;
        }

        $result['total_buy']         = 0;
        $result['total_sell']        = 0;
        $result['total_profit']      = 0;
        $result['total_profit_rate'] = 0;
        $result['items']             = [];
        $symbols                     = [];

        if (!empty($summary)) {
            $items = [];
            foreach ($summary as $key => $value) {
                $value['sub_profit']      = round($value['sub_total_sell'] - $value['sub_total_buy'], 2);
                $value['sub_profit_rate'] = round($value['sub_profit'] / $value['sub_total_buy'] * 100, 1);
                $items[$key]              = $value;
                $result['total_buy'] += $value['sub_total_buy'];
                $result['total_sell'] += $value['sub_total_sell'];
            }

            $result['total_buy']         = round($result['total_buy'], 2);
            $result['total_sell']        = round($result['total_sell'], 2);
            $result['total_profit']      = round($result['total_sell'] - $result['total_buy'], 2);
            $result['total_profit_rate'] = round($result['total_profit'] / $result['total_buy'] * 100, 1);
            $result['items']             = $items;
        }

        $page    = Input::get('page', 1);            // Get the ?page=1 from the url
        $perPage = env('ITEM_PER_PAGE', 20); // Number of items per page
        $offset  = ($page * $perPage) - $perPage;

        $symbols = new LengthAwarePaginator(
            array_slice($result['items'], $offset, $perPage, true),  // Only grab the items we need
            count($result['items']),                                 // Total items
            $perPage,                                                // Items per page
            $page,                                                   // Current page
            ['path' => $request->url(), 'query' => $request->query()]// We need this so we can keep all old query parameters from the url
        );

        return view('bfx.index', ['input' => $input, 'result' => $result, 'symbols' => $symbols, 'userList' => $this->userList]);
    }
}
