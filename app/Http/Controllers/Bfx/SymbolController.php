<?php

namespace App\Http\Controllers\Bfx;

use App\BfxSymbols;
use App\BfxTimeframes;
use App\BfxUserSymbols;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Validator;
use App\User;
use Illuminate\Support\Arr;

class SymbolController extends Controller
{
    
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');

        $key  = 'bfx_timeframes';
        $data = Cache::rememberForever($key, function () {
            $res        = [];
            $timeframes = BfxTimeframes::where('status', 1)->get();
            foreach ($timeframes as $key => $value) {
                $res[$value->time_frame] = $value->time_frame_name;
            }

            return json_encode($res);
        });

        $this->timeframes = json_decode($data, true);

        $userList       = User::select('id AS user_id', 'name')->get();
        $this->userList = Arr::pluck($userList, 'name', 'user_id');

        // \Log::debug($this->userList);        
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $input = Input::get();
        $user_id = Auth::id();

        if ($user_id == 1) {
            if (array_key_exists('user_id', $input)) {
                $request->session()->put('bfx_user_id', $input['user_id']);
            } else {
                $input['user_id'] = $request->session()->get('bfx_user_id');
            }
        } else {
            unset($input['user_id']);
        }

        $query = BfxUserSymbols::whereIn('time_frame', array_keys($this->timeframes));

        if (isset($input['user_id']) && !empty($input['user_id'])) {
            $query->where('user_id', $input['user_id']);
        } else {
            $query->where('user_id', $user_id);
        }            

        if (array_key_exists('search_symbol', $input)) {
            $request->session()->put('bfx_search_symbol', $input['search_symbol']);
        } else {
            $input['search_symbol'] = $request->session()->get('bfx_search_symbol');
        }

        if (array_key_exists('search_time_frame', $input)) {
            $request->session()->put('bfx_search_time_frame', $input['search_time_frame']);
        } else {
            $input['search_time_frame'] = $request->session()->get('bfx_search_time_frame');
        }

        $userSymbols = $query->get();

        $symbols  = [];
        $tmpSymbols = [];
        foreach ($userSymbols as $key => $value) {
            $tmpSymbols[$value->symbol][$value->time_frame] = $value;
        }

        $symbolList = BfxSymbols::getList();
        foreach ($symbolList as $key => $value) {
            if (isset($input['search_symbol'])) {
                $pos = strpos($key, $input['search_symbol']);
                if ($pos === false) {
                    continue;
                }
            }

            if (isset($tmpSymbols[$key])) {
                foreach ($tmpSymbols[$key] as $key2 => $value2) {
                    if (isset($input['search_time_frame']) && $input['search_time_frame'] != $value2->time_frame) {
                        continue;
                    }
                    $symbols[] = $value2;
                }

            } else {
                $symbol             = new BfxUserSymbols();
                $symbol->symbol     = $key;
                $symbol->time_frame = array_keys($this->timeframes)[0];
                $symbol->status     = 2;
                $symbols[]          = $symbol;
            }
        }

        if (array_key_exists('page', $input)) {
            $page = $input['page'];
            $request->session()->put('bfx_page', $input['page']);
        } else {
            $page = $request->session()->get('bfx_page', 1);
        }

        $perPage = env('ITEM_PER_PAGE', 20); // Number of items per page
        $offset  = ($page * $perPage) - $perPage;

        $data = new LengthAwarePaginator(
            array_slice($symbols, $offset, $perPage, true),          // Only grab the items we need
            count($symbols),                                         // Total items
            $perPage,                                                // Items per page
            $page,                                                   // Current page
            ['path' => $request->url(), 'query' => $request->query()]// We need this so we can keep all old query parameters from the url
        );

        foreach ($data as $key => $value) {
            $options = json_decode(base64_decode($value->options), true);
            if (!is_null($options) && is_array($options)) {
                $value->setting = implode(" | ", $options);
            }
        }        

        return view('bfx.symbol.index', ['input' => $input, 'symbols' => $data, 'timeframes' => $this->timeframes, 'userList' => $this->userList]);
    }

    /**
     * Set symbols setting.
     *
     * @return \Illuminate\Http\Response
     */
    public function set(Request $request)
    {
        $fields                          = __('bfx.field');
        $fields['time_frame']['options'] = $this->timeframes;

        $input = Input::get();

        if (isset($input['base_currency']) && $input['base_currency'] == 'BTC') {
            $fields['symbol']['options'] = BfxSymbols::getList('BTC');                      
            $defSymbol = 'ETHBTC';  
        } else {
            $fields['symbol']['options'] = BfxSymbols::getList('USD');
            $defSymbol = 'ETHUSD';        
        }

        if (!isset($input['time_frame'])) {
            // $input['time_frame'] = '30m';
            $input['time_frame'] = '1D';
        }              

        if (!isset($input['status'])) {
            $input['status'] = 2;
        }

        if (!isset($input['opt_bb_upper_status'])) {
            $input['opt_bb_upper_status'] = 2;
        }

        if (!isset($input['opt_bb_lower_status'])) {
            $input['opt_bb_lower_status'] = 2;
        }                

        $user_id = Auth::id();
        if (Auth::id() == 1 && isset($input['user_id'])) {
            $user_id = $input['user_id'];
        }

        $symbol = BfxUserSymbols::where('user_id', $user_id)
            ->where('symbol', $defSymbol)
            ->where('time_frame', $input['time_frame'])
            ->first();      

        if ($symbol && !is_null($symbol->options)) {
            $options = json_decode(base64_decode($symbol->options), true);
            if (count($options) > 0) {
                foreach ($options as $key => $value) {
                    $symbol->$key = $value;
                }
            }
        }

        if ($request->isMethod('post')) {
            // Validate
            // Read more on validation at http://laravel.com/docs/validation

            $rules = [
                    'status' => 'integer|in:1,2'
                ];

            if ($input['status'] == 1) {
                $rules += [
                    'symbol'            => 'required|array',
                    'base_amount_limit' => 'required|numeric|min:20|max:999999',
                    'base_amount'       => 'required|numeric|min:20|max:999999',
                ];

                if ($input['opt_bb_upper_status'] == 1) {
                    $rules += [
                        'opt_bb_upper_price_rate_cond1'     => 'required|numeric|min:1|max:999',
                        'opt_bb_upper_price_rate_cond2'     => 'required|numeric|min:1|max:999',
                        'opt_bb_upper_min_profit_rate_cond' => 'required|numeric|min:1|max:999',
                    ];
                }

                if ($input['opt_bb_lower_status'] == 1) {
                    $rules += [
                        'opt_bb_lower_price_rate_cond1'     => 'required|numeric|min:1|max:999',
                        'opt_bb_lower_price_rate_cond2'     => 'required|numeric|min:1|max:999',
                        'opt_bb_lower_min_profit_rate_cond' => 'required|numeric|min:1|max:999',
                    ];
                }                
            } else {
                $rules = [
                    'base_amount_limit' => 'required|numeric|min:20|max:999999',
                    'base_amount'       => 'required|numeric|min:20|max:999999',
                ];
            }

            $validator = Validator::make($input, $rules);

            // Process validate fail
            if ($validator->fails()) {
                return Redirect::route('bfx.symbol.set')
                    ->withErrors($validator)
                    ->withInput();
            } else {
                $options = [];
                foreach (__('btx.options.opt_bb_upper') as $key => $value) {
                    $options['opt_bb_upper_' . $key] = isset($input['opt_bb_upper_' . $key]) ? $input['opt_bb_upper_' . $key] : null;
                }

                foreach (__('btx.options.opt_bb_lower') as $key => $value) {
                    $options['opt_bb_lower_' . $key] = isset($input['opt_bb_lower_' . $key]) ? $input['opt_bb_lower_' . $key] : null;
                }                 

                $input['options'] = base64_encode(json_encode($options));

                foreach ($input['symbol'] as $key => $value) {
                    $symbol = BfxUserSymbols::where('user_id', $user_id)
                        ->where('symbol', $key)
                        ->where('time_frame', $input['time_frame'])
                        ->first();

                    if (!$symbol) {
                        $symbol          = new BfxUserSymbols();
                        $symbol->user_id = $user_id;
                        $symbol->symbol  = $key;
                    }

                    // Create / Edit
                    unset($input['symbol']);
                    $symbol->fill($input);
                    $symbol->save();
                }

                // Clear user cache
                Cache::forget('bfx_users');
                Cache::forget('bfx_user_symbols');
                Cache::forget('bfx_user_symbols_USD');
                Cache::forget('bfx_user_symbols_BTC');

                // Redirect
                Session::flash('message', 'Successfully updated !');
                return Redirect::route('bfx.symbol', ['time_frame' => $input['time_frame']]);
            }
        }

        if ($symbol) {
            return view('bfx.symbol.set', ['action' => 'edit', 'fields' => $fields, 'input' => $input, 'data' => $symbol]);
        } else {
            return view('bfx.symbol.set', ['action' => 'create', 'fields' => $fields, 'input' => $input]);
        } 
    }

    /**
     * Delete symbol setting.
     *
     * @return \Illuminate\Http\Response
     */
    public function delete()
    {
        $input = Input::get();

        $user_id = Auth::id();
        if (Auth::id() == 1 && isset($input['user_id'])) {
            $user_id = $input['user_id'];
        }

        $symbol = BfxUserSymbols::where('user_id', $user_id)
            ->where('id', $input['id'])
            ->first();

        if ($symbol) {
            $symbol->delete();
        }

        // Clear user cache
        Cache::forget('bfx_users');

        // Redirect
        Session::flash('message', 'Successfully deleted !');
        return Redirect::route('bfx.symbol');
    }

    /**
     * Update default symbol setting.
     *
     * @return \Illuminate\Http\Response
     */
    function default() {
        $input = Input::get();

        $user_id = Auth::id();
        if (Auth::id() == 1 && isset($input['user_id'])) {
            $user_id = $input['user_id'];
        }

        $symbols = BfxUserSymbols::where('user_id', '=', 1)->get();

        foreach ($symbols as $key => $value) {
            $symbol = BfxUserSymbols::where('user_id', $user_id)
                ->where('symbol', $value->symbol)
                ->where('time_frame', $value->time_frame)
                ->first();

            if (!$symbol) {
                $symbol             = new BfxUserSymbols();
                $symbol->user_id    = $user_id;
                $symbol->symbol     = $value->symbol;
                $symbol->time_frame = $value->time_frame;

            }

            $symbol->base_amount_limit = $value->base_amount_limit;
            $symbol->base_amount       = $value->base_amount;
            $symbol->stoploss_rate     = $value->stoploss_rate;
            $symbol->status            = $value->status;
            $symbol->options           = $value->options;
            $symbol->save();
        }

        // Clear user cache
        Cache::forget('bfx_users');

        // Redirect
        Session::flash('message', 'Successfully updated !');
        return Redirect::route('bfx.symbol');
    }

}
