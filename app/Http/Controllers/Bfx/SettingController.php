<?php

namespace App\Http\Controllers\Bfx;

use App\BfxSymbols;
use App\BfxTimeframes;
use App\BfxUserSymbols;
use App\Exchanges;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Request;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Validator;

class SettingController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return Redirect::route('bfx');
    }

    /**
     * Setting Api Key.
     *
     * @return \Illuminate\Http\Response
     */
    public function apikey()
    {

        $input   = Input::get();
        $user_id = Auth::id();

        if (!isset($input['exchange'])) {
            $input['exchange'] = 'bfx';
        }

        if (!isset($input['status'])) {
            $input['status'] = 2;
        }

        $exchange = Exchanges::where('user_id', $user_id)
            ->where('exchange', 'bfx')
            ->first();

        if (Request::isMethod('post')) {

            // Validate
            // Read more on validation at http://laravel.com/docs/validation
            $validator = Validator::make($input, Exchanges::$rules);

            // Process validate fail
            if ($validator->fails()) {
                return Redirect::route('bfx.setting.apikey')
                    ->withErrors($validator)
                    ->withInput();
            } else {
                if (!$exchange) {
                    // Create
                    $exchange          = new Exchanges();
                    $exchange->user_id = Auth::id();
                }

                // Create / Edit
                $exchange->fill($input);
                $exchange->save();

                // Clear user cache
                Cache::forget('bfx_users');

                // Redirect
                Session::flash('message', 'Successfully updated !');

                // return Redirect::route(' bfx.setting.apikey');
            }
        }

        // Edit
        if ($exchange) {
            return view('bfx.setting.apikey.edit', ['action' => 'edit', 'data' => $exchange]);
        }

        return view('bfx.setting.apikey.edit', ['action' => 'create', 'data' => []]);
    }

    /**
     * Setting Symbol.
     *
     * @return \Illuminate\Http\Response
     */
    public function symbol()
    {
        if (Auth::id() != 1) {
            abort(404);
        }

        $input = Input::get();

        if (isset($input['base_currency']) && $input['base_currency'] == 'BTC') {
            $symbolList = __('bfx.symbol.list.BTC');
            $symbols    = BfxSymbols::where('base_currency', 'BTC')->get();
        } else {
            $symbolList = __('bfx.symbol.list.USD');
            $symbols    = BfxSymbols::where('base_currency', 'USD')->get();
        }

        $tmp = [];
        foreach ($symbols as $key => $value) {
            $tmp[$value->symbol] = $value;
        }

        $data = [];
        foreach ($symbolList as $key => $value) {
            if (isset($tmp[$key])) {
                if (isset($input['symbol'][$key])) {
                    $tmp[$key]->status = 1;
                }
                $data[$key] = $tmp[$key];
            } else {
                $data[$key]                = new BfxSymbols();
                $data[$key]->base_currency = substr($key, -3);
                $data[$key]->symbol        = $key;
                if (isset($input['symbol'][$key])) {
                    $data[$key]->status = 1;
                }
            }
        }

        if (Request::isMethod('post')) {

            // Validate
            // Read more on validation at http://laravel.com/docs/validation
            $validator = Validator::make($input, BfxSymbols::$rules);

            // Process validate fail
            if ($validator->fails()) {
                return Redirect::route('bfx.setting.symbol')
                    ->withErrors($validator)
                    ->withInput();
            } else {

                foreach ($data as $symbol) {
                    if (!isset($input['symbol'][$symbol->symbol])) {
                        $symbol->status = 2;
                    }
                    $symbol->save();
                }

                // Remove symbols delisted.
                $symbol_list = trans('bfx.symbol.list.USD') + trans('bfx.symbol.list.BTC');
                BfxSymbols::whereNotIn('symbol', array_keys($symbol_list))->delete();
                BfxUserSymbols::whereNotIn('symbol', array_keys($symbol_list))->delete();

                // Clear user cache
                Cache::forget('bfx_symbols');

                // Redirect
                Session::flash('message', 'Successfully updated !');
            }
        }

        return view('bfx.setting.symbol.set', ['input' => $input, 'symbols' => $data]);
    }

    /**
     * Setting Time Frame.
     *
     * @return \Illuminate\Http\Response
     */
    public function timeframe()
    {
        if (Auth::id() != 1) {
            abort(404);
        }

        $input = Input::get();

        $tmp        = [];
        $timeframes = BfxTimeframes::get();
        foreach ($timeframes as $key => $value) {
            $tmp[$value->time_frame] = $value;
        }

        $timeframeList = trans('bfx.time_frame');

        $data = [];
        foreach ($timeframeList as $key => $value) {
            if (isset($tmp[$key])) {
                if (isset($input['timeframes'][$key])) {
                    $tmp[$key]->status = 1;
                }
                $data[$key] = $tmp[$key];
            } else {
                $data[$key]                  = new BfxTimeframes();
                $data[$key]->time_frame      = $key;
                $data[$key]->time_frame_name = $value;
                if (isset($input['timeframes'][$key])) {
                    $data[$key]->status = 1;
                }
            }
        }

        if (Request::isMethod('post')) {

            // Validate
            // Read more on validation at http://laravel.com/docs/validation
            $validator = Validator::make($input, BfxTimeframes::$rules);

            // Process validate fail
            if ($validator->fails()) {
                return Redirect::route('bfx.setting.timeframe')
                    ->withErrors($validator)
                    ->withInput();
            } else {

                foreach ($data as $timeframe) {
                    if (!isset($input['timeframes'][$timeframe->time_frame])) {
                        $timeframe->status = 2;
                    }
                    $timeframe->save();
                }

                // Clear user cache
                Cache::forget('bfx_timeframes');

                // Redirect
                Session::flash('message', 'Successfully updated !');

            }
        }

        return view('bfx.setting.timeframe.set', ['input' => $input, 'timeframes' => $data]);
    }

}
