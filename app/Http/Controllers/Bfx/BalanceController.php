<?php

namespace App\Http\Controllers\Bfx;

use App\Bfx1;
use App\Exchanges;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Input;
use App\User;
use Illuminate\Support\Arr;

class BalanceController extends Controller {
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct() {
        $this->middleware('auth');

        $userList       = User::select('id AS user_id', 'name')->get();
        $this->userList = Arr::pluck($userList, 'name', 'user_id');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request) {
        $input = Input::get();
        $user_id = Auth::id();

        if ($user_id == 1) {
            if (array_key_exists('user_id', $input)) {
                $request->session()->put('bfx_user_id', $input['user_id']);
            } else {
                $input['user_id'] = $request->session()->get('bfx_user_id');
            }
        } else {
            unset($input['user_id']);
        }

        $query = Exchanges::where('exchange', 'bfx');

        if (isset($input['user_id']) && !empty($input['user_id'])) {
            $query->where('user_id', $input['user_id']);
        } else {
            $query->where('user_id', $user_id);
        }        
        
        $exchange =  $query->firstOrFail();

        // Get user's api key
        $apiKey    = $exchange->api_key2;
        $apiSecret = $exchange->api_secret2;

        $bfx1 = new Bfx1($apiKey, $apiSecret);
        $res  = $bfx1->getBalances();

        $totalUSD    = 0;
        $totalBTC    = 0;
        $btcUsd      = 0;
        $tmpBalances = [];
        $balances    = [];

        // \Log::debug($res);

        if (!isset($res['error'])) {
            foreach ($res as $value) {
                if ($value['amount'] > 0) {
                    $value['reserved'] = $value['amount'] - $value['available'];
                    $value['total']    = $value['amount'];

                    $currency = $value['currency'] = strtoupper($value['currency']);

                    if ($currency == 'USD') {
                        $value['Est.USD']['reserved']  = $value['reserved'];
                        $value['Est.USD']['available'] = $value['available'];
                        $value['Est.USD']['total']     = $value['total'];
                    } else {
                        $ticker = $bfx1->getTicker($currency . 'USD');

                        $value['Est.USD']['reserved']  = isset($ticker['error']) ? 0 : round($value['reserved'] * $ticker['last_price'], 2);
                        $value['Est.USD']['available'] = isset($ticker['error']) ? 0 : round($value['available'] * $ticker['last_price'], 2);
                        $value['Est.USD']['total']     = isset($ticker['error']) ? 0 : round($value['total'] * $ticker['last_price'], 2);

                        if ($currency == 'BTC') {
                            $btcUsd = isset($ticker['error']) ? 0 : $ticker['last_price'];
                        }
                    }

                    if ($value['Est.USD']['total'] >= 1) {
                        $tmpBalances[] = $value;
                        $totalUSD += $value['Est.USD']['total'];
                    }
                }
            }

            if ($btcUsd == 0) {
                $ticker = $bfx1->getTicker('BTCUSD');
                $btcUsd = isset($ticker['error']) ? 0 : $ticker['last_price'];
            }

            if ($btcUsd > 0) {
                $totalBTC = round($totalUSD / $btcUsd, 8);
            }

            $page    = Input::get('page', 1);    // Get the ?page=1 from the url
            $perPage = env('ITEM_PER_PAGE', 20); // Number of items per page
            $offset  = ($page * $perPage) - $perPage;

            $balances = new LengthAwarePaginator(
                array_slice($tmpBalances, $offset, $perPage, true),      // Only grab the items we need
                count($tmpBalances),                                     // Total items
                $perPage,                                                // Items per page
                $page,                                                   // Current page
                ['path' => $request->url(), 'query' => $request->query()]// We need this so we can keep all old query parameters from the url
            );
        }

        return view('bfx.balance.index', ['input' => $input, 'balances' => $balances, 'totalUSD' => round($totalUSD, 2), 'totalBTC' => $totalBTC, 'userList' => $this->userList]);
    }

}
