<?php

namespace App\Http\Controllers\Bfx;

use App\Bfx1;
use App\BfxFailOrderLogs;
use App\BfxOrderLogs;
use App\Exchanges;
use App\Http\Controllers\Controller;
use App\User;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Request;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Log;

class OrderATController extends Controller
{

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');

        $userList       = User::select('id AS user_id', 'name')->get();
        $this->userList = Arr::pluck($userList, 'name', 'user_id');

        // \Log::debug($this->userList);
    }

    /**
     * Show Orders
     *
     * @return \Illuminate\Http\Response
     */
    public function index(\Illuminate\Http\Request $request)
    {
        $input  = Input::get();
        $userId = Auth::id();

        if ($userId == 1) {
            if (array_key_exists('user_id', $input)) {
                $request->session()->put('bfx_user_id', $input['user_id']);
            } else {
                $input['user_id'] = $request->session()->get('bfx_user_id');
            }

            if (!empty($input['user_id'])) {
                $userId = $input['user_id'];
            }

        } else {
            unset($input['user_id']);
        }

        if (array_key_exists('search_symbol', $input)) {
            $request->session()->put('bfx_search_order_symbol', $input['search_symbol']);
        } else {
            $input['search_symbol'] = $request->session()->get('bfx_search_order_symbol', null);
        }

        if (array_key_exists('search_time_frame', $input)) {
            $request->session()->put('bfx_search_order_time_frame', $input['search_time_frame']);
        } else {
            $input['search_time_frame'] = $request->session()->get('bfx_search_order_time_frame', null);
        }

        if (array_key_exists('search_status', $input)) {
            $request->session()->put('bfx_search_order_status', $input['search_status']);
        } else {
            $input['search_status'] = $request->session()->get('bfx_search_order_status', 1);
        }

        if ($input['search_status'] != 'f') {
            $query = BfxOrderLogs::where('status', $input['search_status']);
        } else {
            $query = BfxFailOrderLogs::select();
        }

        $query->where('user_id', $userId);

        if (isset($input['search_time_frame'])) {
            $query->where('time_frame', $input['search_time_frame']);
        } else {
            $input['search_time_frame'] = '';
        }        

        if (isset($input['search_symbol'])) {
            $query->where('symbol', 'like', '%' . $input['search_symbol'] . '%');
        } else {
            $input['search_symbol'] = '';
        }

        // \Log::debug($input);
        // \Log::debug($query->toSql());

        $perPage = env('ITEM_PER_PAGE', 20); // Number of items per page
        $orders  = $query->orderBy('updated_at', 'desc')
                        ->orderBy('created_at', 'desc')->paginate($perPage);

        return view('bfx.order-at.index', ['input' => $input, 'orders' => $orders, 'userList' => $this->userList]);
    }

    /**
     * Edit Order
     *
     * @return \Illuminate\Http\Response
     */
    public function edit()
    {
        $input = Input::get();

        if (!isset($input['id'])) {
            abort(404);
        }

        $userId = Auth::id();
        if (Auth::id() == 1 && isset($input['user_id'])) {
            $userId = $input['user_id'];
        }

        $order = BfxOrderLogs::where('user_id', '=', $userId)
            ->where('id', $input['id'])
            ->where('status', 1)
            ->firstOrFail();

        if (Request::isMethod('post')) {

            if (substr($order->condition_type, 0, 8) == 'upper BB') {
                if ($order->sell_status == 1) {
                    $rules = [
                        'buy_price' => 'required|numeric',
                    ];
                } else {
                    $rules = [
                        'sell_price' => 'required|numeric',
                    ];
                }
            } else {
                if ($order->buy_status == 1) {
                    $rules = [
                        'sell_price' => 'required|numeric',
                    ];
                } else {
                    $rules = [
                        'buy_price' => 'required|numeric',
                    ];
                }
            } 

            // Validate
            // Read more on validation at http://laravel.com/docs/validation
            $validator = Validator::make($input, $rules);

            // Process validate fail
            if ($validator->fails()) {
                return Redirect::route('bfx.order-at.edit', ['user_id' => $input['user_id'], 'id' => $input['id']])
                    ->withErrors($validator)
                    ->withInput();
            } else {
                if ($order->sell_price != $input['sell_price'] || $order->buy_price != $input['buy_price']) {
                    $exchange = Exchanges::where('user_id', $userId)
                        ->where('exchange', 'bfx')
                        ->first();

                    // Get user's api key
                    $apiKey    = $exchange->api_key2;
                    $apiSecret = $exchange->api_secret2;

                    // Init object class
                    $bfx1 = new Bfx1($apiKey, $apiSecret);

                    if (substr($order->condition_type, 0, 8) == 'upper BB') {
                        if ($order->sell_status == 1) {
                            if ($order->buy_order_id == null) {
                                $buyOrder = $bfx1->newOrder($order->symbol, $order->amount, $input['buy_price'], 'bitfinex', 'buy', 'exchange limit');

                                // Log::channel('bfx')->info($buyOrder);

                                if (isset($buyOrder['order_id'])) {
                                    $order->buy_order_id = $buyOrder['order_id'];
                                    $order->buy_price    = $input['buy_price']; 
                                    $order->profit_rate  = str_replace('%', '', $input['profit_rate']);
                                    $order->save();
                                    Session::flash('message', 'Successfully updated !');
                                } else {
                                    $order->buy_order_id = null;
                                    $order->buy_price    = null;
                                    $order->profit_rate  = null;
                                    $order->save();
                                    Session::flash('message', 'Update failed !');
                                }
                            } else {
                                // Get buy order
                                $buyOrder = $bfx1->getOrder($order->buy_order_id);
                                // Buy order is open
                                if ($buyOrder['is_live']) {
                                    // Execute cancel buy order
                                    $cancelOrder = $bfx1->cancelOrder($order->buy_order_id);
                                    usleep(100000);
                                    $buyOrder = $bfx1->newOrder($order->symbol, $order->amount, $input['buy_price'], 'bitfinex', 'buy', 'exchange limit');
                                    if (isset($buyOrder['order_id'])) {
                                        $order->buy_order_id = $buyOrder['order_id'];
                                        $order->buy_price    = $input['buy_price']; 
                                        $order->profit_rate  = str_replace('%', '', $input['profit_rate']);
                                        $order->save();
                                        Session::flash('message', 'Successfully updated !');
                                    } else {
                                        $order->buy_order_id = null;
                                        $order->buy_price    = null;
                                        $order->profit_rate  = null;
                                        $order->save();
                                        Session::flash('message', 'Update failed !');
                                    }
                                } else {
                                    $order->amount = $buyOrder['executed_amount'];
                                    // Calc and set profit rate
                                    $order->profit_rate = round(($order->sell_price - $order->buy_price) / $order->buy_price * 100, 1, PHP_ROUND_HALF_DOWN) - (env('TRADING_FEE', 0.25) * 2);
                                    $order->status = 2;
                                    $order->save();
                                    Session::flash('message', 'Successfully updated - Order closed !');
                                }
                            }
                        } else {
                            // Get sell order
                            $sellOrder = $bfx1->getOrder($order->sell_order_id);
                            // Sell order is open
                            if ($sellOrder['is_live']) {
                                // Execute cancel sell order
                                $cancelOrder = $bfx1->cancelOrder($order->sell_order_id);
                                usleep(100000);
                                $sellOrder = $bfx1->newOrder($order->symbol, $order->amount, $input['sell_price'], 'bitfinex', 'sell', 'exchange limit');
                                if (isset($sellOrder['order_id'])) {
                                    $order->sell_order_id = $sellOrder['order_id'];
                                    $order->sell_price    = $input['sell_price']; 
                                    $order->amount        = $sellOrder['executed_amount'];
                                    $order->profit_rate   = str_replace('%', '', $input['profit_rate']);
                                    $order->save();
                                    Session::flash('message', 'Successfully updated !');
                                } else {
                                    $order->sell_order_id = null;
                                    $order->sell_price    = null;
                                    $order->profit_rate   = null;
                                    $order->save();
                                    Session::flash('message', 'Update failed !');
                                }
                            }
                        }
                    } else {
                        if ($order->buy_status == 1) {
                            if ($order->sell_order_id == null) {
                                $sellOrder = $bfx1->newOrder($order->symbol, $order->amount, $input['sell_price'], 'bitfinex', 'sell', 'exchange limit');

                                // Log::channel('bfx')->info($sellOrder);

                                if (isset($sellOrder['order_id'])) {
                                    $order->sell_order_id = $sellOrder['order_id'];
                                    $order->sell_price    = $input['sell_price'];
                                    $order->profit_rate   = str_replace('%', '', $input['profit_rate']);
                                    $order->save();
                                    Session::flash('message', 'Successfully updated !');
                                } else {
                                    $order->sell_order_id = null;
                                    $order->sell_price    = null;
                                    $order->profit_rate   = null;
                                    $order->save();
                                    Session::flash('message', 'Update failed !');
                                }
                            } else {
                                // Get sell order
                                $sellOrder = $bfx1->getOrder($order->sell_order_id);
                                // Sell order is open
                                if ($sellOrder['is_live']) {
                                    // Execute cancel sell order
                                    $cancelOrder = $bfx1->cancelOrder($order->sell_order_id);
                                    usleep(100000);
                                    $sellOrder = $bfx1->newOrder($order->symbol, $order->amount, $input['sell_price'], 'bitfinex', 'sell', 'exchange limit');
                                    if (isset($sellOrder['order_id'])) {
                                        $order->sell_order_id = $sellOrder['order_id'];
                                        $order->sell_price    = $input['sell_price']; 
                                        $order->profit_rate   = str_replace('%', '', $input['profit_rate']);
                                        $order->save();
                                        Session::flash('message', 'Successfully updated !');
                                    } else {
                                        $order->sell_order_id = null;
                                        $order->sell_price    = null;
                                        $order->profit_rate   = null;
                                        $order->save();
                                        Session::flash('message', 'Update failed !');
                                    }
                                } else {
                                    $order->amount = $sellOrder['executed_amount'];
                                    // Calc and set profit rate
                                    $order->profit_rate = round(($order->sell_price - $order->buy_price) / $order->buy_price * 100, 1, PHP_ROUND_HALF_DOWN) - (env('TRADING_FEE', 0.25) * 2);
                                    $order->status = 2;
                                    $order->save();
                                    Session::flash('message', 'Successfully updated - Order closed !');
                                }
                            }                            
                        } else {
                            // Get buy order
                            $buyOrder = $bfx1->getOrder($order->buy_order_id);
                            // Buy order is open
                            if ($buyOrder['is_live']) {
                                // Execute cancel buy order
                                $cancelOrder = $bfx1->cancelOrder($order->buy_order_id);
                                usleep(100000);
                                $buyOrder = $bfx1->newOrder($order->symbol, $order->amount, $input['buy_price'], 'bitfinex', 'buy', 'exchange limit');
                                if (isset($buyOrder['order_id'])) {
                                    $order->buy_order_id = $buyOrder['order_id'];
                                    $order->buy_price    = $input['buy_price']; 
                                    $order->amount       = $buyOrder['executed_amount'];
                                    $order->profit_rate  = str_replace('%', '', $input['profit_rate']);
                                    $order->save();
                                    Session::flash('message', 'Successfully updated !');
                                } else {
                                    $order->buy_order_id = null;
                                    $order->buy_price    = null;
                                    $order->profit_rate  = null;
                                    $order->save();
                                    Session::flash('message', 'Update failed !');
                                }
                            }
                        }
                    }

                    // $order->save();
                    // Session::flash('message', 'Successfully updated !');
                    // return Redirect::route('bfx.order-at', []);
                } else {
                    Session::flash('message', 'Please change price for updating !');
                }
            }
        }

        $fields = __('bfx.order_edit');

        if (substr($order->condition_type, 0, 8) == 'upper BB') {
            if ($order->sell_status == 1) {
                $fields['sell_price']['readonly'] = true;
            } else {
                $fields['buy_price']['readonly'] = true;
            }
        } else {
            if ($order->buy_status == 1) {
                $fields['buy_price']['readonly'] = true;
            } else {
                $fields['sell_price']['readonly'] = true;
            }
        }

        return view('bfx.order-at.edit', ['fields' => $fields, 'input' => $input, 'data' => $order]);
    }

    /**
     * New Order
     *
     * @return \Illuminate\Http\Response
     */
    public function new(\Illuminate\Http\Request $request)
    {
        $input = Input::get();

        $userId = Auth::id();
        if ($userId == 1) {
            if (array_key_exists('user_id', $input)) {
                $request->session()->put('bfx_user_id', $input['user_id']);
            } else {
                $input['user_id'] = $request->session()->get('bfx_user_id');
            }

            if (!empty($input['user_id'])) {
                $userId = $input['user_id'];
            }

        } else {
            unset($input['user_id']);
        }

        $fields                      = __('bfx.order_new');
        $fields['symbol']['options'] = ['' => '----'] + __('bfx.symbol.list.USD');

        return view('bfx.order-at.new', ['fields' => $fields, 'input' => $input, 'data' => (object) $input, 'userList' => $this->userList]);
    }    

    /**
     * New Order Confirm
     *
     * @return \Illuminate\Http\Response
     */
    public function newConfirm()
    {
        $input = Input::get();

        $rules = [
            'symbol' => 'required|string|min:3|max:10',
            'side'   => 'required|numeric',
            'price'  => 'required|numeric',
        ];

        if (empty($input['amount'])) {
            $rules['base_amount'] = 'required|numeric';
            $input['amount'] = $input['base_amount'] / $input['price'];
            if ($input['amount'] > 1) {
                $input['amount'] = round($input['amount'], 0, PHP_ROUND_HALF_DOWN);
            } else {
                $input['amount'] = round($input['amount'], 4, PHP_ROUND_HALF_DOWN);
            }           
        } else {
            $rules['amount'] = 'required|numeric';
            $input['base_amount'] = $input['amount'] * $input['price'];
            if ($input['base_amount'] > 1) {
                $input['base_amount'] = round($input['base_amount'], 0, PHP_ROUND_HALF_DOWN);
            } else {
                $input['base_amount'] = round($input['base_amount'], 4, PHP_ROUND_HALF_DOWN);
            }         
        }

        // Validate
        // Read more on validation at http://laravel.com/docs/validation
        $validator = Validator::make($input, $rules);

        // Process validate fail
        if ($validator->fails()) {
            return Redirect::route('bfx.order-at.new', [])
                ->withErrors($validator)
                ->withInput();
        }

        $fields                      = __('bfx.order_new');
        $fields['symbol']['options'] = ['' => '----'] + __('bfx.symbol.list.USD');   

        return view('bfx.order-at.new-confirm', ['fields' => $fields, 'input' => $input, 'userList' => $this->userList]);
    }

    /**
     * New Order Save
     *
     * @return \Illuminate\Http\Response
     */
    public function newSave() {
        $input = Input::get();

        $userId = Auth::id();
        if (Auth::id() == 1 && isset($input['user_id'])) {
            $userId = $input['user_id'];
        }

        if (Request::isMethod('post')) {

            $rules = [
                'symbol' => 'required|string|min:3|max:10',
                'side'   => 'required|numeric',
                'base_amount' => 'required|numeric',
                'price'  => 'required|numeric',
            ];

            // Validate
            // Read more on validation at http://laravel.com/docs/validation
            $validator = Validator::make($input, $rules);

            // Process validate fail
            if ($validator->fails()) {
                return Redirect::route('bfx.order-at.new', [])
                    ->withErrors($validator)
                    ->withInput();
            } else {

                $exchange = Exchanges::where('user_id', $userId)
                    ->where('exchange', 'bfx')
                    ->firstOrFail();

                // Get user's api key
                $apiKey    = $exchange->api_key;
                $apiSecret = $exchange->api_secret;

                // Init object class
                $bfx1 = new Bfx1($apiKey, $apiSecret);

                if ($input['side'] == 1) {
                    // Make buy order
                    $order = $bfx1->newOrder($input['symbol'], $input['amount'], $input['price'], 'bitfinex', 'buy', 'exchange limit');
                } else {
                    // Make sell order
                    $order = $bfx1->newOrder($input['symbol'], $input['amount'], $input['price'], 'bitfinex', 'sell', 'exchange limit');
                }

                if (isset($order['order_id'])) {
                    $data = [];
                    $data['user_id'] = $input['user_id'];
                    $data['symbol']  = $input['symbol'];
                    $data['time_frame'] = '1D';

                    if ($input['side'] == 1) {
                        $data['buy_order_id'] = $order['order_id'];
                        $data['buy_price']    = $order['price'];
                        // $data['buy_status']     = 1;
                        $data['amount'] = round(($order['executed_amount'] - $order['executed_amount'] * env('TRADING_FEE', 0.25) / 100), 8, PHP_ROUND_HALF_DOWN);
                        $data['condition_type'] = 'lower BB2';
                        $data['status']         = 1;
                        // Save order logs
                        $bfxOrderLogs = new BfxOrderLogs();
                        $bfxOrderLogs->fill($data);
                        $bfxOrderLogs->save();
                        // Set pre info log
                        // $preInfoLog = $data['user_id'] . ' : ' . $data['symbol'] . ' : ' . $data['buy_order_id'] . ' : ';
                        // Log::channel('bfx_order')->info($preInfoLog . 'make buy order success !');
                    } else {
                        $data['sell_order_id']  = $order['order_id'];
                        $data['sell_price']     = $order['price'];
                        // $data['sell_status']    = 1;
                        $data['condition_type'] = 'upper BB2';
                        $data['status']         = 1;
                        // Save order logs
                        $bfxOrderLogs = new BfxOrderLogs();
                        $bfxOrderLogs->fill($data);
                        $bfxOrderLogs->save();

                        // Set pre info log
                        // $preInfoLog = $data['user_id'] . ' : ' . $data['symbol'] . ' : ' . $data['sell_order_id'] . ' : ';
                        // Log::channel('bfx_order')->info($preInfoLog . 'make sell order success !');
                    }

                    // Log::channel('bfx_order')->info($buyOrder);            
                } else {
                    // Session::flash('message', 'Failed to make new order !');
                    // return Redirect::route('bfx.order-at.new', [])->withInput();                
                }
            }

            return view('bfx.order-at.new-complete', ['order' => $order]);
        }
    }    

}
