<?php

namespace App\Http\Controllers;

use App\Exchanges;
use Illuminate\Support\Facades\Auth;

class ExchangeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $exchanges = __('exchange.list');
        $data      = Exchanges::where('user_id', Auth::id())->get();

        foreach ($data as $key => $value) {
            if ($value->status == 1) {
                $exchanges[$value->exchange]['status'] = 1;
            } else {
                $exchanges[$value->exchange]['status'] = 2;
            }
        }

        return view('exchange.index', ['exchanges' => $exchanges]);
    }

}
