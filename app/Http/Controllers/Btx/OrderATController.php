<?php

namespace App\Http\Controllers\Btx;

use App\Btx1;
use App\BtxFailOrderLogs;
use App\BtxOrderLogs;
use App\Exchanges;
use App\Http\Controllers\Controller;
use App\User;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Request;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Log;

class OrderATController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');

        $userList       = User::select('id AS user_id', 'name')->get();
        $this->userList = Arr::pluck($userList, 'name', 'user_id');

        // \Log::debug($this->userList);
    }

    /**
     * Show Orders.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(\Illuminate\Http\Request $request)
    {
        $input  = Input::get();
        $userId = Auth::id();

        if ($userId == 1) {
            if (array_key_exists('user_id', $input)) {
                $request->session()->put('btx_user_id', $input['user_id']);
            } else {
                $input['user_id'] = $request->session()->get('btx_user_id');
            }

            if (!empty($input['user_id'])) {
                $userId = $input['user_id'];
            }

        } else {
            unset($input['user_id']);
        }

        if (array_key_exists('search_symbol', $input)) {
            $request->session()->put('btx_search_order_symbol', $input['search_symbol']);
        } else {
            $input['search_symbol'] = $request->session()->get('btx_search_order_symbol', null);
        }

        if (array_key_exists('search_time_frame', $input)) {
            $request->session()->put('btx_search_order_time_frame', $input['search_time_frame']);
        } else {
            $input['search_time_frame'] = $request->session()->get('btx_search_order_time_frame', null);
        }

        if (array_key_exists('search_status', $input)) {
            $request->session()->put('btx_search_order_status', $input['search_status']);
        } else {
            $input['search_status'] = $request->session()->get('btx_search_order_status', 1);
        }

        if ($input['search_status'] != 'f') {
            $query = BtxOrderLogs::where('status', $input['search_status']);
        } else {
            $query = BtxFailOrderLogs::select();
        }

        $query->where('user_id', $userId);

        if (isset($input['search_time_frame'])) {
            $query->where('time_frame', $input['search_time_frame']);
        } else {
            $input['search_time_frame'] = '';
        }

        if (isset($input['search_symbol'])) {
            $query->where('symbol', 'like', '%' . $input['search_symbol'] . '%');
        } else {
            $input['search_symbol'] = '';
        }

        // \Log::debug($input);
        // \Log::debug($query->toSql());

        $perPage = env('ITEM_PER_PAGE', 20); // Number of items per page
        $orders  = $query->orderBy('updated_at', 'desc')
                        ->orderBy('created_at', 'desc')->paginate($perPage);

        return view('btx.order-at.index', ['input' => $input, 'orders' => $orders, 'userList' => $this->userList]);
    }

    /**
     * Edit Order
     *
     * @return \Illuminate\Http\Response
     */
    public function edit()
    {
        $input = Input::get();

        if (!isset($input['id'])) {
            abort(404);
        }

        $userId = Auth::id();
        if (Auth::id() == 1 && isset($input['user_id'])) {
            $userId = $input['user_id'];
        }

        $order = BtxOrderLogs::where('user_id', '=', $userId)
            ->where('id', $input['id'])
            ->where('status', 1)
            ->firstOrFail();

        if (Request::isMethod('post')) {

            if (substr($order->condition_type, 0, 8) == 'upper BB') {
                if ($order->sell_status == 1) {
                    $rules = [
                        'buy_price' => 'required|numeric',
                    ];
                } else {
                    $rules = [
                        'sell_price' => 'required|numeric',
                    ];
                }
            } else {
                if ($order->buy_status == 1) {
                    $rules = [
                        'sell_price' => 'required|numeric',
                    ];
                } else {
                    $rules = [
                        'buy_price' => 'required|numeric',
                    ];
                }
            }            

            // Validate
            // Read more on validation at http://laravel.com/docs/validation
            $validator = Validator::make($input, $rules);

            // Process validate fail
            if ($validator->fails()) {
                return Redirect::route('btx.order-at.edit', ['user_id' => $input['user_id'], 'id' => $input['id']])
                    ->withErrors($validator)
                    ->withInput();
            } else {
                if ($order->sell_price != $input['sell_price'] || $order->buy_price != $input['buy_price']) {
                    $exchange = Exchanges::where('user_id', $userId)
                        ->where('exchange', 'btx')
                        ->first();

                    // Get user's api key
                    $apiKey    = $exchange->api_key;
                    $apiSecret = $exchange->api_secret;

                    // Init object class
                    $btx1 = new Btx1($apiKey, $apiSecret);

                    if (substr($order->condition_type, 0, 8) == 'upper BB') {
                        if ($order->sell_status == 1) {
                            if ($order->buy_order_id == null) {
                                $buyOrder = $btx1->buyLimit($order->symbol, $order->amount, $input['buy_price']);
                                // \Log::channel('btx')->info($buyOrder);

                                if (isset($buyOrder['uuid'])) {
                                    $order->buy_order_id = $buyOrder['uuid'];
                                    $order->buy_price    = $input['buy_price']; 
                                    $order->profit_rate  = str_replace('%', '', $input['profit_rate']);
                                    $order->save();
                                    Session::flash('message', 'Successfully updated !');
                                } else {
                                    $order->buy_order_id = null;
                                    $order->buy_price    = null;
                                    $order->profit_rate  = null;
                                    $order->save();
                                    Session::flash('message', 'Update failed !');
                                }
                            } else {
                                // Get buy order
                                $buyOrder = $btx1->getOrder($order->buy_order_id);
                                // Buy order is open
                                if ($buyOrder['IsOpen'] && empty($buyOrder['Closed'])) {
                                    // Execute cancel order
                                    $cancelOrder = $btx1->cancel($order->buy_order_id);
                                    usleep(100000);
                                    $buyOrder = $btx1->buyLimit($order->symbol, $order->amount, $input['buy_price']);
                                    if (isset($buyOrder['uuid'])) {
                                        $order->buy_order_id = $buyOrder['uuid'];
                                        $order->buy_price    = $input['buy_price']; 
                                        $order->profit_rate  = str_replace('%', '', $input['profit_rate']);
                                        $order->save();
                                        Session::flash('message', 'Successfully updated !');                                 
                                    } else {
                                        $order->buy_order_id = null;
                                        $order->buy_price    = null;
                                        $order->profit_rate  = null;
                                        $order->save();
                                        Session::flash('message', 'Update failed !');
                                    }
                                } else {
                                    $order->status = 2;
                                    // Calc and set profit rate
                                    $order->profit_rate = round(($order->sell_price - $order->buy_price) / $order->buy_price * 100, 1, PHP_ROUND_HALF_DOWN) - (env('TRADING_FEE', 0.25) * 2);
                                    $order->save();
                                    Session::flash('message', 'Successfully updated - Order closed !');
                                }
                            }
                        } else {
                            // Get sell order
                            $sellOrder = $btx1->getOrder($order->sell_order_id);
                            // Buy order is open
                            if ($sellOrder['IsOpen'] && empty($sellOrder['Closed'])) {
                                // Execute cancel order
                                $cancelOrder = $btx1->cancel($order->sell_order_id);
                                usleep(100000);
                                $sellOrder = $btx1->sellLimit($order->symbol, $order->amount, $input['sell_price']);
                                if (isset($sellOrder['uuid'])) {
                                    $order->sell_order_id = $sellOrder['uuid'];
                                    $order->sell_price    = $input['sell_price']; 
                                    $order->profit_rate   = str_replace('%', '', $input['profit_rate']);
                                    $order->save();
                                    Session::flash('message', 'Successfully updated !');
                                } else {
                                    $order->sell_order_id = null;
                                    $order->sell_price    = null;
                                    $order->profit_rate   = null;
                                    $order->save();
                                    Session::flash('message', 'Update failed !');
                                }
                            }
                        }
                    } else {
                        if ($order->buy_status == 1) {
                            if ($order->sell_order_id == null) {
                                $sellOrder = $btx1->sellLimit($order->symbol, $order->amount, $input['sell_price']);

                                // \Log::channel('btx')->info($sellOrder);

                                if (isset($sellOrder['uuid'])) {
                                    $order->sell_order_id = $sellOrder['uuid'];
                                    $order->sell_price    = $input['sell_price']; 
                                    $order->profit_rate  = str_replace('%', '', $input['profit_rate']);
                                    $order->save();
                                    Session::flash('message', 'Successfully updated !');
                                } else {
                                    $order->sell_order_id = null;
                                    $order->sell_price    = null;
                                    $order->profit_rate   = null;
                                    $order->save();
                                    Session::flash('message', 'Update failed !');
                                }
                            } else {
                                // Get sell order
                                $sellOrder = $btx1->getOrder($order->sell_order_id);
                                // Buy order is open
                                if ($sellOrder['IsOpen'] && empty($sellOrder['Closed'])) {
                                    // Execute cancel order
                                    $cancelOrder = $btx1->cancel($order->sell_order_id);
                                    usleep(100000);
                                    $sellOrder = $btx1->sellLimit($order->symbol, $order->amount, $input['sell_price']);
                                    if (isset($sellOrder['uuid'])) {
                                        $order->sell_order_id = $sellOrder['uuid'];
                                        $order->sell_price    = $input['sell_price']; 
                                        $order->profit_rate   = str_replace('%', '', $input['profit_rate']);
                                        $order->save();
                                        Session::flash('message', 'Successfully updated !');                                    
                                    } else {
                                        $order->sell_order_id = null;
                                        $order->sell_price    = null;
                                        $order->profit_rate   = null;
                                        $order->save();
                                        Session::flash('message', 'Update failed !');
                                    }
                                } else {
                                    $order->status = 2;
                                    // Calc and set profit rate
                                    $order->profit_rate = round(($order->sell_price - $order->buy_price) / $order->buy_price * 100, 1, PHP_ROUND_HALF_DOWN) - (env('TRADING_FEE', 0.25) * 2);
                                    $order->save();
                                    Session::flash('message', 'Successfully updated - Order closed !');
                                }
                            }
                        } else {
                            // Get buy order
                            $buyOrder = $btx1->getOrder($order->buy_order_id);
                            // Buy order is open
                            if ($buyOrder['IsOpen'] && empty($buyOrder['Closed'])) {
                                // Execute cancel order
                                $cancelOrder = $btx1->cancel($order->buy_order_id);
                                usleep(100000);
                                $buyOrder = $btx1->buyLimit($order->symbol, $order->amount, $input['buy_price']);
                                if (isset($buyOrder['uuid'])) {
                                    $order->buy_order_id = $buyOrder['uuid'];
                                    $order->buy_price    = $input['buy_price']; 
                                    $order->profit_rate  = str_replace('%', '', $input['profit_rate']);
                                    $order->save();
                                    Session::flash('message', 'Successfully updated !');
                                } else {
                                    $order->buy_order_id = null;
                                    $order->buy_price    = null;
                                    $order->profit_rate  = null;
                                    $order->save();
                                    Session::flash('message', 'Update failed !');
                                }
                            }
                        }
                    }

                    // $order->save();
                    // Session::flash('message', 'Successfully updated !');
                    // return Redirect::route('btx.order-at', []);
                } else {
                    Session::flash('message', 'Please change price for updating !');
                }
            }
        }

        $fields = __('btx.order_edit');

        if (substr($order->condition_type, 0, 8) == 'upper BB') {
            if ($order->sell_status == 1) {
                $fields['sell_price']['readonly'] = true;
            } else {
                $fields['buy_price']['readonly'] = true;
            }
        } else {
            if ($order->buy_status == 1) {
                $fields['buy_price']['readonly'] = true;
            } else {
                $fields['sell_price']['readonly'] = true;
            }
        }

        return view('btx.order-at.edit', ['fields' => $fields, 'input' => $input, 'data' => $order]);
    }

    /**
     * New Order
     *
     * @return \Illuminate\Http\Response
     */
    public function new(\Illuminate\Http\Request $request)
    {
        $input = Input::get();

        $userId = Auth::id();
        if ($userId == 1) {
            if (array_key_exists('user_id', $input)) {
                $request->session()->put('btx_user_id', $input['user_id']);
            } else {
                $input['user_id'] = $request->session()->get('btx_user_id');
            }

            if (!empty($input['user_id'])) {
                $userId = $input['user_id'];
            }

        } else {
            unset($input['user_id']);
        }

        $fields                      = __('btx.order_new');
        $fields['symbol']['options'] = ['' => '----'] + __('btx.symbol.list.BTC') + __('btx.symbol.list.USD');

        return view('btx.order-at.new', ['fields' => $fields, 'input' => $input, 'data' => (object) $input, 'userList' => $this->userList]);
    }    

    /**
     * New Order Confirm
     *
     * @return \Illuminate\Http\Response
     */
    public function newConfirm()
    {
        $input = Input::get();

        $rules = [
            'symbol' => 'required|string|min:3|max:10',
            'side'   => 'required|numeric',
            'price'  => 'required|numeric',
        ];

        if (empty($input['amount'])) {
            $rules['base_amount'] = 'required|numeric';
            $input['amount'] = $input['base_amount'] / $input['price'];
            if ($input['amount'] > 1) {
                $input['amount'] = round($input['amount'], 0, PHP_ROUND_HALF_DOWN);
            } else {
                $input['amount'] = round($input['amount'], 4, PHP_ROUND_HALF_DOWN);
            }           
        } else {
            $rules['amount'] = 'required|numeric';
            $input['base_amount'] = $input['amount'] * $input['price'];
            if ($input['base_amount'] > 1) {
                $input['base_amount'] = round($input['base_amount'], 0, PHP_ROUND_HALF_DOWN);
            } else {
                $input['base_amount'] = round($input['base_amount'], 4, PHP_ROUND_HALF_DOWN);
            }         
        }        

        // Validate
        // Read more on validation at http://laravel.com/docs/validation
        $validator = Validator::make($input, $rules);

        // Process validate fail
        if ($validator->fails()) {
            return Redirect::route('btx.order-at.new', [])
                ->withErrors($validator)
                ->withInput();
        }

        $fields                      = __('btx.order_new');
        $fields['symbol']['options'] = ['' => '----'] + __('btx.symbol.list.BTC') + __('btx.symbol.list.USD');        

        return view('btx.order-at.new-confirm', ['fields' => $fields, 'input' => $input, 'userList' => $this->userList]);
    }

    /**
     * New Order Save
     *
     * @return \Illuminate\Http\Response
     */
    public function newSave() {
        $input = Input::get();

        $userId = Auth::id();
        if (Auth::id() == 1 && isset($input['user_id'])) {
            $userId = $input['user_id'];
        }

        if (Request::isMethod('post')) {

            $rules = [
                'symbol' => 'required|string|min:3|max:10',
                'side'   => 'required|numeric',
                'amount' => 'required|numeric',
                'price'  => 'required|numeric',
            ];

            // Validate
            // Read more on validation at http://laravel.com/docs/validation
            $validator = Validator::make($input, $rules);

            // Process validate fail
            if ($validator->fails()) {
                return Redirect::route('btx.order-at.new', [])
                    ->withErrors($validator)
                    ->withInput();
            } else {

                $exchange = Exchanges::where('user_id', $userId)
                    ->where('exchange', 'btx')
                    ->firstOrFail();

                // Get user's api key
                $apiKey    = $exchange->api_key;
                $apiSecret = $exchange->api_secret;

                // Init object class
                $btx1 = new Btx1($apiKey, $apiSecret);

                if ($input['side'] == 1) {
                    // Make buy order
                    $order = $btx1->buyLimit($input['symbol'], $input['amount'], $input['price']);
                } else {
                    // Make sell order
                    $order = $btx1->sellLimit($input['symbol'], $input['amount'], $input['price']);
                }

                if (isset($order['uuid'])) {
                    $data = [];
                    $data['user_id'] = $input['user_id'];
                    $data['symbol']  = $input['symbol'];
                    $data['amount']  = $input['amount'];
                    $data['time_frame'] = 'day';

                    if ($input['side'] == 1) {
                        $data['buy_order_id']   = $order['uuid'];
                        $data['buy_price']      = $input['price'];
                        // $data['buy_status']     = 1;
                        $data['condition_type'] = 'lower BB2';
                        $data['status']         = 1;
                        // Save order logs
                        $btxOrderLogs = new BtxOrderLogs();
                        $btxOrderLogs->fill($data);
                        $btxOrderLogs->save();
                        // Set pre info log
                        // $preInfoLog = $data['user_id'] . ' : ' . $data['symbol'] . ' : ' . $data['buy_order_id'] . ' : ';
                        // Log::channel('btx_order')->info($preInfoLog . 'make buy order success !');
                    } else {
                        $data['sell_order_id']  = $order['uuid'];
                        $data['sell_price']     = $input['price'];
                        // $data['sell_status']    = 1;
                        $data['condition_type'] = 'upper BB2';
                        $data['status']         = 1;
                        // Save order logs
                        $btxOrderLogs = new BtxOrderLogs();
                        $btxOrderLogs->fill($data);
                        $btxOrderLogs->save();                        
                        // Set pre info log
                        // $preInfoLog = $data['user_id'] . ' : ' . $data['symbol'] . ' : ' . $data['sell_order_id'] . ' : ';
                        // Log::channel('btx_order')->info($preInfoLog . 'make sell order success !');
                    }

                    // Log::channel('btx_order')->info($buyOrder);            
                } else {
                    // Session::flash('message', 'Failed to make new order !');
                    // return Redirect::route('btx.order-at.new', [])->withInput();                
                }
            }

            return view('btx.order-at.new-complete', ['order' => $order]);
        }
    }        
}
