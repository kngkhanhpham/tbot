<?php

namespace App\Http\Controllers\Btx;

use App\Btx1;
use App\BtxOrderLogs;
use App\Http\Controllers\Controller;
use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Input;
use App\User;
use Illuminate\Support\Arr;

class IndexController extends Controller
{
    private $tickers;
    
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
        $this->tickers = [];

        $userList       = User::select('id AS user_id', 'name')->get();
        $this->userList = Arr::pluck($userList, 'name', 'user_id');    
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(\Illuminate\Http\Request $request)
    {
        $input = Input::get();
        $user_id = Auth::id();

        if ($user_id == 1) {
            if (array_key_exists('user_id', $input)) {
                $request->session()->put('btx_user_id', $input['user_id']);
            } else {
                $input['user_id'] = $request->session()->get('btx_user_id');
            }
        } else {
            unset($input['user_id']);
        }

        if (array_key_exists('year', $input)) {
            $request->session()->put('btx_year', $input['year']);
        } else {
            $input['year'] = $request->session()->get('year', date('Y'));
        }   

        if (array_key_exists('month', $input)) {
            $request->session()->put('btx_month', $input['month']);
        } else {
            $input['month'] = $request->session()->get('month');
        } 

        if (array_key_exists('day', $input)) {
            $request->session()->put('btx_day', $input['day']);
        } else {
            $input['day'] = $request->session()->get('day');
        }        

        $query = BtxOrderLogs::where('amount', '>', 0)
            ->where('status', 2);

        if (isset($input['bc']) && $input['bc'] == 'USD') {
            $query->where(\DB::raw('SUBSTRING(symbol, 1, 4)'), 'USD-');
        }

        if (isset($input['user_id']) && !empty($input['user_id'])) {
            $query->where('user_id', $input['user_id']);
        } else {
            $query->where('user_id', $user_id);
        }

        if (!empty($input['year'])) {
            $query->where(\DB::raw('YEAR(updated_at)'), $input['year']);
        }      

        if (!empty($input['month'])) {
            $query->where(\DB::raw('MONTH(updated_at)'), $input['month']);
        }  

        if (!empty($input['day'])) {
            $query->where(\DB::raw('DAY(updated_at)'), $input['day']);
        }                       

        $data = $query->orderBy('updated_at', 'DESC')->get();

        $btx1 = new Btx1('xxx', 'yyy');

        // Base currency is USD
        $summary = [];
        foreach ($data as $row) {
            // if ($row->status == 1) {
            //     if (! isset($this->tickers[$row->symbol])) {
            //         $this->tickers[$row->symbol] = $btx1->getTicker($row->symbol);
            //     }

            //     $row->sell_price = $this->tickers[$row->symbol]['Last'];
            // }

            if (substr($row->symbol, 0, 4) != 'USD-') {

                continue;
            }

            $amount_buy  = ($row->amount * $row->buy_price) + ($row->amount * $row->buy_price) * env('TRADING_FEE', 0.25) / 100;
            $amount_sell = ($row->amount * $row->sell_price) - ($row->amount * $row->sell_price) * env('TRADING_FEE', 0.25) / 100;

            $summary[$row->symbol]['orders'][$row->id] = ['amount_buy' => $amount_buy, 'amount_sell' => $amount_sell];
            if (!isset($summary[$row->symbol]['sub_total_buy'])) {
                $summary[$row->symbol]['sub_total_buy'] = 0;
            }

            if (!isset($summary[$row->symbol]['sub_total_sell'])) {
                $summary[$row->symbol]['sub_total_sell'] = 0;
            }

            $summary[$row->symbol]['sub_total_buy'] += $amount_buy;
            $summary[$row->symbol]['sub_total_sell'] += $amount_sell;
        }

        $result['USD']['total_buy']         = 0;
        $result['USD']['total_sell']        = 0;
        $result['USD']['total_profit']      = 0;
        $result['USD']['total_profit_rate'] = 0;
        $result['USD']['items']             = [];
        // $symbols                     = [];

        if (!empty($summary)) {
            $items = [];
            foreach ($summary as $key => $value) {
                $value['sub_profit']      = round($value['sub_total_sell'] - $value['sub_total_buy'], 2);
                $value['sub_profit_rate'] = round($value['sub_profit'] / $value['sub_total_buy'] * 100, 1);
                $items[$key]              = $value;
                $result['USD']['total_buy'] += $value['sub_total_buy'];
                $result['USD']['total_sell'] += $value['sub_total_sell'];
            }

            $result['USD']['total_buy']         = round($result['USD']['total_buy'], 2);
            $result['USD']['total_sell']        = round($result['USD']['total_sell'], 2);
            $result['USD']['total_profit']      = round($result['USD']['total_sell'] - $result['USD']['total_buy'], 2);
            $result['USD']['total_profit_rate'] = round($result['USD']['total_profit'] / $result['USD']['total_buy'] * 100, 1);
            $result['USD']['items']             = $items;
        }

        // $page    = Input::get('page', 1);            // Get the ?page=1 from the url
        // $perPage = env('ITEM_PER_PAGE', 20); // Number of items per page
        // $offset  = ($page * $perPage) - $perPage;

        // $symbols['USD'] = new LengthAwarePaginator(
        //     array_slice($result['USD']['items'], $offset, $perPage, true),  // Only grab the items we need
        //     count($result['USD']['items']),                                 // Total items
        //     $perPage,                                                // Items per page
        //     $page,                                                   // Current page
        //     ['path' => $request->url(), 'query' => $request->query()]// We need this so we can keep all old query parameters from the url
        // );    

        // Base currency is BTC
        $summary = [];
        foreach ($data as $row) {
            // if ($row->status == 1) {
            //     if (! isset($this->tickers[$row->symbol])) {
            //         $this->tickers[$row->symbol] = $btx1->getTicker($row->symbol);
            //     }

            //     $row->sell_price = $this->tickers[$row->symbol]['Last'];
            // }

            if (mb_substr($row->symbol, 0, 4) != 'BTC-') {
                continue;
            }

            $amount_buy  = ($row->amount * $row->buy_price) + ($row->amount * $row->buy_price) * env('TRADING_FEE', 0.25) / 100;
            $amount_sell = ($row->amount * $row->sell_price) - ($row->amount * $row->sell_price) * env('TRADING_FEE', 0.25) / 100;

            $summary[$row->symbol]['orders'][$row->id] = ['amount_buy' => $amount_buy, 'amount_sell' => $amount_sell];
            if (!isset($summary[$row->symbol]['sub_total_buy'])) {
                $summary[$row->symbol]['sub_total_buy'] = 0;
            }

            if (!isset($summary[$row->symbol]['sub_total_sell'])) {
                $summary[$row->symbol]['sub_total_sell'] = 0;
            }

            $summary[$row->symbol]['sub_total_buy'] += $amount_buy;
            $summary[$row->symbol]['sub_total_sell'] += $amount_sell;
        }

        $result['BTC']['total_buy']         = 0;
        $result['BTC']['total_sell']        = 0;
        $result['BTC']['total_profit']      = 0;
        $result['BTC']['total_profit_rate'] = 0;
        $result['BTC']['items']             = [];
        // $symbols                     = [];

        if (!empty($summary)) {
            $items = [];
            foreach ($summary as $key => $value) {
                $value['sub_profit']      = round($value['sub_total_sell'] - $value['sub_total_buy'], 8);
                $value['sub_profit_rate'] = round($value['sub_profit'] / $value['sub_total_buy'] * 100, 1);
                $items[$key]              = $value;
                $result['BTC']['total_buy'] += $value['sub_total_buy'];
                $result['BTC']['total_sell'] += $value['sub_total_sell'];
            }

            $result['BTC']['total_buy']         = round($result['BTC']['total_buy'], 8);
            $result['BTC']['total_sell']        = round($result['BTC']['total_sell'], 8);
            $result['BTC']['total_profit']      = round($result['BTC']['total_sell'] - $result['BTC']['total_buy'], 8);
            $result['BTC']['total_profit_rate'] = round($result['BTC']['total_profit'] / $result['BTC']['total_buy'] * 100, 1);
            $result['BTC']['items']             = $items;
        }

        // $page    = Input::get('page', 1);            // Get the ?page=1 from the url
        // $perPage = env('ITEM_PER_PAGE', 20); // Number of items per page
        // $offset  = ($page * $perPage) - $perPage;

        // $symbols['BTC'] = new LengthAwarePaginator(
        //     array_slice($result['BTC']['items'], $offset, $perPage, true),  // Only grab the items we need
        //     count($result['BTC']['items']),                                 // Total items
        //     $perPage,                                                // Items per page
        //     $page,                                                   // Current page
        //     ['path' => $request->url(), 'query' => $request->query()]// We need this so we can keep all old query parameters from the url
        // );


        // echo "<pre/>";
        // var_dump($result['USD']['items']);
        // var_dump($result['BTC']['items']);

        return view('btx.index', ['input' => $input, 'result' => $result, 'userList' => $this->userList]);
    }
}
