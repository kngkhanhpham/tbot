<?php

namespace App\Http\Controllers\Btx;

use App\Exchanges;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Input;
use \App\Btx1;
use App\User;
use Illuminate\Support\Arr;

class BalanceController extends Controller {
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct() {
        $this->middleware('auth');

        $userList       = User::select('id AS user_id', 'name')->get();
        $this->userList = Arr::pluck($userList, 'name', 'user_id');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request) {
        $input = Input::get();
        $user_id = Auth::id();

        if ($user_id == 1) {
            if (array_key_exists('user_id', $input)) {
                $request->session()->put('bfx_user_id', $input['user_id']);
            } else {
                $input['user_id'] = $request->session()->get('bfx_user_id');
            }
        } else {
            unset($input['user_id']);
        }

        $query = Exchanges::where('exchange', 'btx');

        if (isset($input['user_id']) && !empty($input['user_id'])) {
            $query->where('user_id', $input['user_id']);
        } else {
            $query->where('user_id', $user_id);
        }  

        $exchange = $query->firstOrFail();

        // Get user's api key
        $apiKey    = $exchange->api_key;
        $apiSecret = $exchange->api_secret;

        $btx1 = new Btx1($apiKey, $apiSecret);
        $res  = $btx1->getBalances();

        $tmpBalances = [];
        $totalBTC    = 0;
        $totalUSD    = 0;
        foreach ($res as $value) {
            if ($value['Balance'] > 0) {
                $value['Balance']   = $value['Balance'] >= 1 ? $value['Balance'] : number_format($value['Balance'], 8, '.', '');
                $value['Available'] = $value['Available'] >= 1 ? $value['Available'] : number_format($value['Available'], 8, '.', '');
                $value['Pending']   = $value['Pending'] >= 1 ? $value['Pending'] : number_format($value['Pending'], 8, '.', '');
                $value['Reserved']  = $value['Balance'] - $value['Available'];
                $value['Reserved']  = $value['Reserved'] >= 1 ? $value['Reserved'] : number_format($value['Reserved'], 8, '.', '');
                $value['Total']     = $value['Balance'] + $value['Pending'];
                $value['Total']     = $value['Total'] >= 1 ? $value['Total'] : number_format($value['Total'], 8, '.', '');

                if ($value['Currency'] == 'BTC') {
                    $value['Est.BTC']['Reserved']  = $value['Reserved'];
                    $value['Est.BTC']['Available'] = $value['Available'];
                    $value['Est.BTC']['Total']     = $value['Total'];
                } elseif ($value['Currency'] == 'USDT') {
                    $ticker                        = $btx1->getTicker('USDT-BTC');
                    $value['Est.BTC']['Reserved']  = $value['Reserved'] / $ticker['Last'];
                    $value['Est.BTC']['Available'] = $value['Available'] / $ticker['Last'];
                    $value['Est.BTC']['Total']     = $value['Total'] / $ticker['Last'];
                } elseif ($value['Currency'] == 'USD') {
                    $ticker                        = $btx1->getTicker('USD-BTC');
                    $value['Est.BTC']['Reserved']  = $value['Reserved'] / $ticker['Last'];
                    $value['Est.BTC']['Available'] = $value['Available'] / $ticker['Last'];
                    $value['Est.BTC']['Total']     = $value['Total'] / $ticker['Last'];
                } else {
                    $ticker = $btx1->getTicker('BTC-' . $value['Currency']);
                    if (isset($ticker['success']) && $ticker['success'] == false) {
                        $value['Est.BTC']['Reserved']  = 0;
                        $value['Est.BTC']['Available'] = 0;
                        $value['Est.BTC']['Total']     = 0;
                    } else {
                        $value['Est.BTC']['Reserved']  = $value['Reserved'] * $ticker['Last'];
                        $value['Est.BTC']['Available'] = $value['Available'] * $ticker['Last'];
                        $value['Est.BTC']['Total']     = $value['Total'] * $ticker['Last'];
                    }

                }

                $value['Est.BTC']['Reserved']  = $value['Est.BTC']['Reserved'] >= 1 ? $value['Est.BTC']['Reserved'] : number_format($value['Est.BTC']['Reserved'], 8, '.', '');
                $value['Est.BTC']['Available'] = $value['Est.BTC']['Available'] >= 1 ? $value['Est.BTC']['Available'] : number_format($value['Est.BTC']['Available'], 8, '.', '');
                $value['Est.BTC']['Total']     = $value['Est.BTC']['Total'] >= 1 ? $value['Est.BTC']['Total'] : number_format($value['Est.BTC']['Total'], 8, '.', '');

                if ($value['Est.BTC']['Total'] > 0.00005000) {
                    $tmpBalances[] = $value;
                    $totalBTC += $value['Est.BTC']['Total'];
                }
            }
        }

        $ticker   = $btx1->getTicker('USDT-BTC');
        $totalUSD = round($totalBTC * $ticker['Last'], 2);

        $page    = Input::get('page', 1);    // Get the ?page=1 from the url
        $perPage = env('ITEM_PER_PAGE', 20); // Number of items per page
        $offset  = ($page * $perPage) - $perPage;

        $balances = new LengthAwarePaginator(
            array_slice($tmpBalances, $offset, $perPage, true),      // Only grab the items we need
            count($tmpBalances),                                     // Total items
            $perPage,                                                // Items per page
            $page,                                                   // Current page
            ['path' => $request->url(), 'query' => $request->query()]// We need this so we can keep all old query parameters from the url
        );

        return view('btx.balance.index', ['input' => $input, 'balances' => $balances, 'totalBTC' => $totalBTC, 'totalUSD' => $totalUSD, 'userList' => $this->userList]);
    }

}
