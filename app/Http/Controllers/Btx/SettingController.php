<?php

namespace App\Http\Controllers\Btx;

use App\BtxSymbols;
use App\BtxTimeframes;
use App\BtxUserSymbols;
use App\Exchanges;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Request;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Validator;

class SettingController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return Redirect::route('btx');
    }

    /**
     * Setting Api Key.
     *
     * @return \Illuminate\Http\Response
     */
    public function apikey()
    {

        $input   = Input::get();
        $user_id = Auth::id();

        if (!isset($input['exchange'])) {
            $input['exchange'] = 'btx';
        }

        if (!isset($input['status'])) {
            $input['status'] = 2;
        }

        $exchange = Exchanges::where('user_id', $user_id)
            ->where('exchange', 'btx')
            ->first();

        if (Request::isMethod('post')) {

            // Validate
            // Read more on validation at http://laravel.com/docs/validation
            $validator = Validator::make($input, Exchanges::$rules);

            // Process validate fail
            if ($validator->fails()) {
                return Redirect::route('btx.setting.apikey')
                    ->withErrors($validator)
                    ->withInput();
            } else {
                if (!$exchange) {
                    // Create
                    $exchange          = new Exchanges();
                    $exchange->user_id = Auth::id();
                }

                // Create / Edit
                $exchange->fill($input);
                $exchange->save();

                // Clear user cache
                Cache::forget('btx_users');

                // Redirect
                Session::flash('message', 'Successfully updated !');

                // return Redirect::route(' btx.setting.apikey');
            }
        }

        // Edit
        if ($exchange) {
            return view('btx.setting.apikey.edit', ['action' => 'edit', 'data' => $exchange]);
        }

        return view('btx.setting.apikey.edit', ['action' => 'create', 'data' => []]);
    }

    /**
     * Setting Symbol.
     *
     * @return \Illuminate\Http\Response
     */
    public function symbol()
    {
        if (Auth::id() != 1) {
            abort(404);
        }

        $input = Input::get();

        if (isset($input['base_currency']) && $input['base_currency'] == 'USD') {
            $symbolList = __('btx.symbol.list.USD');
            $symbols    = BtxSymbols::where('base_currency', 'USD')->get();
        } else {
            $symbolList = __('btx.symbol.list.BTC');
            $symbols    = BtxSymbols::where('base_currency', 'BTC')->get();
        }

        $tmp = [];
        foreach ($symbols as $key => $value) {
            $tmp[$value->symbol] = $value;
        }

        $data = [];
        foreach ($symbolList as $key => $value) {
            if (isset($tmp[$key])) {
                if (isset($input['symbol'][$key])) {
                    $tmp[$key]->status = 1;
                }
                $data[$key] = $tmp[$key];
            } else {
                $data[$key]                = new BtxSymbols();
                $data[$key]->base_currency = substr($key, 0, 3);
                $data[$key]->symbol        = $key;
                if (isset($input['symbol'][$key])) {
                    $data[$key]->status = 1;
                }
            }
        }

        if (Request::isMethod('post')) {

            // Validate
            // Read more on validation at http://laravel.com/docs/validation
            $validator = Validator::make($input, BtxSymbols::$rules);

            // Process validate fail
            if ($validator->fails()) {
                return Redirect::route('btx.setting.symbol')
                    ->withErrors($validator)
                    ->withInput();
            } else {
                foreach ($data as $symbol) {
                    if (!isset($input['symbol'][$symbol->symbol])) {
                        $symbol->status = 2;
                    }
                    $symbol->save();
                }

                // Remove symbols delisted.
                $symbol_list = trans('btx.symbol.list.USD') + trans('btx.symbol.list.BTC');
                BtxSymbols::whereNotIn('symbol', array_keys($symbol_list))->delete();
                BtxUserSymbols::whereNotIn('symbol', array_keys($symbol_list))->delete();

                // Clear user cache
                Cache::forget('btx_symbols');

                // Redirect
                Session::flash('message', 'Successfully updated !');
            }
        }

        return view('btx.setting.symbol.set', ['input' => $input, 'symbols' => $data]);
    }

    /**
     * Setting Time Frame.
     *
     * @return \Illuminate\Http\Response
     */
    public function timeframe()
    {
        if (Auth::id() != 1) {
            throw new Exception("Error Processing Request", 1);
        }

        $input = Input::get();

        $tmp        = [];
        $timeframes = BtxTimeframes::get();
        foreach ($timeframes as $key => $value) {
            $tmp[$value->time_frame] = $value;
        }

        $timeframeList = trans('btx.time_frame');

        $data = [];
        foreach ($timeframeList as $key => $value) {
            if (isset($tmp[$key])) {
                if (isset($input['timeframes'][$key])) {
                    $tmp[$key]->status = 1;
                }
                $data[$key] = $tmp[$key];
            } else {
                $data[$key]                  = new BtxTimeframes();
                $data[$key]->time_frame      = $key;
                $data[$key]->time_frame_name = $value;
                if (isset($input['timeframes'][$key])) {
                    $data[$key]->status = 1;
                }
            }
        }

        if (Request::isMethod('post')) {

            // Validate
            // Read more on validation at http://laravel.com/docs/validation
            $validator = Validator::make($input, BtxTimeframes::$rules);

            // Process validate fail
            if ($validator->fails()) {
                return Redirect::route('btx.setting.timeframe')
                    ->withErrors($validator)
                    ->withInput();
            } else {

                foreach ($data as $timeframe) {
                    if (!isset($input['timeframes'][$timeframe->time_frame])) {
                        $timeframe->status = 2;
                    }
                    $timeframe->save();
                }

                // Clear user cache
                Cache::forget('btx_timeframes');

                // Redirect
                Session::flash('message', 'Successfully updated !');

                // return Redirect::route(' btx.setting.apikey');
            }
        }

        return view('btx.setting.timeframe.set', ['input' => $input, 'timeframes' => $data]);
    }

}
