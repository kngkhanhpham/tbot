<?php

namespace App\Http\Controllers\Btx;

use App\Btx1;
use App\BtxOrders;
use App\Exchanges;
use App\Http\Controllers\Controller;
use App\User;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Request;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Validator;

class OrderMTController extends Controller {
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct() {
        $this->middleware('auth');

        $userList       = User::select('id AS user_id', 'name')->get();
        $this->userList = Arr::pluck($userList, 'name', 'user_id');

        // \Log::debug($this->userList);
    }

    /**
     * Show Orders.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(\Illuminate\Http\Request $request) {
        $input   = Input::get();
        $userId = Auth::id();

        if ($userId == 1) {
            if (array_key_exists('user_id', $input)) {
                $request->session()->put('btx_user_id', $input['user_id']);
                $userId = $input['user_id'];
            } else {
                $input['user_id'] = $request->session()->get('btx_user_id');
            }
        } else {
            unset($input['user_id']);
        }

        if (array_key_exists('search_symbol', $input)) {
            $request->session()->put('btx_search_order_symbol', $input['search_symbol']);
        } else {
            $input['search_symbol'] = $request->session()->get('btx_search_order_symbol');
        }

        if (array_key_exists('search_status', $input)) {
            $request->session()->put('btx_search_order_status', $input['search_status']);
        } else {
            $input['search_status'] = $request->session()->get('btx_search_order_status', 1);
        }

        $query = BtxOrders::where('status', $input['search_status']);

        if (isset($input['user_id']) && !empty($input['user_id'])) {
            $query->where('user_id', $input['user_id']);
        } else {
            $query->where('user_id', $userId);
        }

        if (isset($input['search_symbol'])) {
            $query->where('symbol', 'like', '%' . $input['search_symbol'] . '%');
        } else {
            $input['search_symbol'] = '';
        }

        $perPage = env('ITEM_PER_PAGE', 20); // Number of items per page
        $orders  = $query->orderBy('updated_at', 'desc')
                         ->orderBy('created_at', 'desc')->paginate($perPage);

        return view('btx.order-mt.index', ['input' => $input, 'orders' => $orders, 'userList' => $this->userList]);
    }

    /**
     * Edit Order
     *
     * @return \Illuminate\Http\Response
     */
    public function edit() {
        $input = Input::get();

        $order = null;

        if (isset($input['symbol']) && isset($input['side']) && isset($input['amount']) && isset($input['price'])) {
            $order = (object) $input;
        } else {
            if (isset($input['id'])) {
                $order = BtxOrders::where('user_id', '=', $userId)
                    ->where('id', $input['id'])
                    ->where('status', 1)
                    ->firstOrFail();
            }
        }

        $fields                      = __('btx.manual_order_edit');
        $fields['symbol']['options'] = ['' => '----'] + __('btx.symbol.list.BTC');

        if ($order != null) {
            return view('btx.order-mt.edit', ['fields' => $fields, 'input' => $input, 'action' => 'edit', 'data' => $order]);
        } else {
            return view('btx.order-mt.edit', ['fields' => $fields, 'input' => $input, 'action' => 'create', 'data' => []]);
        }
    }

    /**
     * Edit Confirm Order
     *
     * @return \Illuminate\Http\Response
     */
    public function editConfirm() {
        $input = Input::get();

        $rules = [
            'symbol' => 'required|string|min:3|max:10',
            'side'   => 'required|numeric',
            'amount' => 'required|numeric',
            'price'  => 'required|numeric',
        ];

        // Validate
        // Read more on validation at http://laravel.com/docs/validation
        $validator = Validator::make($input, $rules);

        // Process validate fail
        if ($validator->fails()) {
            return Redirect::route('btx.order-mt.edit', [])
                ->withErrors($validator)
                ->withInput();
        }        

        $fields = __('btx.manual_order_edit_confirm');

        $data = $input;
        $data['side'] = __('btx.manual_order_edit.side.options')[$input['side']];
        $data = (object)$data;

        // \Log::error($data);

        return view('btx.order-mt.edit-confirm', ['fields' => $fields, 'input' => $input, 'data' => $data]);
    }

    /**
     * Edit Save Order
     *
     * @return \Illuminate\Http\Response
     */
    public function editSave() {
        $input = Input::get();

        $userId = Auth::id();

        if (Request::isMethod('post')) {

            $rules = [
                'symbol' => 'required|string|min:3|max:10',
                'side'   => 'required|numeric',
                'amount' => 'required|numeric',
                'price'  => 'required|numeric',
            ];

            // Validate
            // Read more on validation at http://laravel.com/docs/validation
            $validator = Validator::make($input, $rules);

            // Process validate fail
            if ($validator->fails()) {
                return Redirect::route('btx.order-mt.edit', [])
                    ->withErrors($validator)
                    ->withInput();
            } else {

                $exchange = Exchanges::where('user_id', $userId)
                    ->where('exchange', 'btx')
                    ->firstOrFail();

                // Get user's api key
                $apiKey    = $exchange->api_key;
                $apiSecret = $exchange->api_secret;

                // Init object class
                $btx1 = new Btx1($apiKey, $apiSecret);

                if ($input['side'] == 1) {
                    // Make buy order
                    $newOrder = $btx1->buyLimit($input['symbol'], $input['amount'], $input['price']);
                } else {
                    // Make sell order
                    $newOrder = $btx1->sellLimit($input['symbol'], $input['amount'], $input['price']);
                }

                if (isset($newOrder['uuid'])) {
                    // Check order status
                    $checkOrder = $btx1->getOrder($newOrder['uuid']);
                    if ($checkOrder['IsOpen']) {
                        $input['status'] = 1;
                    } else {
                        $input['status'] = 2;
                    }

                    $input['user_id']  = $userId;
                    $input['order_id'] = $newOrder['uuid'];

                    $btxOrders = new BtxOrders();
                    $btxOrders->fill($input);
                    $btxOrders->save();

                    Session::flash('message', 'Successfully updated !');
                    return Redirect::route('btx.order-mt', [])->withInput();                    
                } else {
                    Session::flash('message', 'Failed make new order !');
                    return Redirect::route('btx.order-mt.edit', [])->withInput();                
                }
            }
        }
    }

    /**
     * Cancel Order
     *
     * @return \Illuminate\Http\Response
     */
    public function cancel() {
        $input = Input::get();

        $userId = Auth::id();

        $exchange = Exchanges::where('user_id', $userId)
            ->where('exchange', 'btx')
            ->firstOrFail();

        // Get user's api key
        $apiKey    = $exchange->api_key;
        $apiSecret = $exchange->api_secret;

        // Init object class
        $btx1 = new Btx1($apiKey, $apiSecret);

        $order = BtxOrders::where('user_id', '=', $userId)
            ->where('id', $input['id'])
            ->where('status', 1)
            ->firstOrFail();

        if ($order) {
            // Check order status
            $checkOrder = $btx1->getOrder($order->order_id);
            if ($checkOrder['IsOpen'] && empty($checkOrder['Closed'])) {
                // Cancel order
                $btx1->cancel($order->order_id);
                $order->status = 4;
                $order->save();
                Session::flash('message', 'Successfully canceled !');
                return Redirect::route('btx.order-mt', [])->withInput();  
            } else {
                $order->status = 4;
                $order->save();
                Session::flash('message', 'Successfully canceled !');
                return Redirect::route('btx.order-mt', [])->withInput();                 
            }
        }
    }
}
