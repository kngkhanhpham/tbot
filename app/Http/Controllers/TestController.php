<?php

namespace App\Http\Controllers;

use App\Bfx1;
use App\Bfx2;
use App\BfxOrderLogs;
use App\BtxOrderLogs;
use App\Btx1;
use App\Btx2;
use App\User;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Input;

class TestController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        // $this->middleware('auth');
    }

    public function resetpw()
    {
        $input = Input::get();
        $user_id = $input['user_id'];
        $pw = $input['pw'];
        
        $user = User::where('id', '=', $user_id)->first();
        $user->password = Hash::make($pw);
        $user->save();

        die('reset passwor success, please login then change your password new !');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        echo '<pre/>';

        // $buy_price = 0.53429;
        // $profit_rate = 1.2;
        // $sell_price = round($buy_price + $buy_price * ($profit_rate + env('TRADING_FEE', 0.25)) / 100, 8);
        // echo $sell_price;
        die;

        // $timestamp = 1536226200000;
        // $timestamp = substr($timestamp, 0, -3);
        // // echo $timestamp;
        // echo date('Y-m-d H:i:s', $timestamp);
        // echo '<br/>';

        // $timestamp = 1536224400000;
        // $timestamp = substr($timestamp, 0, -3);
        // echo date('Y-m-d H:i:s', $timestamp);
        // echo '<br/>';

        // $timestamp = 1536222600000;
        // $timestamp = substr($timestamp, 0, -3);
        // echo date('Y-m-d H:i:s', $timestamp);
        // echo '<br/>';

        // $timestamp = str_replace('T', ' ', '2018-08-29T15:31:02.793');
        // $timestamp = date("Y-m-d H:i:s", strtotime("+7 hours", strtotime($timestamp)));
        // var_dump($timestamp);
        die;

        // $five = date("Y-m-d H:i:s",strtotime("-5 minutes",strtotime(date('Y-m-d H:i:s'))));
        // print_r($five);
        // die;

        die('Test > index');
    }

    public function bfx_api()
    {
        echo '<pre/>';

        $users = User::with(['exchanges' => function ($query) {
            $query->where('exchange', '=', 'bfx')
                  ->where(function ($query) {
                      $query->where('status', '=', 1);
                  });
        }])
            ->with(['bfx_symbols' => function ($query) {
                $query->where('status', '=', 1);
            }])->get();

        foreach ($users as $user) {

            if (count($user->exchanges) == 0) {
                continue;
            }

            // Get user's api key
            $apiKey    = $user->exchanges[0]->api_key;
            $apiSecret = $user->exchanges[0]->api_secret;

            // Init object class
            $bfx1 = new Bfx1($apiKey, $apiSecret);
            $bfx2 = new Bfx2($apiKey, $apiSecret);

            if ($user->id == 1) {
            	
                // print_r($bfx1->cancelOrder(38575392612));
                // print_r($bfx1->newOrder('ETHUSD', 1.0025, 190, 'bitfinex', 'buy', 'exchange limit'));
                // print_r($bfx1->newOrder('ETHUSD', 1, 190, 'bitfinex', 'sell', 'exchange market'));
                // print_r($bfx1->newOrder('ETHUSD', 1.0025, 190, 'bitfinex', 'buy', 'exchange market'));
                // print_r($bfx1->newOrder('LTCUSD', 1.0025, 72, 'bitfinex', 'buy', 'exchange market'));
                // print_r($bfx1->newOrder('XMRUSD', 1.002501, 72, 'bitfinex', 'buy', 'exchange market'));
                // print_r($bfx1->getOrders());

                // $orderHistory = $bfx1->getOrdersHistory();
                // print_r($orderHistory);

                // die;

                $orderList = [];

                $orderHistory = $bfx1->getOrdersHistory();
                foreach ($orderHistory as $key => $value) {

                    if ($key == 7) {
                        break;
                    }

                    if ($value['side'] == 'buy' && in_array($value['symbol'], ['ethusd', 'ltcusd', 'xmrusd'])
                        && !$value['is_live'] && !$value['is_cancelled']) {
                        $orderList[] = $value;
                    }
                }

                $orderList = array_reverse($orderList);

                print_r($orderList); die();

                foreach ($orderList as $key => $value) {
                   
                    if ($value['side'] == 'buy' && in_array($value['symbol'], ['ethusd', 'ltcusd', 'xmrusd'])
                        && !$value['is_live'] && !$value['is_cancelled']) {
                    // if ($value['side'] == 'sell') {
                        // $value['timestamp'] = date('Y-m-d H:i:s', $value['timestamp']);
                        // print_r($value);

                        $bfxOrderLogs = new BfxOrderLogs();
                        
                        $checkExist = $bfxOrderLogs->where(
                            [
                                'user_id' => $user->id, 
                                'buy_order_id' => $value['id'], 
                            ]
                        )->first();

                        if ($checkExist) {
                            // echo '<br/>' . $checkExist->sell_order_id;
                            // $checkExist->message = null;
                            // $checkExist->created_at = $value['timestamp'];
                            // $checkExist->save();
                        }

                        if (!$checkExist) {
                            echo '<br/>' . $value['id'];
                            $data['user_id']    = $user->id;
                            $data['symbol']     = strtoupper($value['symbol']);
                            $data['time_frame'] = '1D';
                            $data['buy_order_id'] = $value['id'];
                            $data['buy_price']    = $value['price'];
                            $data['buy_status']    = 1;
                            $data['amount']    = $value['original_amount'];
                            $data['profit_rate'] = null;
                            $data['condition_type'] = 'lower BB2';
                            $data['price_rate'] = 22;
                            $data['price_rate_cond'] = 22;
                            $data['status'] = 1;
                            // $data['created_at'] = $value['timestamp'];

                            $bfxOrderLogs->fill($data);
                            $bfxOrderLogs->save();
                        }

                    }
                }

                // $sell_order = $bfx1->newOrder('neousd', '10.4895', '24.5', 'bitfinex', 'sell', 'exchange limit');
                // $sell_order = $bfx1->newOrder('eosusd', '8.1201111', 123.45, 'bitfinex', 'sell', 'exchange market');
                // print_r($sell_order);

                // print_r($bfx1->cancelOrder(17472886262));
                
                // print_r($bfx1->getOrders());

                // $order = $bfx1->getOrder(16930419614);
                // print_r($order);

                // $balances_res = $bfx1->getBalances();
                // print_r($balances_res);

                // $candles = $bfx2->getCandles('tBTCUSD');
                
                // echo '<br/> SMA : ';
                // print_r($this->calc_sma($candles));

                // echo '<br/> EMA : ';
                // print_r($this->calc_ema($candles));                

                // echo '<br/> BB : ';
                // print_r($this->calc_bb($candles));

                // echo '<br/>MACD : ';
                // print_r($this->calc_macd($candles));

                // echo '<br/>RSI : ';
                // print_r($this->calc_rsi($candles));

                // $idxs = 26;
                // for ($i = 0; $i < $idxs; $i++) {
                //     print_r($i . ' sma = ' . $this->calc_sma($candles, 20, $i));
                //     echo '<br/>';
                //     print_r($i . ' ema = ' . $this->calc_ema($candles, 20, $i));
                //     echo '<br/><br/>';
                // }

                // print_r($candles);

            }
        }

        die;
    }

    public function btx_api()
    {

        echo '<pre/>';

        $users = User::with(['exchanges' => function ($query) {
            $query->where('exchange', '=', 'btx');
        }])->get();

        foreach ($users as $user) {

            if (count($user->exchanges) == 0) {
                continue;
            }

            // Get user's api key
            $apiKey    = $user->exchanges[0]->api_key;
            $apiSecret = $user->exchanges[0]->api_secret;

            // Init object class
            $btx1 = new Btx1($apiKey, $apiSecret);
            $btx2 = new Btx2($apiKey, $apiSecret);

            if ($user->id == 3) {
                
                // print_r($btx1->getOpenOrders('BTC-ZEN'));

                $orderList = $btx1->getOrderHistory('BTC-ETC', 100); 
                
                // print_r($orderList);

                foreach ($orderList as $key => $value) {

                    if ($key == 1) {
                        break;
                    }

                    if ($value['OrderType'] == 'LIMIT_SELL' && in_array($value['Exchange'], ['BTC-ETC'])
                        && !empty($value['Closed']) && !$value['ImmediateOrCancel']) {

                        $btxOrderLogs = new BtxOrderLogs();
                        
                        $checkExist = $btxOrderLogs->where(
                            [
                                'user_id' => $user->id, 
                                'sell_order_id' => $value['OrderUuid'], 
                            ]
                        )->first();

                        if (!$checkExist) {
                            echo '<br/>' . $value['OrderUuid'];
                            $data['user_id']    = $user->id;
                            $data['symbol']     = strtoupper($value['Exchange']);
                            $data['time_frame'] = 'day';
                            $data['sell_order_id'] = $value['OrderUuid'];
                            $data['sell_price']    = number_format($value['Limit'], 8, '.', '');
                            $data['sell_status']    = 1;
                            $data['amount']    = $value['Quantity'] - $value['QuantityRemaining'];
                            $data['profit_rate'] = null;
                            $data['condition_type'] = 'upper BB2';
                            $data['price_rate'] = 22;
                            $data['price_rate_cond'] = 22;
                            $data['status'] = 1;
                            // $data['created_at'] = $value['timestamp'];

                            $btxOrderLogs->fill($data);
                            $btxOrderLogs->save();
                        }
                    }
                }

                // print_r($btx1->getOrder('c42f5d5b-d02f-4a5a-8ea3-729773904a28'));

                // $order = $btx1->getOrder('9833ffdb-317a-4395-bb1d-b8adfb5658fc');
                // print_r($order);
                // print_r(number_format($order['Limit'], 8, '.', ''));

                // $sell_order_book = $btx1->getOrderBook('BTC-TRX', 'sell');
                // print_r($sell_order_book);


                // $market = $btx1->getMarketSummary('BTC-CRW');
                // print_r($market);


                // $candle = $btx2->getTicks('BTC-XRP', 'fiveMin');
                // $candle = $btx2->getTicks('BTC-XVG', 'thirtyMin');
                // $candle = $btx2->getTicks('BTC-BCH', 'fiveMin');
                // print_r($candle);
            }
        }

        die;
    }
}
