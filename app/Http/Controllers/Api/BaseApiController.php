<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Response;
use Log;

class BaseApiController extends Controller
{

    /**
     * @var array
     */
    protected $result;

    public function __construct()
    {
        $this->result = [
            'success' => false,
            'message' => '',
        ];
    }

    /**
     * @param $class
     * @param $method
     * @param $errorMessage
     */
    public function logError($class, $method, $errorMessage)
    {
        $this->result['success'] = false;
        $this->result['message'] = trans('messages.server_error');
        Log::error($class . "::" . $method . "  " . $errorMessage);
    }

    /**
     * @param array $data
     * @param $message
     * @param $statusCode
     */
    public function sendResponse()
    {
        // $this->result = [
        //     'success' => true,
        //     'message' => $message,
        //     'data'    => $data,
        // ];

        if ($this->result['success'] == false && $this->result['message'] == '' ) {
            $this->result['message'] = trans('messages.server_error');
        }

        return response()->json($this->result, Response::HTTP_OK);

    }

}
