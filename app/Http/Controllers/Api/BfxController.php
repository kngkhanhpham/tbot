<?php

namespace App\Http\Controllers\Api;

use App\BfxUserSymbols;
use App\Http\Controllers\Api\BaseApiController;
use App\Http\Requests\Api\BfxSymbolRequest;
use Exception;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Input;

class BfxController extends BaseApiController
{

    /**
     * @return mixed
     */
    public function getSymbol(BfxSymbolRequest $request)
    {
        try {

            $input        = Input::get();
            $symbols      = trans('bfx.symbol.list.USD') + trans('bfx.symbol.list.BTC');
            $settingField = trans('bfx.field');

            $input['exchange'] = 'bfx';
            if (empty($input['symbol'])) {
                $input['symbol'] = array_keys($symbols)[0];
            }

            $symbol = BfxUserSymbols::where('user_id', Auth::id())
                ->where('symbol', $input['symbol'])
                ->where('time_frame', $input['time_frame'])
                ->first();

            if ($symbol) {
                // Map fiels
                foreach ($settingField as $key => $value) {
                    if (isset($value['items'])) {
                        foreach ($value['items'] as $sub_key => $value2) {
                            $settingField[$key]['items'][$sub_key]['value'] = $symbol->{$key . '_' . $sub_key};
                        }
                    } else {
                        $settingField[$key]['value'] = $symbol->$key;
                    }
                }
            } else {
                // $symbolDefault = trans('setting.' . $input['exchange'] . '_symbol.default.' . $input['symbol']);
                // foreach ($settingField as $key => $value) {
                //     if (isset($symbolDefault[$key])) {
                //         $settingField[$key]['default'] = $symbolDefault[$key];
                //     }
                // }
            }

            $this->result = [
                'success' => true,
                'message' => 'get data success',
                'data'    => ['symbols' => $symbols, 'fields' => $settingField],
            ];

        } catch (\Exception $e) {
            $this->logError(__CLASS__, __METHOD__, $e->getMessage());
        }

        return $this->sendResponse();
    }

}
