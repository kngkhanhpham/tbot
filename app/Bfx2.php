<?php

namespace App;

class Bfx2
{
    const CONNECT_TIMEOUT = 60;
    const API_URL         = 'https://api.bitfinex.com';

    /**
     * @var string
     */
    private $apiKey = '';
    /**
     * @var string
     */
    private $apiSecret = '';
    /**
     * @var string
     */
    private $api_version = '';

    /**
     * @param string $apiKey       Your API key obtained from https://www.bitfinex.com/account/api
     * @param string $apiSecret    Your API secret obtained from https://www.bitfinex.com/account/api
     * @param string $api_version   Bitfinex API version
     */
    public function __construct($apiKey, $apiSecret)
    {
        $this->api_key     = $apiKey;
        $this->api_secret  = $apiSecret;
        $this->api_version = 'v2';
    }

    /**
     * Public endpoints
     * =================================================================
     */

    /**
     * Get Candle
     *
     * @param string $symbol    The name of the symbol (see `/symbols`).
     * @param int $time_frame   Available values: '1m', '5m', '15m', '30m', '1h', '3h', '6h',
     *                          '12h', '1D', '7D', '14D', '1M'
     * @param int $section      Available values: "last", "hist"
     * @return array
     *          MTS     int millisecond time stamp
     *          OPEN    float   First execution during the time frame
     *          CLOSE   float   Last execution during the time frame
     *          HIGH    float   Highest execution during the time frame
     *          LOW     float   Lowest execution during the timeframe
     *          VOLUME  float   Quantity of symbol traded within the timeframe
     *
     */
    public function getCandles($symbol = 'tBTCUSD', $time_frame = '30m', $section = 'hist', $param = ['limit' => 336])
    {
        $request = $this->endpoint('candles', ["trade:$time_frame:$symbol", $section]);

        return $this->sendPublicRequest($request, $param);
    }

    /**
     * Get Book
     *
     * Get the full order book.
     *
     * @param string $symbol    The name of the symbol (see `/symbols`).
     * @param int $precision    Limit the number of bids returned. May be 0
     *                          in which case the array of bids is empty.
     * @param int len           Number of price points ("25", "100")
     *
     * @return mixed
     */
    public function getBook($symbol = 'tBTCUSD', $precision = 'P0', $data = ['len' => 25])
    {
        $request = $this->endpoint('book', [$symbol, $precision]);

        return $this->sendPublicRequest($request, $data);
    }

    /**
     * Get Ticker
     *
     * Gives innermost bid and asks and information on the most recent trade, as
     * well as high, low and volume of the last 24 hours.
     *
     * @param string $symbol    The name of the symbol (see `/symbols`).
     * @return mixed
     */
    public function getTicker($symbol = 'tBTCUSD')
    {
        $request = $this->endpoint('ticker', $symbol);

        return $this->sendPublicRequest($request);
    }

    /**
     * Get Tickers
     *
     * Gives innermost bid and asks and information on the most recent trade, as
     * well as high, low and volume of the last 24 hours.
     *
     * @param string $symbol    The name of the symbol (see `/symbols`).
     * @return mixed
     */
    public function getTickers($symbols = 'tBTCUSD,tLTCUSD,tETHUSD')
    {
        $request = $this->endpoint('tickers');

        return $this->sendPublicRequest($request, ['symbols' => $symbols]);
    }

    /**
     * Get Trades
     *
     * Get a list of the most recent trades for the given symbol.
     *
     * @param string $symbol        The name of the symbol (see `/symbols`).
     * @param time $timestamp       Only show trades at or after this timestamp.
     * @param int $limit_trades     Limit the number of trades returned. Must
     *                              be >= 1.
     * @return mixed
     */
    public function getTrades($symbol = 'tBTCUSD', $data = ['limit' => 120, 'start' => 0, 'end' => 0, 'sort' => -1])
    {
        $request = $this->endpoint('trades', [$symbol, 'hist']);

        return $this->sendPublicRequest($request, $data);
    }

    /**
     * Authenticated endpoints
     * =================================================================
     */

    /**
     * Calc Available Balance (V2)
     *
     * @param string $symbol    Symbol
     *
     * @param int $direction    Direction of the order/offer
     *                          (orders: > 0 buy, < 0 sell | offers: > 0 sell, < 0 buy)
     *
     * @param string $rate      Rate of the order/offer
     *
     * @param $type             Type of the order/offer EXCHANGE or MARGIN
     *
     * @return float Amount available for order/offer
     */
    public function calcAvailableBalance($symbol = 'tBTCUSD', $direction = 1, $rate = '800', $type = 'EXCHANGE')
    {
        $apiPath = $this->endpoint('auth/calc/order/avail');

        $data = [
            'api_path' => $apiPath,
            'body'     => [
                'symbol' => $symbol,
                'dir'    => $direction,
                'rate'   => $rate,
                'type'   => $type,
            ],
        ];

        return $this->sendAuthRequest($data);
    }

    /**
     * Get Orders History
     *
     * View your orders history.
     *
     * @return mixed
     */
    public function getOrdersHistory()
    {
        $apiPath = $this->endpoint('auth/r/orders/tBTCUSD/hist');

        $data = [
            'api_path' => $apiPath,
            'body'     => [
                'start' => 0,
                'end'   => 0,
                'limit' => 25,
            ],
        ];

        return $this->sendAuthRequest($data);
    }

    /**
     * Endpoint
     *
     * Construct an endpoint URL
     *
     * @param string $method
     * @param mixed $params
     * @return string
     */
    private function endpoint($method, $params = null)
    {
        $parameters = '';

        if ($params !== null) {
            $parameters = '/';

            if (is_array($params)) {
                $parameters .= implode('/', $params);
            } else {
                $parameters .= $params;
            }
        }

        return "/{$this->api_version}/$method$parameters";
    }

    /**
     * Prepare Header
     *
     * Add data to header for authentication purpose
     *
     * @param array $data
     * @return json
     */
    private function prepareHeader($data)
    {
        // print_r($data);
        $apiPath = $data['api_path'];
        $nonce   = (string) number_format(round(microtime(true) * 100000), 0, '.', '');
        // $payload    = base64_encode(json_encode($data['body']));
        $payload    = json_encode($data['body']);
        $postData   = (count($data['body'])) ? '/' . implode("/", $data['body']) : '';
        $sigPayload = "/api$apiPath$postData$nonce";
        // print_r($sigPayload);
        $signature = hash_hmac('sha384', utf8_encode($sigPayload), utf8_encode($this->api_secret));
        // print_r($signature);

        // die;

        return [
            'content-type: application/json',
            'bfx-nonce: ' . $nonce,
            'bfx-apikey: ' . $this->api_key,
            'bfx-signature: ' . $signature,
        ];
    }

    /**
     * Curl Error
     *
     * Output curl error if possible
     *
     * @param array $data
     * @return json
     */
    private function curlError($ch)
    {
        if ($errno = curl_errno($ch)) {
            $error_message = curl_strerror($errno);
            echo "cURL error ({$errno}):\n {$error_message}";

            return false;
        }

        return true;
    }

    /**
     * Is Bitfinex Error
     *
     * Check whether bitfinex API returned an error message
     *
     * @param array $ch     Curl resource
     * @return bool
     */
    private function isBitfinexError($ch)
    {
        $http_code = (int) curl_getinfo($ch, CURLINFO_HTTP_CODE);

        if ($http_code !== 200) {
            return true;
        }

        return false;
    }

    /**
     * Output
     *
     * Prepare API output
     *
     * @param json $result
     * @param bool $is_error
     * @return array
     */
    private function output($result, $is_error = false)
    {
        $out_array = json_decode($result, true);

        if ($is_error) {
            $out_array['error'] = true;
        }

        return $out_array;
    }

    /**
     * Send Signed Request
     *
     * Send a signed HTTP request
     *
     * @param array $data
     * @return mixed
     */
    private function sendAuthRequest($data)
    {
        $ch  = curl_init();
        $url = self::API_URL . $data['api_path'];

        $headers = $this->prepareHeader($data);

        curl_setopt_array($ch, [
            CURLOPT_URL            => $url,
            CURLOPT_POST           => true,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_HTTPHEADER     => $headers,
            CURLOPT_SSL_VERIFYPEER => true,
            CURLOPT_CONNECTTIMEOUT => self::CONNECT_TIMEOUT,
            CURLOPT_POSTFIELDS     => '',
            // CURLOPT_POSTFIELDS => $data['body'],
        ]);

        if (!$result = curl_exec($ch)) {
            return $this->curlError($ch);
        } else {
            return $this->output($result, $this->isBitfinexError($ch));
        }
    }

    /**
     * Send Unsigned Request
     *
     * Send an unsigned HTTP request
     *
     * @param string $request
     * @param array $params
     * @return mixed
     */
    private function sendPublicRequest($request, $params = null)
    {
        $ch    = curl_init();
        $query = '';

        if (count($params)) {
            $query = '?' . http_build_query($params);
        }

        $url = self::API_URL . $request . $query;

        curl_setopt_array($ch, [
            CURLOPT_URL            => $url,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_SSL_VERIFYPEER => true,
            CURLOPT_CONNECTTIMEOUT => self::CONNECT_TIMEOUT,
        ]);

        if (!$result = curl_exec($ch)) {
            return $this->curlError($ch);
        } else {
            return $this->output($result, $this->isBitfinexError($ch));
        }
    }

    /**
     * @param $num
     * @param $decimal
     */
    public function num2string($num, $decimal = 8)
    {
        return number_format($num, $decimal, '.', '');
    }
}
