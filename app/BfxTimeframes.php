<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Cache;

class BfxTimeframes extends Model
{
    /**
     * @var string
     */
    protected $table = 'bfx_timeframes';

    /**
     * @var array
     */
    public static $rules = [
        'time_frame'        => 'string|max:10',
        'status'            => 'integer|in:1,2',
    ];

    /**
     * @var array
     */
    protected $fillable = [
        'id',
        'time_frame',
        'time_frame_name',
        'status',
    ];

    public static function getList()
    {
        $key  = 'bfx_timeframes';
        $data = Cache::rememberForever($key, function () {
            $res        = [];
            $timeframes = BfxTimeframes::where('status', 1)->get();
            foreach ($timeframes as $key => $value) {
                $res[$value->time_frame] = $value->time_frame_name;
            }
            return json_encode($res);
        });

        return json_decode($data, true);
    }    
}
