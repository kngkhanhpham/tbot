<?php

namespace App;

class BtxCalc
{
    /**
     * @param $candles
     * @param $n
     * @param $idx
     * @return mixed
     */
    public static function sma($candles, $n = 20, $idx = 0, $round = 8)
    {
        $total = 0;
        for ($i = ($n - 1 + $idx); $i >= $idx; $i--) {
            $total += $candles[$i]['C'];
        }

        return round($total / $n, $round);
    }

    /**
     * @param $candles
     * @param $n
     * @param $idx
     * @return mixed
     */
    public static function ema($candles, $n = 20, $idx = 0, $round = 8)
    {
        $k = 2 / ($n + 1);

        $cnt = count($candles);

        if ($cnt > 200) {
            $cnt = 200;
        }

        // Calc First Prev EMA
        $emaPrev = self::sma($candles, $n, $cnt - ($n + $idx));
        for ($i = $cnt - 1 - ($n + $idx); $i > $n - 1 + $idx; $i--) {
            $priceClose = $candles[$i]['C'];
            $ema        = ($priceClose * $k) + ($emaPrev * (1 - $k));
            $emaPrev    = $ema;
        }

        // Calc EMA
        for ($i = $n - 1 + $idx; $i >= $idx; $i--) {
            $priceClose = $candles[$i]['C'];
            $ema        = ($priceClose * $k) + ($emaPrev * (1 - $k));
            $emaPrev    = $ema;
        }

        return round($ema, $round);
    }

    /**
     * @param $candles
     * @param $n1
     * @param $n2
     * @param $n3
     * @param $idx
     */
    public static function macd($candles, $n1 = 12, $n2 = 26, $n3 = 9, $idx = 0)
    {
        $emas = [];
        for ($i = $n3 - 1 + $idx; $i >= $idx; $i++) {
            $emas[] = self::ema($candles, $n1, $i) - self::ema($candles, $n2, $i);
        }

        $macdSignal = array_sum($emas) / count($emas);

        return ['macd' => round($emas[0], 4), 'macd_signal' => round($macdSignal, 4)];
    }

    /**
     * @param $candles
     * @param $n
     * @param $idx
     */
    public static function rsi($candles, $n = 14, $idx = 0)
    {
        $cnt = count($candles);

        if ($cnt > 200) {
            $cnt = 200;
        }

        // Calc first Prev RSI
        $rsi  = [];
        $up   = [];
        $down = [];
        for ($i = $cnt - 2 - $idx; $i > $cnt - 2 - $n - $idx; $i--) {
            $up[]   = $candles[$i]['C'] > $candles[$i + 1]['C'] ? $candles[$i]['C'] - $candles[$i + 1]['C'] : 0;
            $down[] = $candles[$i]['C'] < $candles[$i + 1]['C'] ? $candles[$i + 1]['C'] - $candles[$i]['C'] : 0;
        }

        $avgUp   = array_sum($up) / count($up);
        $avgDown = array_sum($down) / count($down);
        // $rs      = $avgUp / $avgDown;
        // $rsi[]   = round(100 - (100 / (1 + $rs)), 4);

        // Calc RSI
        for ($i = $cnt - 2 - $n - $idx; $i >= $idx; $i--) {
            $up      = $candles[$i]['C'] > $candles[$i + 1]['C'] ? $candles[$i]['C'] - $candles[$i + 1]['C'] : 0;
            $down    = $candles[$i]['C'] < $candles[$i + 1]['C'] ? $candles[$i + 1]['C'] - $candles[$i]['C'] : 0;
            $avgUp   = ($avgUp * ($n - 1) + $up) / $n;
            $avgDown = ($avgDown * ($n - 1) + $down) / $n;
            $rs      = $avgUp / $avgDown;
            $rsi[]   = round(100 - (100 / (1 + $rs)), 4);
        }

        return array_slice(array_reverse($rsi), 0, 10);
    }

    /**
     * @param $candles
     * @param $n
     * @param $idx
     */
    public static function stdevp($candles, $n = 20, $idx = 0)
    {
        $arr = [];
        for ($i = ($n - 1 + $idx); $i >= $idx; $i--) {
            $arr[] = $candles[$i]['C'];
        }

        $avg   = array_sum($arr) / $n;
        $carry = 0.0;
        foreach ($arr as $val) {
            $d = $val - $avg;
            $carry += $d * $d;
        };

        return sqrt($carry / $n);

    }

    /**
     * @param $candles
     * @param $n
     * @param $std
     * @param $idx
     * @return mixed
     */
    public static function bb($candles, $n = 20, $std = 2, $idx = 0)
    {
        $sma    = self::sma($candles, $n, $idx);
        $stdevp = self::stdevp($candles, $n, $idx);
        $result = [
            'std'   => $std,
            'upper' => round($sma + ($stdevp * $std), 8),
            'mid'   => round($sma, 8),
            'lower' => round($sma - ($stdevp * $std), 8),
        ];

        return $result;
    }
}
