<?php

namespace App;

use App\Bfx1;
use App\Bfx2;
use App\BfxCalc;
use App\BfxFailOrderLogs;
use App\BfxOrderLogs;
use App\BfxUserSymbols;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Log;

class BfxBbV5
{

    /**
     * @param array $users
     * @return null
     */
    public function handle($users = [], $symbol, $timeFrame, $candle = [])
    {
        if (empty($users) || empty($candle)) {
            return;
        }

        // Get open, high, low, current price, vol
        $priceOpen    = $candle[0][1];
        $priceHigh    = $candle[0][3];
        $priceLow     = $candle[0][4];
        $priceCurrent = $candle[0][2];
        $volCurrent   = $candle[0][5];

        if ($priceOpen == 0 || $priceCurrent == 0 || $priceHigh == 0) {
            // Log::channel('bfx')->info($preUserInfoLog . ' : get price = 0');
            return;
        }         

        // // Debug
        // if ($symbol == 'BTCUSD') {
        //     Log::channel('bfx')->debug('------- BB debug -------');
        //     for ($i = 1; $i < 5 ; $i++) { 
        //         Log::channel('bfx')->debug('idx : ' . $i);
        //         Log::channel('bfx')->debug(BfxCalc::bb($candle, 20, 2, $i));
        //     }                
        // }        

        // Init data
        $data               = [];
        $data['symbol']     = $symbol;
        $data['time_frame'] = $timeFrame;

        $symbolList = BfxUserSymbols::getList(1);
        // \Log::debug($symbolList);

        if (! isset($symbolList[$symbol][$timeFrame])) {
            return;
        }

        $optionSetting = $symbolList[$symbol][$timeFrame]['options'];
        
        // use BB to check make order
        // Set upper rate cond
        $upperPriceRateCond1 = $optionSetting['opt_bb_upper_price_rate_cond1'];
        $upperPriceRateCond2 = $optionSetting['opt_bb_upper_price_rate_cond2'];
        // Set lower rate cond
        $lowerPriceRateCond1 = $optionSetting['opt_bb_lower_price_rate_cond1'];
        $lowerPriceRateCond2 = $optionSetting['opt_bb_lower_price_rate_cond2'];
        // Set min profit rate
        $data['upper_min_profit_rate_cond'] = $optionSetting['opt_bb_upper_min_profit_rate_cond'];
        $data['lower_min_profit_rate_cond'] = $optionSetting['opt_bb_lower_min_profit_rate_cond'];

        // Check buy/sell available
        // Set sell/buy price
        $data['sell_price'] = $priceCurrent;
        $data['buy_price']  = $priceCurrent;
        $this->runUserList2($users, $data);

        // Get BB data
        $bb = BfxCalc::bb($candle, 20, 2);            

        // Sell when current price > BB upper then buy
        $keyST = $symbol. '_' . $timeFrame . '_upper' ;
        if ($optionSetting['opt_bb_upper_status'] == 1 && ($priceCurrent > $bb['upper'] || Cache::has($keyST))) {
            $onFlg = false;

            $cachePrices = [];
            if (Cache::has($keyST)) {
                $cachePrices = Cache::get($keyST);
                if (!empty($cachePrices)) {
                    $cachePrices = json_decode($cachePrices, true);
                    if (!empty($cachePrices)) {
                        if ($cachePrices[0] < $priceCurrent) {
                            $priceStop = round($priceCurrent - $priceCurrent * env('PRICE_STOP_PERCENT', 2) / 100, 8);
                            // Log::channel('bfx')->info($keyST . ' priceStop : ' . $priceStop);
                            // Log::channel('bfx')->info($keyST . ' $cachePrices[0] : ' . $cachePrices[0]);
                            if ($priceStop > $cachePrices[0]) {
                                $cachePrices = array_merge([$priceStop], $cachePrices);
                                // Log::channel('bfx')->info($cachePrices);
                                Cache::put($keyST, json_encode($cachePrices), 172800);
                                Log::channel('bfx')->info($keyST . ' : ' . Cache::get($keyST));
                            }
                            return;
                        } else {
                            // Set price current
                            $priceCurrent = $cachePrices[0];                            
                            // Calc price rate
                            $priceRate = round(($priceCurrent - $bb['mid']) / $bb['mid'] * 100, 2);
                            
                            Log::channel('bfx')->info($keyST . ' rate : ' . $priceRate . '/' .  $upperPriceRateCond2);
                            
                            if ($priceRate >= $upperPriceRateCond2) {
                                $onFlg = true;
                            }                      

                            Cache::forget($keyST);
                        }
                    }
                } else {
                    Cache::forget($keyST);
                }
            }

            // Check price rate (current/bb['mid'])
            if ($onFlg == false) {
                // Calc price rate
                $priceRate = round(($priceCurrent - $bb['mid']) / $bb['mid'] * 100, 2);
                if ($priceRate >= $upperPriceRateCond1) {
                    if (empty($cachePrices)) {
                        $priceStop = round($priceCurrent - $priceCurrent * env('PRICE_STOP_PERCENT', 2) / 100, 8);
                        if ($priceStop > $bb['mid']) {
                            $cachePrices = [$priceStop, $bb['mid']];
                        } else {
                            $cachePrices = [$bb['mid']];
                        } 
                        
                        if (Cache::has($keyST)) {
                            Cache::put($keyST, json_encode($cachePrices), 172800);
                        } else {
                            Cache::add($keyST, json_encode($cachePrices), 172800);
                        }
                        
                        Log::channel('bfx')->info($keyST . ' first : ' . Cache::get($keyST));
                        
                        return;
                    }
                }                 
            }              

            // Match condition
            if ($onFlg == true) {
                // Set current price
                $data['price_current'] = $priceCurrent;
                // Set sell price
                $data['sell_price'] = $priceCurrent;
                // Set profit rate
                $data['profit_rate'] = null;
                // Set buy price
                // $data['buy_price'] = round($data['sell_price'] - $data['sell_price'] * ($data['profit_rate'] + env('TRADING_FEE', 0.25)) / 100, 8);
                // $data['buy_price'] = number_format($data['buy_price'], 8, '.', '');
                $data['buy_price'] = null;
                // Set condition type
                $data['condition_type'] = 'upper BB2';
                // Set price rate
                $data['price_rate'] = $priceRate;
                // Set price rate cond
                $data['price_rate_cond'] = $upperPriceRateCond1;
                // Set status
                $data['status'] = 1;

                // Out Info Log
                // Log::channel('bfx')->info($bb);

                $this->runUserList($users, $data);
                return;
            }
        }                       

        // Buy when current price < BB lower then sell
        $keyST = $symbol. '_' . $timeFrame . '_lower' ;
        if ($optionSetting['opt_bb_lower_status'] == 1 && ($priceCurrent < $bb['lower'] || Cache::has($keyST))) {
            $onFlg = false;

            $cachePrices = [];
            if (Cache::has($keyST)) {
                $cachePrices = Cache::get($keyST);
                if (!empty($cachePrices)) {
                    $cachePrices = json_decode($cachePrices, true);
                    if (!empty($cachePrices)) {
                        if ($cachePrices[0] > $priceCurrent) {
                            $priceStop = round($priceCurrent + $priceCurrent * env('PRICE_STOP_PERCENT', 2) / 100, 8);
                            if ($priceStop < $cachePrices[0]) {
                                $cachePrices = array_merge([$priceStop], $cachePrices);
                                Cache::put($keyST, json_encode($cachePrices), 172800);
                                Log::channel('bfx')->info($keyST . ' : ' . Cache::get($keyST));
                            }
                            return;
                        } else {
                            // Set price current
                            $priceCurrent = $cachePrices[0];                            
                            // Calc price rate
                            $priceRate = round(abs($priceCurrent - $bb['mid']) / $bb['mid'] * 100, 2);
                            
                            Log::channel('bfx')->info($keyST . ' rate : ' . $priceRate . '/' .  $lowerPriceRateCond2);

                            if ($priceRate >= $lowerPriceRateCond2) {
                                $onFlg = true;
                            }    
                            Cache::forget($keyST);
                        }
                    }
                } else {
                    Cache::forget($keyST);
                }                
            }             

            // Check price rate (current/bb['mid'])
            if ($onFlg == false) {
                // Calc price rate
                $priceRate = round(abs($priceCurrent - $bb['mid']) / $bb['mid'] * 100, 2);
                if ($priceRate >= $lowerPriceRateCond1) {
                    if (empty($cachePrices)) {                        
                        $priceStop = round($priceCurrent + $priceCurrent * env('PRICE_STOP_PERCENT', 2) / 100, 8);
                        if ($priceStop < $bb['mid']) {
                            $cachePrices = [$priceStop, $bb['mid']];
                        } else {
                            $cachePrices = [$bb['mid']];
                        }

                        if (Cache::has($keyST)) {
                            Cache::put($keyST, json_encode($cachePrices), 172800);
                        } else {
                            Cache::add($keyST, json_encode($cachePrices), 172800);
                        }

                        Log::channel('bfx')->info($keyST . ' first : ' . Cache::get($keyST));

                        return;
                    }   
                }                   
            } 

            if ($onFlg == true) {
                // Set current price
                $data['price_current'] = $priceCurrent;                
                // Set buy price
                $data['buy_price'] = $priceCurrent;
                // Set profit rate
                $data['profit_rate'] = null;
                // Set sell price
                // $data['sell_price'] = round($data['buy_price'] + $data['buy_price'] * ($data['profit_rate'] + env('TRADING_FEE', 0.25)) / 100, 8);
                // $data['sell_price'] = number_format($data['sell_price'], 8, '.', '');
                $data['sell_price'] = null;
                // Set condition type
                $data['condition_type'] = 'lower BB2';
                // Set price rate
                $data['price_rate'] = $priceRate;
                // Set price rate cond
                $data['price_rate_cond'] = $lowerPriceRateCond1;
                // Set status
                $data['status'] = 1;

                // Out Info Log
                // Log::channel('bfx')->info($bb);

                $this->runUserList($users, $data);
                return;                    
            }  
        }
    }

    /**
     * @param $users
     * @param $data
     * @return null
     */
    protected function runUserList($users, $data)
    {
        foreach ($users as $key => $user) {
            // Only run for test
            // if ($user->id != 1) {
            //     continue;
            // }

            $exchange = $user->exchanges[0];
            if ($exchange->status != 1) {
                continue;
            }

            // Get user's api key
            $apiKey    = $exchange->api_key;
            $apiSecret = $exchange->api_secret;

            // Init object class
            $bfx1 = new Bfx1($apiKey, $apiSecret);

            $data['user_id'] = $user->id;
            // Get user's symbol setting
            $setting = BfxUserSymbols::where(
                [
                    'user_id'    => $data['user_id'],
                    'symbol'     => $data['symbol'],
                    'time_frame' => $data['time_frame']
                ]
            )->first();

            // // Debug
            // if ($data['user_id'] == 1) {
            //     Log::channel('bfx')->debug('setting');
            //     Log::channel('bfx')->debug($setting);
            // }

            if (! is_null($setting) && $setting->status == 1) {

                $preUserInfoLog = $data['user_id'] . ' : ' . $data['symbol'] . ' : ' . $data['time_frame'];
                // Log::channel('bfx')->info($preUserInfoLog);                

                // Set base amount
                $data['base_amount_limit'] = $setting->base_amount_limit;
                $data['base_amount']       = $setting->base_amount;
                // Set amount
                $data['amount'] = round(($data['base_amount'] - ($data['base_amount'] * env('TRADING_FEE', 0.25) / 100)) / $data['price_current'], 8, PHP_ROUND_HALF_DOWN);                     

                // Log::channel('bfx')->info($preUserInfoLog);

                // Debug
                if ($data['user_id'] == 1) {
                    Log::channel('bfx')->debug($data);
                }  

                if (substr($data['condition_type'], 0, 8) == 'lower BB') {
                    $this->buyAvailable($bfx1, $data);   
                    if ($data['symbol'] == 'BTCUSD') {
                        $this->buy($bfx1, $data);
                    }                             
                } else {
                    $this->sellAvailable($bfx1, $data); 
                    $this->sell($bfx1, $data);                               
                }
            }
        }
    }

    /**
     * @param $bfx1
     * @param $data
     * @return null
     */
    protected function buy($bfx1, $data)
    {
        $key = 'bfx_balances_' . $data['user_id'];
        if (Cache::has($key)) {
            $balances = Cache::get($key);
        } else {
            // Get user's balances
            $balancesRes = $bfx1->getBalances();

            if (empty($balancesRes) || isset($balancesRes['error'])) {
                Log::channel('bfx')->error('get balances error');
                Log::channel('bfx')->error($balancesRes);
            } else {

                $balances = [];
                foreach ($balancesRes as $key => $value) {
                    $balances[strtoupper($value['currency'])]['amount']    = $value['amount'];
                    $balances[strtoupper($value['currency'])]['available'] = $value['available'];
                }

                if (!empty($balances)) {
                    Cache::put($key, $balances, 1);
                }
            }
        }

        $tCurrency    = substr($data['symbol'], 0, 3);
        $baseCurrency = substr($data['symbol'], -3);
        // Log::channel('bfx')->info($baseCurrency);
        // Log::channel('bfx')->info('amount    : ' . $balances[$baseCurrency]['amount']);
        // Log::channel('bfx')->info('available : ' . $balances[$baseCurrency]['available']);

        // Balances amount > config limit amount (values calculate on base_currency)
        if (isset($balances[$tCurrency]) && $balances[$tCurrency]['amount'] * $data['buy_price'] >= $data['base_amount_limit']) {
            $bfxFailOrderLogs = new BfxFailOrderLogs();
            $data['message'] = 'balances >= base amount limit';
            $bfxFailOrderLogs->fill($data);
            $bfxFailOrderLogs->save();

            Log::channel('bfx')->info($data['message']);

            return;
        }

        if (isset($balances[$baseCurrency]['available'])) {
            // Balances base_currency < config amount base_currency
            if ($balances[$baseCurrency]['available'] < env('USD_ORDER_AMOUNT_MIN', 20)) {
                $bfxFailOrderLogs = new BfxFailOrderLogs();
                $data['message'] = 'The minimum order size is ' . env('USD_ORDER_AMOUNT_MIN', 20) . ' USD';
                $bfxFailOrderLogs->fill($data);
                $bfxFailOrderLogs->save();
                return;
            }

            if ($balances[$baseCurrency]['available'] < $data['base_amount']) {
                $data['base_amount'] = $balances[$baseCurrency]['available'];
                // Calc amount again
                $data['amount'] = round(($data['base_amount'] + ($data['base_amount'] * env('TRADING_FEE', 0.3) / 100)) / $data['buy_price'], 8, PHP_ROUND_HALF_DOWN);
            }
        }

        // Make buy order
        // $buyOrder = $bfx1->newOrder($data['symbol'], $data['amount'], 1.23, 'bitfinex', 'buy', 'exchange market');
        $buyOrder = $bfx1->newOrder($data['symbol'], $data['amount'], $data['buy_price'], 'bitfinex', 'buy', 'exchange limit');
        if (isset($buyOrder['order_id'])) {
            // Save buy order logs
            $bfxOrderLogs         = new BfxOrderLogs();
            $data['buy_order_id'] = $buyOrder['order_id'];
            $data['buy_price']    = $buyOrder['price'];
            $bfxOrderLogs->fill($data);
            $bfxOrderLogs->save();

            // Set pre info log
            $preInfoLog = $data['user_id'] . ' : ' . $data['symbol'] . ' : ' . $data['buy_order_id'] . ' : ';
            Log::channel('bfx_order')->info($preInfoLog . 'set buy order success !');
            Log::channel('bfx_order')->info($buyOrder);            
        } else {
            $bfxFailOrderLogs = new BfxFailOrderLogs();
            $data['message'] = $buyOrder['message'];
            $bfxFailOrderLogs->fill($data);
            $bfxFailOrderLogs->save();
        }
    }

    /**
     * @param $bfx1
     * @param $data
     * @return null
     */
    protected function sell($bfx1, $data)
    {
        $key = 'bfx_balances_' . $data['user_id'];
        if (Cache::has($key)) {
            $balances = Cache::get($key);
        } else {
            // Get user's balances
            $balancesRes = $bfx1->getBalances();

            if (empty($balancesRes) || isset($balancesRes['error'])) {
                Log::channel('bfx')->error(get_class($this) . ' :: ' . $data['user_id'] . ' : get balances error');
                Log::channel('bfx')->error($balancesRes);
            } else {

                $balances = [];
                foreach ($balancesRes as $key => $value) {
                    $balances[strtoupper($value['currency'])]['amount']    = $value['amount'];
                    $balances[strtoupper($value['currency'])]['available'] = $value['available'];
                }

                if (!empty($balances)) {
                    Cache::put($key, $balances, 1);
                }
            }
        }

        $tCurrency    = substr($data['symbol'], 0, 3);
        $baseCurrency = substr($data['symbol'], -3);
        // Log::channel('bfx')->info($baseCurrency);
        // Log::channel('bfx')->info('amount    : ' . $balances[$baseCurrency]['amount']);
        // Log::channel('bfx')->info('available : ' . $balances[$baseCurrency]['available']);

        // Check Currency balance
        if (isset($balances[$tCurrency])) {
            if ($balances[$tCurrency]['available'] < $data['amount']) {
                if ($balances[$tCurrency]['available'] * $data['sell_price'] < env('USD_ORDER_AMOUNT_MIN', 20)) {
                    $data['message'] = 'The minimum order size is ' . env('USD_ORDER_AMOUNT_MIN', 20) . ' USD';
                    $bfxFailOrderLogs = new BfxFailOrderLogs();
                    $bfxFailOrderLogs->fill($data);
                    $bfxFailOrderLogs->save();
                    return;
                }
                $data['amount'] = $balances[$tCurrency]['available'];
            }
        } else {
            $data['message'] = 'balances no available !';
            $bfxFailOrderLogs = new BfxFailOrderLogs();
            $bfxFailOrderLogs->fill($data);
            $bfxFailOrderLogs->save();
            return;
        }

        // Make sell order
        $sellOrder = $bfx1->newOrder($data['symbol'], $data['amount'], $data['sell_price'], 'bitfinex', 'sell', 'exchange limit');
        if (isset($sellOrder['order_id'])) {
            // Save sell order
            $data['sell_order_id'] = $sellOrder['order_id'];
            // Save buy order logs
            $bfxOrderLogs    = new BfxOrderLogs();
            $bfxOrderLogs->fill($data);
            $bfxOrderLogs->save();
            // Set pre info log
            $preInfoLog = $data['user_id'] . ' : ' . $data['symbol'] . ' : ' . $data['sell_order_id'] . ' : ';
            Log::channel('bfx_order')->info($preInfoLog . 'set sell order success !');
            Log::channel('bfx_order')->info($sellOrder);            
        } else {
            $bfxFailOrderLogs = new BfxFailOrderLogs();
            $data['message'] = $sellOrder['message'];
            $bfxFailOrderLogs->fill($data);
            $bfxFailOrderLogs->save();
        }
    }

    /**
     * @param $bfx1
     * @param $data
     * @return null
     */
    protected function buyAvailable($bfx1, $data)
    {
        $minProfitRateCond = $data['upper_min_profit_rate_cond'];
        $orderList = BfxOrderLogs::where('user_id', '=', $data['user_id'])
            ->where('symbol', '=', $data['symbol'])
            ->where('buy_order_id', '=', null)
            ->where('sell_order_id', '!=', null)
            ->where('sell_status', '!=', null)
            ->whereRaw("sell_price - (sell_price * $minProfitRateCond / 100 ) > ?", $data['buy_price'])
            ->where('status', 1)
            ->get();

        if (count($orderList) > 0) {
            // Check update available order 
            foreach ($orderList as $key => $order) {
                // Set pre info log
                $preInfoLog = $data['user_id'] . ' : ' . $order->symbol . ' : ' . $order->id . ' : ';

                // Make buy order
                $buyOrder = $bfx1->newOrder($order->symbol, $order->amount, $data['buy_price'], 'bitfinex', 'buy', 'exchange limit', true);

                if (isset($buyOrder['order_id'])) {
                    // Set order data
                    $order->buy_order_id = $buyOrder['order_id'];
                    $order->buy_price    = $buyOrder['price'];
                    $order->buy_status   = 1;
                    $order->amount       = $buyOrder['original_amount'];
                    // Calc and set profit rate
                    $order->profit_rate = round((($order->sell_price - $order->buy_price) / $order->buy_price * 100) - (env('TRADING_FEE', 0.25) * 2), 2, PHP_ROUND_HALF_DOWN);
                    $order->message = null;
                    $order->status  = 2;
                    $order->save();
                    Log::channel('bfx_order')->info($preInfoLog . 'set available buy order success !');
                } else {
                    Log::channel('bfx_order')->info($preInfoLog . 'set available buy order fail !');
                }

                Log::channel('bfx_order')->info($buyOrder);
            }
        }       
    }      
   
    /**
     * @param $bfx1
     * @param $data
     * @return null
     */
    protected function sellAvailable($bfx1, $data)
    {
        $minProfitRateCond = $data['lower_min_profit_rate_cond'];
        $orderList = BfxOrderLogs::where('user_id', '=', $data['user_id'])
            ->where('symbol', '=', $data['symbol'])
            ->where('buy_order_id', '!=', null)
            ->where('buy_status', '!=', null)
            ->where('sell_order_id', '=', null)
            ->whereRaw("buy_price + (buy_price * $minProfitRateCond / 100) < ?", $data['sell_price'])
            ->where('status', 1)
            ->get();

        if (count($orderList) > 0) {
            // Check update available order
            foreach ($orderList as $key => $order) {
                // Set pre info log
                $preInfoLog = $data['user_id'] . ' : ' . $order->symbol . ' : ' . $order->id . ' : ';

                // Make sell order
                $sellOrder = $bfx1->newOrder($order->symbol, $order->amount, $data['buy_price'], 'bitfinex', 'sell', 'exchange limit');

                if (isset($sellOrder['order_id'])) {
                    // Set order data
                    $order->sell_order_id = $sellOrder['order_id'];
                    $order->sell_price    = $sellOrder['price'];
                    $order->sell_status   = 1;
                    $order->amount        = $sellOrder['original_amount'];
                    // Calc and set profit rate
                    $order->profit_rate = round((($order->sell_price - $order->buy_price) / $order->buy_price * 100) - (env('TRADING_FEE', 0.25) * 2), 2, PHP_ROUND_HALF_DOWN);
                    $order->message = null;
                    $order->status  = 2;
                    $order->save();
                    Log::channel('bfx_order')->info($preInfoLog . 'set available sell order success !');
                } else {
                    Log::channel('bfx_order')->info($preInfoLog . 'set available sell order fail !');
                }
                    
                Log::channel('bfx_order')->info($sellOrder);
            }
        }        
    }    

    /**
     * @param $users
     * @param $data
     * @return null
     */
    protected function runUserList2($users, $data)
    {
        foreach ($users as $key => $user) {
            // Only run for test
            // if ($user->id != 1) {
            //     continue;
            // }

            $exchange = $user->exchanges[0];
            // if ($exchange->status != 1) {
            //     continue;
            // }

            // Get user's api key
            $apiKey    = $exchange->api_key;
            $apiSecret = $exchange->api_secret;

            // Init object class
            $bfx1 = new Bfx1($apiKey, $apiSecret);

            $data['user_id'] = $user->id;

            $this->sellAvailable($bfx1, $data);
            $this->buyAvailable($bfx1, $data);
        }
    }    
}
