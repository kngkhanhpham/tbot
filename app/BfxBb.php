<?php

namespace App;

use App\Bfx1;
use App\Bfx2;
use App\BfxCalc;
use App\BfxFailOrderLogs;
use App\BfxOrderLogs;
use App\BfxUserSymbols;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Log;

class BfxBb
{

    /**
     * @param array $users
     * @return null
     */
    public function handle($users = [], $symbol, $timeFrame, $candle = [])
    {
        if (empty($users) || empty($candle)) {
            return;
        }

        foreach ($users as $key => $user) {

            // Only run for test
            // if ($user->id != 1) {
            //     continue;
            // }

            $exchange = $user->exchanges[0];
            if ($exchange->status != 1) {
                continue;
            }

            // Get user's api key
            $apiKey    = $exchange->api_key;
            $apiSecret = $exchange->api_secret;

            // Init object class
            $bfx1 = new Bfx1($apiKey, $apiSecret);
            $bfx2 = new Bfx2($apiKey, $apiSecret);

            // Init data
            $data               = [];
            $data['user_id']    = $user->id;
            $data['symbol']     = $symbol;
            $data['time_frame'] = $timeFrame;              

            // Get user's symbol setting
            $setting = BfxUserSymbols::where(
                [
                    'user_id'    => $data['user_id'],
                    'symbol'     => $symbol,
                    'time_frame' => $timeFrame
                ]
            )->first();

            // // Debug
            // if ($data['user_id'] == 1) {
            //     Log::channel('bfx')->debug('setting');
            //     Log::channel('bfx')->debug($setting);
            // }

            if (! is_null($setting) && $setting->status == 1) {

                // Set base amount
                $data['base_amount_limit'] = $setting->base_amount_limit;
                $data['base_amount']       = $setting->base_amount;

                $preUserInfoLog = $data['user_id'] . ' : ' . $symbol . ' : ' . $timeFrame;
                // Log::channel('bfx')->info($preUserInfoLog);

                // Get open, high, low, current price, vol
                $priceOpen    = $candle[0][1];
                $priceHigh    = $candle[0][3];
                $priceLow     = $candle[0][4];
                $priceCurrent = $candle[0][2];
                $volCurrent   = $candle[0][5];

                if ($priceOpen == 0 || $priceCurrent == 0 || $priceHigh == 0) {
                    // Log::channel('bfx')->info($preUserInfoLog . ' : get price = 0');
                    continue;
                }

                // use BB to check make order
                // Set price rate cond
                $priceLowerRateCond = -5.0;
                $priceUpperRateCond   = 5.0;
                // Calc price rate
                $priceRate = round(($priceCurrent - $priceOpen) / $priceOpen * 100, 2);
                // Calc profit rate
                $profitRate = round(abs($priceRate) / env('BFX_BB_CALC_PROFIT_RATE', 2), 2);

                // Set bb cond list
                $listBbCond = [
                    [
                        'std'         => 5,
                        'profit_rate' => 3.0,
                    ],
                    [
                        'std'         => 4,
                        'profit_rate' => 2.5,
                    ],
                    [
                        'std'         => 3,
                        'profit_rate' => 2.0,
                    ],
                    [
                        'std'         => 2,
                        'profit_rate' => 1.5,
                    ],
                ];

                foreach ($listBbCond as $key => $bbCond) {
                    // Get BB data
                    $bb = BfxCalc::bb($candle, 20, $bbCond['std']);

                    // // Debug
                    // if ($data['user_id'] == 1) {
                    //     Log::channel('bfx')->debug('data');
                    //     Log::channel('bfx')->debug($data);
                    // }                    

                    // Buy when current price < BB lower then sell
                    // if ($data['user_id'] == 1) {
                    if ($priceCurrent < $bb['lower'] && $priceRate <= $priceLowerRateCond) {
                        // Set buy price
                        $data['buy_price'] = $priceCurrent;
                        // Set profit rate
                        // $data['profit_rate'] = $bbCond['profit_rate'];
                        $data['profit_rate'] = $profitRate;
                        // Set sell price
                        $data['sell_price'] = round($data['buy_price'] + $data['buy_price'] * ($data['profit_rate'] + env('TRADING_FEE', 0.25)) / 100, 8);
                        $data['sell_price'] = number_format($data['sell_price'], 8, '.', '');
                        // Set amount
                        $data['amount'] = round(($data['base_amount'] - ($data['base_amount'] * env('TRADING_FEE', 0.25) / 100)) / $priceCurrent, 8, PHP_ROUND_HALF_DOWN);
                        // Set condition type
                        $data['condition_type'] = 'lower BB' . $bbCond['std'];
                        // Set candle range
                        $data['candle_range'] = 1;

                        // Set price rate
                        $data['price_rate'] = $priceRate;
                        // Set price rate cond
                        $data['price_rate_cond'] = $priceLowerRateCond;
                        // Set status
                        $data['status'] = 1;

                        // Out Info Log
                        $bb['-------------'] = '-------------';
                        $bb['amount']        = $data['amount'];
                        $bb['buy_price']     = $data['buy_price'];
                        $bb['sell_price']    = $data['sell_price'];
                        $bb['profit_rate']   = $data['profit_rate'];
                        $bb['price_rate']    = $data['price_rate'];
                        $bb['price_rate_cond'] = $data['price_rate_cond'];

                        Log::channel('bfx')->info($preUserInfoLog . ' : --> BB Lower!!!');
                        Log::channel('bfx')->info($bb);
                        $this->buyToSell($bfx1, $data);
                        break;
                    }

                    // Sell when current price > BB upper then buy
                    // if ($data['user_id'] == 1) {
                    if ($priceCurrent > $bb['upper'] && $priceRate >= $priceUpperRateCond) {
                        // Set sell price
                        $data['sell_price'] = $priceCurrent;
                        // Set profit rate
                        // $data['profit_rate'] = $bbCond['profit_rate'];
                        $data['profit_rate'] = $profitRate;
                        // Set buy price
                        $data['buy_price'] = round($data['sell_price'] - $data['sell_price'] * ($data['profit_rate'] + env('TRADING_FEE', 0.25)) / 100, 8);
                        $data['buy_price'] = number_format($data['buy_price'], 8, '.', '');
                        // Set amount
                        $data['amount'] = round(($data['base_amount'] - ($data['base_amount'] * env('TRADING_FEE', 0.25) / 100)) / $priceCurrent, 8, PHP_ROUND_HALF_DOWN);
                        // Set condition type
                        $data['condition_type'] = 'upper BB' . $bbCond['std'];
                        // Set candle range
                        $data['candle_range'] = 1;
                        // Set price rate
                        $data['price_rate'] = $priceRate;
                        // Set price rate cond
                        $data['price_rate_cond'] = $priceUpperRateCond;
                        // Set status
                        $data['status'] = 1;

                        // Out Info Log
                        $bb['-------------'] = '-------------';
                        $bb['amount']        = $data['amount'];
                        $bb['buy_price']     = $data['buy_price'];
                        $bb['sell_price']    = $data['sell_price'];
                        $bb['profit_rate']   = $data['profit_rate'];
                        $bb['price_rate']    = $data['price_rate'];
                        $bb['price_rate_cond'] = $data['price_rate_cond'];

                        Log::channel('bfx')->info($preUserInfoLog . ' : --> BB Upper !!!');
                        Log::channel('bfx')->info($bb);
                        $this->sellToBuy($bfx1, $data);
                        break;
                    }
                }
            }
        }
    }

    /**
     * @param $bfx1
     * @param $data
     * @return null
     */
    protected function buyToSell($bfx1, $data)
    {
        $key = 'bfx_balances_' . $data['user_id'];
        if (Cache::has($key)) {
            $balances = Cache::get($key);
        } else {
            // Get user's balances
            $balancesRes = $bfx1->getBalances();

            if (empty($balancesRes) || isset($balancesRes['error'])) {
                Log::channel('bfx')->error('get balances error');
                Log::channel('bfx')->error($balancesRes);
            } else {

                $balances = [];
                foreach ($balancesRes as $key => $value) {
                    $balances[strtoupper($value['currency'])]['amount']    = $value['amount'];
                    $balances[strtoupper($value['currency'])]['available'] = $value['available'];
                }

                if (!empty($balances)) {
                    Cache::put($key, $balances, 1);
                }
            }
        }

        $tCurrency    = substr($data['symbol'], 0, 3);
        $baseCurrency = substr($data['symbol'], -3);
        // Log::channel('bfx')->info($baseCurrency);
        // Log::channel('bfx')->info('amount    : ' . $balances[$baseCurrency]['amount']);
        // Log::channel('bfx')->info('available : ' . $balances[$baseCurrency]['available']);

        // Balances amount > config limit amount (values calculate on base_currency)
        if (isset($balances[$tCurrency]) && $balances[$tCurrency]['amount'] * $data['buy_price'] >= $data['base_amount_limit']) {
            $bfxFailOrderLogs = new BfxFailOrderLogs();
            $data['message'] = 'balances >= base amount limit';
            $bfxFailOrderLogs->fill($data);
            $bfxFailOrderLogs->save();

            Log::channel('bfx')->info($data['message']);

            return;
        }

        if (isset($balances[$baseCurrency]['available'])) {
            // Balances base_currency < config amount base_currency
            if ($balances[$baseCurrency]['available'] < 20) {
                $bfxFailOrderLogs = new BfxFailOrderLogs();
                $data['message'] = 'The minimum order size is 20 ' . $baseCurrency;
                $bfxFailOrderLogs->fill($data);
                $bfxFailOrderLogs->save();
                return;
            }

            if ($balances[$baseCurrency]['available'] < $data['base_amount']) {
                $data['base_amount'] = $balances[$baseCurrency]['available'];
                // Calc amount again
                $data['amount'] = round(($data['base_amount'] - ($data['base_amount'] * env('TRADING_FEE', 0.25) / 100)) / $data['buy_price'], 8, PHP_ROUND_HALF_DOWN);
            }
        }

        // Make buy order
        // $buyOrder = $bfx1->newOrder($data['symbol'], $data['amount'], 1.23, 'bitfinex', 'buy', 'exchange market');
        $buyOrder = $bfx1->newOrder($data['symbol'], $data['amount'], $data['buy_price'], 'bitfinex', 'buy', 'exchange limit');

        Log::channel('bfx')->info($buyOrder);

        // Calc amount again
        $data['amount'] = round($data['amount'] - ($data['amount'] * env('TRADING_FEE', 0.25) / 100), 8, PHP_ROUND_HALF_DOWN);

        // Make sell order
        if (isset($buyOrder['order_id'])) {
            // Save buy order logs
            $bfxOrderLogs    = new BfxOrderLogs();
            $data['buy_order_id'] = $buyOrder['order_id'];
            $data['buy_price']    = $buyOrder['price'];
            $bfxOrderLogs->fill($data);
            $bfxOrderLogs->save();

            if ($buyOrder['is_live']) {
                return;
            }

            // Calc sell price
            $data['sell_price'] = round($data['buy_price'] + $data['buy_price'] * ($data['profit_rate'] + env('TRADING_FEE', 0.25)) / 100, 8);

            // Make sell order
            // $sellOrder = $bfx1->newOrder($data['symbol'], $buyOrder['original_amount'], $sellPrice, 'bitfinex', 'sell', 'exchange limit');
            $sellOrder = $bfx1->newOrder($data['symbol'], $data['amount'], $data['sell_price'], 'bitfinex', 'sell', 'exchange limit');

            Log::channel('bfx')->info($sellOrder);

            if (isset($sellOrder['order_id'])) {
                // Save sell order
                $data['sell_order_id'] = $sellOrder['order_id'];
                $bfxOrderLogs->fill($data);
                $bfxOrderLogs->save();
            }
        } else {
            $bfxFailOrderLogs = new BfxFailOrderLogs();
            $data['message'] = $buyOrder['message'];
            $bfxFailOrderLogs->fill($data);
            $bfxFailOrderLogs->save();
        }
    }

    /**
     * @param $bfx1
     * @param $data
     * @return null
     */
    protected function sellToBuy($bfx1, $data)
    {
        $key = 'bfx_balances_' . $data['user_id'];
        if (Cache::has($key)) {
            $balances = Cache::get($key);
        } else {
            // Get user's balances
            $balancesRes = $bfx1->getBalances();

            if (empty($balancesRes) || isset($balancesRes['error'])) {
                Log::channel('bfx')->error(get_class($this) . ' :: ' . $data['user_id'] . ' : get balances error');
                Log::channel('bfx')->error($balancesRes);
            } else {

                $balances = [];
                foreach ($balancesRes as $key => $value) {
                    $balances[strtoupper($value['currency'])]['amount']    = $value['amount'];
                    $balances[strtoupper($value['currency'])]['available'] = $value['available'];
                }

                if (!empty($balances)) {
                    Cache::put($key, $balances, 1);
                }
            }
        }

        $tCurrency    = substr($data['symbol'], 0, 3);
        $baseCurrency = substr($data['symbol'], -3);
        // Log::channel('bfx')->info($baseCurrency);
        // Log::channel('bfx')->info('amount    : ' . $balances[$baseCurrency]['amount']);
        // Log::channel('bfx')->info('available : ' . $balances[$baseCurrency]['available']);

        // Check Currency balance
        if (isset($balances[$tCurrency])) {
            if ($balances[$tCurrency]['available'] < $data['amount']) {
                if ($balances[$tCurrency]['available'] * $data['sell_price'] < 20) {
                    $data['message'] = 'The minimum order size is 20 USD';
                    $bfxFailOrderLogs = new BfxFailOrderLogs();
                    $bfxFailOrderLogs->fill($data);
                    $bfxFailOrderLogs->save();
                    return;
                }
                $data['amount'] = $balances[$tCurrency]['available'];
            }
        } else {
            $data['message'] = 'balances no available !';
            $bfxFailOrderLogs = new BfxFailOrderLogs();
            $bfxFailOrderLogs->fill($data);
            $bfxFailOrderLogs->save();
            return;
        }

        // Make sell order
        $sellOrder = $bfx1->newOrder($data['symbol'], $data['amount'], $data['sell_price'], 'bitfinex', 'sell', 'exchange limit');

        Log::channel('bfx')->info($sellOrder);

        if (isset($sellOrder['order_id'])) {
            // Save sell order
            $data['sell_order_id'] = $sellOrder['order_id'];
            // Save buy order logs
            $bfxOrderLogs    = new BfxOrderLogs();
            $bfxOrderLogs->fill($data);
            $bfxOrderLogs->save();

            if ($sellOrder['is_live']) {
                return;
            }

            $data['amount_inc_fee'] = round(($data['amount'] + ($data['amount'] * env('TRADING_FEE', 0.25) / 100)), 8);

            // Make buy order
            $buyOrder = $bfx1->newOrder($data['symbol'], $data['amount_inc_fee'], $data['buy_price'], 'bitfinex', 'buy', 'exchange limit');

            Log::channel('bfx')->info($buyOrder);

            if (isset($buyOrder['order_id'])) {
                // Save buy order logs
                $bfxOrderLogs    = new BfxOrderLogs();
                $data['buy_order_id'] = $buyOrder['order_id'];
                $bfxOrderLogs->fill($data);
                $bfxOrderLogs->save();
            }
        } else {
            $bfxFailOrderLogs = new BfxFailOrderLogs();
            $data['message'] = $sellOrder['message'];
            $bfxFailOrderLogs->fill($data);
            $bfxFailOrderLogs->save();
        }
    }
}
