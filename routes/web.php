<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    if (Auth::guest()) {
        return view('auth.login');
    } else {
        return redirect()->route('home');
    }
});

Auth::routes();

Route::get('/logout', function(){
   Auth::logout();
   return Redirect::to('login');
});

// Change Password Routes...
Route::get('/change-password', 'Auth\ChangePasswordController@showChangePasswordForm')->name('auth.change_password');
Route::patch('/change-password', 'Auth\ChangePasswordController@changePassword')->name('auth.change_password');

Route::get('/home', 'HomeController@index')->name('home');
Route::get('/passport', 'PassportController@index')->name('passport');
Route::get('/test', 'TestController@index')->name('test');
Route::get('/test/api/bfx', 'TestController@bfx_api')->name('test.api.bfx');
Route::get('/test/api/btx', 'TestController@btx_api')->name('test.api.btx');
Route::get('/test/resetpw', 'TestController@resetpw')->name('test.resetpw');

// Exchanges
Route::get('/', 'ExchangeController@index')->name('exchange');

Route::group(['namespace' => 'Bfx'], function () {
    // Bfx index
    Route::get('/bfx', 'IndexController@index')->name('bfx');
    // Bfx Api key
    Route::get('/bfx/setting/apikey', 'SettingController@apikey')->name('bfx.setting.apikey');
    Route::post('/bfx/setting/apikey', 'SettingController@apikey')->name('bfx.setting.apikey');
    // Bfx symbol
    Route::get('/bfx/setting/symbol', 'SettingController@symbol')->name('bfx.setting.symbol');
    Route::post('/bfx/setting/symbol', 'SettingController@symbol')->name('bfx.setting.symbol');
    // Bfx time frame
    Route::get('/bfx/setting/timeframe', 'SettingController@timeframe')->name('bfx.setting.timeframe');
    Route::post('/bfx/setting/timeframe', 'SettingController@timeframe')->name('bfx.setting.timeframe');
    // Bfx Balance
    Route::get('/bfx/balance', 'BalanceController@index')->name('bfx.balance');      
    // Bfx Symbol
    Route::get('/bfx/symbol', 'SymbolController@index')->name('bfx.symbol');
    Route::get('/bfx/symbol/edit', 'SymbolController@edit')->name('bfx.symbol.edit');
    Route::post('/bfx/symbol/edit', 'SymbolController@edit')->name('bfx.symbol.edit');
    Route::get('/bfx/symbol/set', 'SymbolController@set')->name('bfx.symbol.set');
    Route::post('/bfx/symbol/set', 'SymbolController@set')->name('bfx.symbol.set');
    Route::get('/bfx/symbol/delete', 'SymbolController@delete')->name('bfx.symbol.delete'); 
    Route::get('/bfx/symbol/default', 'SymbolController@default')->name('bfx.symbol.default');      
    // Bfx Orders (AT)  
    Route::get('/bfx/order-at', 'OrderATController@index')->name('bfx.order-at');
    Route::get('/bfx/order-at/edit', 'OrderATController@edit')->name('bfx.order-at.edit');
    Route::post('/bfx/order-at/edit', 'OrderATController@edit')->name('bfx.order-at.edit');
    Route::get('/bfx/order-at/new', 'OrderATController@new')->name('bfx.order-at.new');
    Route::post('/bfx/order-at/new-confirm', 'OrderATController@newConfirm')->name('bfx.order-at.new-confirm');
    Route::post('/bfx/order-at/new-save', 'OrderATController@newSave')->name('bfx.order-at.new-save');
});

Route::group(['namespace' => 'Btx'], function () {
    // Btx index
    Route::get('/btx', 'IndexController@index')->name('btx');
    // Btx Api key
    Route::get('/btx/setting/apikey', 'SettingController@apikey')->name('btx.setting.apikey');
    Route::post('/btx/setting/apikey', 'SettingController@apikey')->name('btx.setting.apikey');
    // Btx symbol
    Route::get('/btx/setting/symbol', 'SettingController@symbol')->name('btx.setting.symbol');
    Route::post('/btx/setting/symbol', 'SettingController@symbol')->name('btx.setting.symbol');    
    // Bfx time frame
    Route::get('/btx/setting/timeframe', 'SettingController@timeframe')->name('btx.setting.timeframe');
    Route::post('/btx/setting/timeframe', 'SettingController@timeframe')->name('btx.setting.timeframe');    
    // Btx Balance
    Route::get('/btx/balance', 'BalanceController@index')->name('btx.balance');    
    // Btx Symbol
    Route::get('/btx/symbol', 'SymbolController@index')->name('btx.symbol');
    Route::get('/btx/symbol/edit', 'SymbolController@edit')->name('btx.symbol.edit');
    Route::post('/btx/symbol/edit', 'SymbolController@edit')->name('btx.symbol.edit');
    Route::get('/btx/symbol/set', 'SymbolController@set')->name('btx.symbol.set');
    Route::post('/btx/symbol/set', 'SymbolController@set')->name('btx.symbol.set');    
    Route::get('/btx/symbol/delete', 'SymbolController@delete')->name('btx.symbol.delete'); 
    Route::get('/btx/symbol/default', 'SymbolController@default')->name('btx.symbol.default');      
    // Btx Orders (AT)  
    Route::get('/btx/order-at', 'OrderATController@index')->name('btx.order-at');
    Route::get('/btx/order-at/edit', 'OrderATController@edit')->name('btx.order-at.edit');
    Route::post('/btx/order-at/edit', 'OrderATController@edit')->name('btx.order-at.edit'); 
    Route::get('/btx/order-at/new', 'OrderATController@new')->name('btx.order-at.new');
    Route::post('/btx/order-at/new-confirm', 'OrderATController@newConfirm')->name('btx.order-at.new-confirm');
    Route::post('/btx/order-at/new-save', 'OrderATController@newSave')->name('btx.order-at.new-save');
    // Btx Orders (MT)  
    Route::get('/btx/order-mt', 'OrderMTController@index')->name('btx.order-mt');
    Route::get('/btx/order-mt/edit', 'OrderMTController@edit')->name('btx.order-mt.edit');
    Route::post('/btx/order-mt/edit', 'OrderMTController@edit')->name('btx.order-mt.edit');     
    Route::post('/btx/order-mt/edit/confirm', 'OrderMTController@editConfirm')->name('btx.order-mt.edit-confirm');     
    Route::post('/btx/order-mt/edit/save', 'OrderMTController@editSave')->name('btx.order-mt.edit-save');     
    Route::get('/btx/order-mt/cancel', 'OrderMTController@cancel')->name('btx.order-mt.cancel');
});