<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateBtxFailOrderLogsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('btx_fail_order_logs', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id');
            $table->string('symbol');
            $table->string('time_frame');
            $table->string('buy_order_id')->nullable();
            $table->string('buy_price')->nullable();
            $table->string('sell_order_id')->nullable();
            $table->string('sell_price')->nullable();
            $table->string('amount')->nullable();
            $table->string('condition_type');
            $table->integer('candle_range')->nullable();
            $table->string('profit_rate')->nullable();
            $table->string('vol_current')->nullable();
            $table->string('vol_current_cond')->nullable();   
            $table->string('vol_rate')->nullable();
            $table->string('vol_rate_cond')->nullable(); 
            $table->string('price_rate')->nullable();
            $table->string('price_rate_cond')->nullable();                                
            $table->tinyinteger('status')->nullable();
            $table->string('message')->nullable();
            $table->timestamps();

            $table->index(['user_id', 'symbol', 'time_frame']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('btx_fail_order_logs');
    }
}
