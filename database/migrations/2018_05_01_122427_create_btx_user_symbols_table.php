<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateBtxUserSymbolsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('btx_user_symbols', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id');
            $table->string('symbol');
            $table->string('time_frame');
            $table->decimal('base_amount_limit', 8, 2);
            $table->decimal('base_amount', 8, 2);
            $table->decimal('stoploss_rate', 4, 2)->nullable();
            $table->tinyinteger('status')->default(2);
            $table->text('options')->nullable();
            $table->timestamps();

            $table->index(['user_id', 'symbol', 'time_frame']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('btx_user_symbols');
    }
}
