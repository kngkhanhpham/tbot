<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateBtxOrdersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('btx_orders', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id');
            $table->string('symbol');
            $table->string('side');
            $table->string('order_id');
            $table->string('amount');
            $table->string('price');
            $table->tinyinteger('status');
            $table->string('message')->nullable();
            $table->timestamps();

            $table->index(['user_id', 'symbol']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('btx_orders');
    }
}
