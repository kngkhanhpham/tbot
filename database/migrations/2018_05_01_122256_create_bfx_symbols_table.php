<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateBfxSymbolsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('bfx_symbols', function (Blueprint $table) {
            $table->increments('id');
            $table->string('base_currency');
            $table->string('symbol');
            $table->tinyinteger('status')->default(2);
            $table->timestamps();

            $table->index(['base_currency']);
            $table->index(['symbol']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('bfx_symbols');
    }
}
