<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateBTxTimeframesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('btx_timeframes', function (Blueprint $table) {
            $table->increments('id');
            $table->string('time_frame');
            $table->string('time_frame_name');
            $table->tinyinteger('status')->default(2);
            $table->timestamps();

            $table->index(['time_frame']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('btx_timeframes');
    }
}
