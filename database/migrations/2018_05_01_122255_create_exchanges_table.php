<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateExchangesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('exchanges', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id');
            $table->string('exchange');
            $table->string('api_key');
            $table->string('api_secret');
            $table->string('api_key1')->nullable();
            $table->string('api_secret1')->nullable();            
            $table->string('api_key2')->nullable();
            $table->string('api_secret2')->nullable();      
            $table->string('api_key3')->nullable();
            $table->string('api_secret3')->nullable();                    
            $table->tinyinteger('status')->default(2);
            $table->timestamps();

            $table->index(['user_id', 'exchange']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('exchanges');
    }
}
