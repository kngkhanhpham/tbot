function getRootUrl() {
  return window.location.origin?window.location.origin+'/':window.location.protocol+'/'+window.location.host+'/';
}

function getBaseUrl() {
  var re = new RegExp(/^.*\//);
  return re.exec(window.location.href)[0];
}

// Filter Date
$(document).on("change", "#year, #month, #day, #user_id", function() {
  $.ajaxSetup({
    headers:{
      'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content'),
      'X-Requested-With': 'XMLHttpRequest',
    }
  });

  var user_id = $('#user_id').val();
  if (user_id) {
    user_id = 'user_id=' + user_id;
  } else {
    user_id = '';
  }

  var year = $('#year').val();
  var month = $('#month').val();
  var day = $('#day').val();

  var pathname = window.location.pathname;
  if (pathname == '/bfx') {
    window.location.href = getBaseUrl() + 'bfx?' + user_id + '&year=' + year + '&month=' + month + '&day=' + day;
  }

});

// Search by symbol, timeframe action
$(document).on("change", "#search_symbol, #search_time_frame, #user_id", function() {
  $.ajaxSetup({
    headers:{
      'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content'),
      'X-Requested-With': 'XMLHttpRequest',
    }
  });

  var user_id = $('#user_id').val();
  if (user_id) {
    user_id = 'user_id=' + user_id;
  } else {
    user_id = '';
  }

  var pathname = window.location.pathname;
  if (pathname == '/bfx/balance') {
    window.location.href = getBaseUrl() + 'balance?' + user_id;
  } else if (pathname == '/bfx/symbol') {
    var search_symbol = $('#search_symbol').val();
    var search_time_frame = $('#search_time_frame').val();
    window.location.href = getBaseUrl() + 'symbol?' + user_id + '&search_symbol=' + search_symbol + '&search_time_frame=' + search_time_frame;
  }

});

// Change symbol, time_frame action
$(document).on("change", "#symbol, #time_frame", function() {
  $.ajaxSetup({
    headers:{
      'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content'),
      'X-Requested-With': 'XMLHttpRequest',
    }
  });

  var data = {
    symbol : $('#symbol').val(),
    time_frame : $('#time_frame').val(),
  };

  $.ajax({
    url: "/api/get-bfx-symbol",
    method: 'GET',
    data: data,
    success: function(res) {
      // console.log(res);
      if (res.success && res.data) {
        var fields = res.data.fields;
        show(fields);
      }
    }
  });
});

// Show data on screen
function show(fields)
{
  $.each(fields, function(key, field) {

    if (field.type == 'text') {
      if (field.items) {
        $.each(field.items, function(sub_key, item) {

          if (item.value) {
            $('[name="'+key+'_'+sub_key+'"]').val(item.value);
          } else {
            $('[name="'+key+'_'+sub_key+'"]').val(item.default);
          }
        });

      } else {
        if (field.value) {
          $('[name="'+key+'"]').val(field.value);
        } else {
          $('[name="'+key+'"]').val(field.default);
        }
      }
    }

    if (field.type == 'checkbox') {
      if (field.value) {
        if (field.value == 1) {
          $('[id="'+key+'"]').prop('checked', true);
        } else {
          $('[id="'+key+'"]').prop('checked', false);
        }
      } else {
        if (field.checked == true) {
          $('[id="'+key+'"]').prop('checked', true);
        } else {
          $('[id="'+key+'"]').prop('checked', false);
        }
      }
    }

    if (field.type == 'select') {
      if (key != 'symbol' && key != 'time_frame') {
        if (field.value) {
          $('[id="'+key+'"]').val(field.value);
        } else {
          $('[id="'+key+'"]').val(field.default);
        }
      }
    }

  });
}

// Check all symbols
$(document).on("click", "input[name=all_symbol]", function() {

  $.ajaxSetup({
    headers:{
      'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content'),
      'X-Requested-With': 'XMLHttpRequest',
    }
  });

  if ($(this).prop( "checked")) {
    $("input[name*=symbol]").prop( "checked", true);
  } else {
    $("input[name*=symbol]").prop( "checked", false);
  }

});

// Set default symbols
$(document).on("click", "#default_symbols", function() {

  if (confirm("Are you sure you want to set default for all symbols ?")) {
    window.location.href = getBaseUrl() + 'symbol/default';
  }
  else {
      return false;
  }

});

// delete symbol
function delete_symbol(id, symbol, time_frame) {
  if (confirm('Are you sure you want to delete symbol "' + symbol + ' ' + time_frame + '" ?')) {
    window.location.href = getBaseUrl() + 'symbol/delete?id=' + id;
  }
  else {
    return false;
  }
}
