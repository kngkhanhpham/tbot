function getRootUrl() {
  return window.location.origin?window.location.origin+'/':window.location.protocol+'/'+window.location.host+'/';
}

function getBaseUrl() {
  var re = new RegExp(/^.*\//);
  return re.exec(window.location.href)[0];
}

$(document).on("change", "#search_symbol, #search_time_frame, #search_status, #user_id", function() {

  $.ajaxSetup({
    headers:{
      'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content'),
      'X-Requested-With': 'XMLHttpRequest',
    }
  });

  var user_id = $('#user_id').val();
  if (user_id) {
    user_id = 'user_id=' + user_id + '&';
  } else {
    user_id = '';
  }  

  var search_symbol = $('#search_symbol').val();
  var search_time_frame = $('#search_time_frame').val();
  var search_status = $('#search_status').val();
  
  var pathname = window.location.pathname;
  if (pathname == '/btx/order-at') {
    window.location.href = getBaseUrl() + 'order-at?' + user_id + 'search_symbol=' + search_symbol + '&search_time_frame=' + search_time_frame + '&search_status=' + search_status;
  } else {
    window.location.href = getBaseUrl() + 'order-mt?' + user_id + 'search_symbol=' + search_symbol;
  }
  
});

$(document).on("change", "#buy_price, #sell_price", function() {
  $.ajaxSetup({
    headers:{
      'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content'),
      'X-Requested-With': 'XMLHttpRequest',
    }
  });

  var buy_price = $('#buy_price').val();
  var sell_price = $('#sell_price').val();
  if ($.isNumeric(buy_price) && $.isNumeric(sell_price)) {
    var profit_rate = ((sell_price - buy_price) / buy_price * 100) - 0.5;
    $('#profit_rate').val(profit_rate.toFixed(1) + '%');    
  } else {
    alert('Please input "Buy/Sell Price" must be a numeric !');
  }

});
