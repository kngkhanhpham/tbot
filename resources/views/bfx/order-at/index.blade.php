@extends('layouts.bfx')

<?php
    $timeFrame = trans('bfx.time_frame');
?>

@section('content')

<div class="card-body">
  <div class="form-group row">
    <?php
        $key   = 'search_symbol';
        $field = [
            'label'    => 'Symbol',
            'type'     => 'text',
            'class'    => 'form-control',
            'required' => false,
            'default'  => '',
        ];
    ?>
    {!! Form::label($key, $field['label'], ['class' => 'col-sm-2 col-form-label text-sm-right']) !!}
    <div class="col-sm-2">
      {!! Form::text($key, old($key, $input[$key]), ['class' => $errors->has($key) ? $field['class'] . ' is-invalid' : $field['class'], $field['required'] ? 'required' : '', 'id' => $key]) !!}
    </div>

    <?php
        $key   = 'search_time_frame';
        $field = [
            'label'    => 'Time Frame',
            'type'     => 'select',
            'options'  => $timeFrame,
            'class'    => 'form-control',
            'required' => false,
            'default'  => '',
        ];
    ?>
    {!! Form::label($key, $field['label'], ['class' => 'col-sm-2 col-form-label text-sm-right' ]) !!}
    <div class="col-sm-2">
      {!! Form::select($key, $field['options'], old($key, $input[$key]), ['class' => $errors->has($key) ? $field['class'] . ' is-invalid' : $field['class'], $field['required'] ? 'required' : '', 'id' => $key]) !!}
    </div>

    <?php
        $key   = 'search_status';
        $field = [
            'label'    => 'Status',
            'type'     => 'select',
            'options'  => __('bfx.order_status'),
            'class'    => 'form-control',
            'required' => false,
            'default'  => '',
        ];
    ?>
    {!! Form::label($key, $field['label'], ['class' => 'col-sm-2 col-form-label text-sm-right']) !!}
    <div class="col-sm-2">
      {!! Form::select($key, $field['options'], old($key, $input[$key]), ['class' => $errors->has($key) ? $field['class'] . ' is-invalid' : $field['class'], $field['required'] ? 'required' : '', 'id' => $key]) !!}
    </div>
  </div>

  @if (Auth::id() == 1)
  <div class="form-group row">
    @include('partials.user-list', ['input' => $input, 'userList' => $userList])
  </div>
  <div class="form-group row">
    <div class="col-md-6 offset-md-2">
    {!! link_to(route('bfx.order-at.new'), 'Add New', ['class' => 'btn btn-primary']) !!}
    </div>  
  </div>  
  @endif 

</div>
<div class="card-body">
  <div class="table-responsive-sm">
    {{ $orders->appends($input)->links('partials.pagination.default') }}

    <table class="table table-hover table-bordered">
      <thead>
        <tr>
          <th>{{ __('Date Time') }}</th>
          <th>{{ __('Sym') }}</th>
          <th>{{ __('Amount') }}<br/>{{ __('Buy') }}/{{ __('Sell') }}<br/>{{ __('Profit') }}</th>
          <th>{{ __('Type/Status') }}</th>
        </tr>
      </thead>

      <tbody>
        @foreach ($orders as $order)
        <tr>
          <td>
           {{ $order->updated_at }}
           <br/>{{ $order->created_at }}
         </td>
         <td>
          @if ($order->status == 1 && $input['search_status'] != 'f')
          <?php
              if (isset($input['user_id']) && !empty($input['user_id'])) {
                  $params['user_id'] = $input['user_id'];
              }          
              $params['id'] = $order->id;
          ?>
          <a href="{{ route('bfx.order-at.edit', $params) }}">
           {{ substr($order->symbol, 0, 3) }}
           <br/>{{ substr($order->symbol, 3, 5) }}
         </a>
         @else
         {{ substr($order->symbol, 0, 3) }}
         <br/>{{ substr($order->symbol, 3, 5) }}
         @endif
       </td>
       <td>
         {{ $order->amount }}
         @if (!empty($order->buy_price)) <br/><span style="color: green">{{ $order->buy_price }}</span> @endif
         @if (!empty($order->sell_price)) <br/><span style="color: red">{{ $order->sell_price, 2 }}</span> @endif
         @if (!empty($order->profit_rate)) <br/>{{ $order->profit_rate }}% @endif
       </td>
      <td>
        {{ $order->condition_type }}
        {{ $timeFrame[$order->time_frame] }}
        @if ($input['search_status'] == 'f')
        <p style="word-wrap:break-word; white-space:normal; max-width:200px;">
          {{ $order->message }}
        </p>    
        @else    
        <br/>({{ __('bfx.order_status')[$order->status] }})
        @endif
      </td>
   </tr>
   @endforeach
 </tbody>
</table>

{{ $orders->appends($input)->links('partials.pagination.default') }}
</div>
</div>
@endsection
