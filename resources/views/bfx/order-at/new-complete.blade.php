@extends('layouts.bfx')

@section('content')

<!-- include show message -->
@include('partials.alert')

<div class="card-body">
  <div class="form-group row mb-0">
    <div class="col-md-6 offset-md-4">
      @if (isset($order['error']))
      <span style="font-size: 18px; color: red"> Order Failed ! </span>
      <br/> {!! $order['message'] !!}
      @else
      <span style="font-size: 18px; color: red"> Order Success ! </span>
      @foreach ($order as $key => $value)
        <br/>{!! $key !!} : {!! $value !!}
      @endforeach 
      @endif
    </div>

  </div>
</div>

<div class="card-body">
  <div class="form-group row mb-0">
    <div class="col-md-6 offset-md-4">
      {!! link_to(route('bfx.order-at'), 'Back', ['class' => 'btn btn-primary']) !!}
    </div>
  </div>
  {!! Form::close() !!}
</div>

@endsection
