@extends('layouts.bfx')

@section('content')

<!-- include show message -->
@include('partials.alert')

<div class="card-body">
  {!! Form::open(['url' => route('bfx.order-at.new-confirm'), 'method' => 'post']) !!}

  @if (Auth::id() == 1)
  <div class="form-group row">
    @include('partials.user-list2', ['input' => $input, 'userList' => $userList])
  </div>
  @endif

  <?php $params = [];?>
  @if (isset($input['user_id']))
  <?php
      $params['user_id'] = $input['user_id'];
  ?>
  @endif

  <?php 
    if (empty($input['symbol'])) {
      $partial = 'partials.form-item.create';
    } else {
      $partial = 'partials.form-item.edit';
    }
    
  ?>

  @foreach ($fields as $key => $field)
  @include($partial, ['field' => $field])
  @endforeach


  <div class="form-group row mb-0">
    <div class="col-md-6 offset-md-4">
      {!! Form::submit('Confirm', ['class' => 'btn btn-primary']) !!}  {!! link_to(route('bfx.order-at', $params), 'Back', ['class' => 'btn btn-primary']) !!}
    </div>
  </div>
  {!! Form::close() !!}
</div>

@endsection
