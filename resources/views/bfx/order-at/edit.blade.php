@extends('layouts.bfx')

@section('content')

<!-- include show message -->
@include('partials.alert')

<div class="card-body">
  {!! Form::open(['url' => route('bfx.order-at.edit'), 'method' => 'post']) !!}

  <?php $params = [];?>
  @if (isset($input['user_id']))
  {!! Form::hidden('user_id', $input['user_id']) !!}
  <?php
$params['user_id'] = $input['user_id'];
?>
  @endif

  @foreach ($fields as $key => $field)
  @if($key == 'amount')
  <div class="form-group row">
    {!! Form::label('condition', 'Conditions', ['class' => 'col-md-4 col-form-label text-md-right']) !!}
    <div class="col-md-6">
      {{ $data->condition_type }}
      {{ __('bfx.time_frame')[$data->time_frame] }}
      {{ $data->candle_range }}

    </div>
  </div>
  @endif
  @include('partials.form-item.edit', ['field' => $field])
  @endforeach

  <div class="form-group row mb-0">
    <div class="col-md-6 offset-md-4">
      {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}  {!! link_to(route('bfx.order-at', $params), 'Back', ['class' => 'btn btn-primary']) !!}
    </div>
  </div>
  {!! Form::close() !!}
</div>


@endsection
