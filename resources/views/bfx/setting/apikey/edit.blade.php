@extends('layouts.bfx')

@section('content')

<!-- include show message -->
@include('partials.alert')

<div class="card-body">
  {!! Form::open(['url' => route('bfx.setting.apikey'), 'method' => 'post']) !!}

  @if (isset($input['user_id']))
  {!! Form::hidden('user_id', $input['user_id']) !!}
  @endif 

  <?php 
  if ($action == 'edit') {
    $partial = 'partials.form-item.edit';
  } else {
    $partial = 'partials.form-item.create';
  }
  ?>

  @php $fields = __('bfx.apikey'); @endphp
  @foreach ($fields as $key => $field)
  @include($partial, ['field' => $field])
  @endforeach

  <div class="form-group row mb-0">
    <div class="col-md-6 offset-md-4">
      {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}  {!! link_to(route('bfx'), 'Back', ['class' => 'btn btn-primary']) !!}
    </div>
  </div>
  {!! Form::close() !!}
</div>


@endsection
