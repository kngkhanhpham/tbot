<?php
$links = [
    'bfx.order-at' => 'Orders',
    'bfx.balance' => 'Balances',
    'bfx.symbol' => 'Symbols',
    'bfx.setting.apikey' => 'ApiKey',
];

if (Auth::id() == 1) {
    $links['bfx.setting.symbol'] = 'Symbol Management';
    $links['bfx.setting.timeframe'] = 'Timeframe Management';
}

$param = [];
if (isset($input['base_currency'])) {
    $param['base_currency'] = $input['base_currency'];
}

if (isset($input['symbol'])) {
    $param['symbol'] = $input['symbol'];
}

if (isset($input['time_frame'])) {
    $param['time_frame'] = $input['time_frame'];
}
?>

@foreach ($links as $key => $value)
      {!! link_to(route($key), $value, ['class' => 'btn btn-outline-primary']) !!}
@endforeach

@if (Route::currentRouteName() == 'bfx.symbol')
<div style="padding-top: 5px">
{!! link_to(route('bfx.symbol.set', $param), 'Set symbols', ['class' => 'btn btn-outline-primary']) !!}
{!! link_to('#', 'Set symbols default', ['id'=> 'default_symbols', 'class' => 'btn btn-outline-primary']) !!}
</div>
@endif

@if (Route::currentRouteName() == 'bfx.symbol.edit')
<div style="padding-top: 5px">
{!! link_to(route('bfx.symbol.set', $param), 'Set symbols', ['class' => 'btn btn-outline-primary']) !!}
</div>
@endif
