<?php
$links = [
    'bfx.order-at' => 'Orders',
    'bfx.balance' => 'Balances',
    'bfx.symbol' => 'Symbols',
    'bfx.setting.apikey' => 'ApiKey',
];

if (Auth::id() == 1) {
    $links['bfx.setting.symbol'] = 'Symbol Management';
    $links['bfx.setting.timeframe'] = 'Timeframe Management';
}

$param = [];
if (isset($input['base_currency'])) {
    $param['base_currency'] = $input['base_currency'];
}

if (isset($input['symbol'])) {
    $param['symbol'] = $input['symbol'];
}

if (isset($input['time_frame'])) {
    $param['time_frame'] = $input['time_frame'];
}
?>

@foreach ($links as $key => $value)
<a class="dropdown-item" href="{{ route($key) }}">{{ $value }}</a>
@endforeach

