@extends('layouts.bfx')

@section('content')

<div class="card-body"> 
  <div class="form-group row">
    @include('partials.date-filter', ['input' => $input])
  </div>

  @if (Auth::id() == 1)
    <div class="form-group row">
      @include('partials.user-list', ['input' => $input, 'userList' => $userList])
    </div>
  @endif  
</div>

<?php 
$total_color = 'green';
if ($result['total_profit'] < 0) {
  $total_color = 'red';
}
?>
<div class="card-body">
  <span style="color: black; font-size: 18px;"><strong>Buy : ${{ $result['total_buy'] }} | </strong></span>&nbsp
  <span style="color: black; font-size: 18px;"><strong>Sell : ${{ $result['total_sell'] }} | </strong></span>&nbsp
  <span style="color: black; font-size: 18px;"><strong>Profit : </strong><strong style="color: {{ $total_color }}; font-size: 18px;">${{ $result['total_profit'] }} | </strong></span>&nbsp
  <span style="color: black; font-size: 18px;"><strong>Profit Rate : </strong><strong style="color: {{ $total_color }}; font-size: 18px;">{{ $result['total_profit_rate'] }}%</strong></span>&nbsp   
</div>

<div class="card-body"> 
  <div class="table-responsive-sm">
   {{ $symbols->appends($input)->links('partials.pagination.default') }}

   <table class="table table-hover table-bordered">
    <thead>
      <tr>
        <th>{{ __('Symbol') }}</th>
        <th>{{ __('Buy') }}</th>
        <th>{{ __('Sell') }}</th>
        <th>{{ __('Profit') }}</th>
        <th>{{ __('Profit Rate') }}</th>
      </tr>
    </thead>

    <tbody>
      @foreach ($symbols as $symbol => $value)
      <?php 
      $sub_total_color = 'green';
      if ($value['sub_profit'] < 0) {
        $sub_total_color = 'red';
      }
      ?>

      <tr>
        <td>
          {{ $symbol }}
        </td>                
        <td>
        ${{ round($value['sub_total_buy'], 2) }}
        </td>
        <td>
        ${{ round($value['sub_total_sell'], 2) }}
        </td>
        <td>
        <span style="color: {{ $sub_total_color }}; font-size: 16px;">${{ round($value['sub_profit'], 2) }}</span>
        </td>
        <td>
        <span style="color: {{ $sub_total_color }}; font-size: 16px;">{{ round($value['sub_profit_rate'], 2) }}%</span>
        </td>             
      </tr>
      @endforeach      
    </tbody>
  </table>

  {{ $symbols->appends($input)->links('partials.pagination.default') }}
</div>
</div>
@endsection
