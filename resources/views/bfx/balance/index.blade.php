@extends('layouts.bfx')

@section('content')

@if (Auth::id() == 1)
<div class="card-body"> 
  <div class="form-group row">
    @include('partials.user-list', ['input' => $input, 'userList' => $userList])
  </div>
</div>
@endif

<div class="card-body">
  <span style="color: green; font-size: 18px;"><strong>Total</strong></span>
  &nbsp&nbsp&nbsp
  <i class="fas fa-dollar-sign" style="color: green; font-size: 18px;"></i>
  <span style="color: green; font-size: 18px;"><strong>{{ $totalUSD }}</strong></span>
  &nbsp&nbsp&nbsp
  <i class="fab fa-btc" style="color: green; font-size: 18px;"></i>
  <span style="color: green; font-size: 18px;"><strong>{{ $totalBTC }}</strong></span>
</div>

<div class="card-body"> 
  <div class="table-responsive-sm">
   {{ $balances->appends($input)->links('partials.pagination.default') }}

   <table class="table table-hover table-bordered">
    <thead>
      <tr>
        <th>{{ __('Symbol') }}</th>
        <th>{{ __('Balance') }}</th>
        <th>{{ __('Est.USD Value') }}</th>
      </tr>
    </thead>

    <tbody>
      @foreach ($balances as $balance)
      <tr>
        <td>
          <strong>{{ $balance['currency'] == 'BAB' ? 'BCH' : $balance['currency'] }}</strong>
        </td>                
        <td>
          <span style="color: red">{{ $balance['reserved'] }}</span>
          <br/><span style="color: green"><strong>{{ $balance['available'] }}</strong></span>
          <br/><span style="font-size: 18px;"><strong>{{ $balance['total'] }}</strong></span>
        </td>
        <td>
          <span style="color: red">{{ $balance['Est.USD']['reserved'] }}</span>
          <br/><span style="color: green"><strong>{{ $balance['Est.USD']['available'] }}</strong></span>
          <br/><span style="font-size: 18px;"><strong>{{ $balance['Est.USD']['total'] }}</strong></span>
        </td>               
      </tr>
      @endforeach      
    </tbody>
  </table>

  {{ $balances->appends($input)->links('partials.pagination.default') }}
</div>
</div>
@endsection
