@extends('layouts.app')

<?php
    $func = [
        'btx'                       => 'Home',
        'btx.symbol'                => 'Symbols',
        'btx.symbol.set'            => 'Set Symbol',
        'btx.symbol.edit'           => 'Edit Symbol',
        'btx.order-at'              => 'Orders',
        'btx.order-at.edit'         => 'Edit Order',
        'btx.order-at.new'          => 'New Order',
        'btx.order-at.new-confirm'  => 'New Order',
        'btx.order-at.new-save'     => 'New Order',        
        'btx.setting.apikey'        => 'ApiKey',
        'btx.setting.symbol'        => 'Symbol List',
        'btx.setting.timeframe'     => 'Time Frame',
        'btx.balance'               => 'Balance',
        // 'btx.order-mt'              => 'Orders(MT)',
        // 'btx.order-mt.edit'         => 'Edit Order(MT)',
        // 'btx.order-mt.edit-confirm' => 'Confirm Edit Order(MT)',
    ];
?>

@section('main_content')
<div class="container">
  <div class="row justify-content-center">
    <div class="col-md-10">
      <div class="card">
        <div class="card-header"><a href="{{ route('btx') }}"><img src="{!! __('exchange.list')['btx']['logo'] !!}"></a> >> {{ $func[Route::currentRouteName()] }}</div>
        <!-- <div class="card-body">@include('btx.nav-link.top')</div> -->
        <div class="card-body">@include('btx.nav-link.links')</div>
        @yield('content')
        <!-- <div class="card-body">@include('btx.nav-link.bottom')</div> -->
        <div class="card-body">@include('btx.nav-link.links')</div>
      </div>
    </div>
  </div>
</div>
@endsection

