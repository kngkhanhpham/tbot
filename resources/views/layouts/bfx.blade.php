@extends('layouts.app')

<?php
    $func = [
        'bfx'                   => 'Home',
        'bfx.symbol'            => 'Symbols',
        'bfx.symbol.set'        => 'Set Symbol',
        'bfx.symbol.edit'       => 'Edit Symbol',
        'bfx.order-at'          => 'Orders',
        'bfx.order-at.edit'     => 'Edit Order',
        'bfx.order-at.new'          => 'New Order',
        'bfx.order-at.new-confirm'  => 'New Order',
        'bfx.order-at.new-save'     => 'New Order',
        'bfx.setting.apikey'    => 'ApiKey',
        'bfx.setting.symbol'    => 'Symbol List',
        'bfx.setting.timeframe' => 'Time Frame',
        'bfx.balance'           => 'Balance',
    ];
?>

@section('main_content')
<div class="container">
  <div class="row justify-content-center">
    <div class="col-md-10">
      <div class="card">
        <div class="card-header"><a href="{{ route('bfx') }}"><img src="{!! __('exchange.list')['bfx']['logo'] !!}"></a> >> {{ $func[Route::currentRouteName()] }}</div>
        <!-- <div class="card-body">@include('bfx.nav-link.top')</div> -->
        <div class="card-body">@include('bfx.nav-link.links')</div>
        @yield('content')
        <!-- <div class="card-body">@include('bfx.nav-link.bottom')</div> -->
        <div class="card-body">@include('bfx.nav-link.links')</div>
      </div>
    </div>
  </div>
</div>
@endsection

