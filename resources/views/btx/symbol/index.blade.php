@extends('layouts.btx')

@section('content')

<!-- include show message -->
@include('partials.alert')

@if (isset($input['user_id']))
{!! Form::hidden('user_id', $input['user_id']) !!}
<?php 
$params['user_id'] = $input['user_id'];
?>   
@endif 

<div class="card-body">
  <div class="form-group row">
    <?php
    $key = 'search_symbol'; 
    $field = [            
      'label'    => 'Symbol',
      'type'     => 'text',
      'class'    => 'form-control',
      'required' => false,
      'default'  => '',
    ];
    ?>
    {!! Form::label($key, $field['label'], ['class' => 'col-sm-2 col-form-label text-sm-right']) !!}

    <div class="col-md-3">
      {!! Form::text($key, old($key, $input[$key]), ['class' => $errors->has($key) ? $field['class'] . ' is-invalid' : $field['class'], $field['required'] ? 'required' : '', 'id' => $key]) !!}
    </div>

    <?php 
    $key = 'search_time_frame'; 
    $field = [            
      'label'    => 'Time Frame',
      'type'     => 'select',
      'options'  => $timeframes,
      'class'    => 'form-control',
      'required' => false,
      'default'  => '',
    ];
    ?>
    {!! Form::label($key, $field['label'], ['class' => 'col-sm-2 col-form-label text-sm-right' ]) !!}

    <div class="col-md-3">
      {!! Form::select($key, $field['options'], old($key, $input[$key]), ['class' => $errors->has($key) ? $field['class'] . ' is-invalid' : $field['class'], $field['required'] ? 'required' : '', 'id' => $key]) !!}
    </div>
  </div>    

  @if (Auth::id() == 1)
  <div class="form-group row">
    @include('partials.user-list', ['input' => $input, 'userList' => $userList])
  </div>
  @endif

</div>         
<div class="card-body">
  {{ $symbols->appends($input)->links('partials.pagination.default') }}
  
  <table class="table table-hover table-bordered">
    <thead>
      <tr>
        <th>{{ __('Symbol') }}</th>
        <th>{{ __('Setting') }}</th>
        <th>{{ __('Enable') }}</th>
        <th>{{ __('Act') }}</th>
      </tr>
    </thead>
    <tbody>
      @foreach ($symbols as $value)
      <?php 
      $text = $params['symbol'] = $value->symbol;
      if (isset($value->time_frame)) {
        $params['time_frame'] = $value->time_frame;
        $text = $text . ' ' . $timeframes[$value->time_frame];
      }
      ?>      
      <tr>
       <td>
        <span>{{ $text }}</span>
      </td>
      <td>
        <span>{{ $value->base_amount }}  |  {{ $value->base_amount_limit }}</span>
        @if ((!isset($input['user_id']) && Auth::id() == 1) || (isset($input['user_id']) &&  $input['user_id'] == 1))
        <br/>
        <span>{{ $value->setting }}</span>
        @endif
      </td>    
      <td>
        @if ($value->status == 1)
        <i class="far fa-check-circle " style="color:green"></i>
        @else
        <i class="far fa-times-circle" style="color:red"></i>
        @endif
      </td>
      <td>
        @if (isset($value->time_frame))
        @if (isset($input['user_id']))
        @else
        <a href="javascript:void(0)" onclick="delete_symbol({{ $value->id }}, '{{ $value->symbol }}', '{{ $value->time_frame }}')">
          <i class="fas fa-trash"  style="color:black"></i>
        </a>
        @endif
        @else
        <i class="far fa-times-circle" style="color:red"></i>@endif
      </td>              
    </tr>
    @endforeach      
  </tbody>
</table>
{{ $symbols->appends($input)->links('partials.pagination.default') }}
</div>
@endsection

