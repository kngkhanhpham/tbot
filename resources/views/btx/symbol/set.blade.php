@extends('layouts.btx')

@section('content')
<!-- will be used to show any messages -->
@if (Session::has('message'))
<div class="alert alert-info">{{ Session::get('message') }}</div>
@endif

<div class="card-body">
  {!! Form::open(['url' => route('btx.symbol.set'), 'method' => 'post']) !!}

  @if (isset($input['user_id']))
  {!! Form::hidden('user_id', $input['user_id']) !!}
  @endif

  @if (isset($input['base_currency']))
  {!! Form::hidden('base_currency', $input['base_currency']) !!}
  @endif  

  <?php 
    if ($action == 'edit') {
      $partial = 'partials.form-item.edit';
    } else {
      $partial = 'partials.form-item.create';
    }
    $symbols = $fields['symbol']['options'];
  ?>        

  <div class="form-group row">
    <div class="col-md-6 offset-md-4">
      @if (isset($input['base_currency']) && $input['base_currency'] == 'USD')
      USD MARKETS | 
      {!! link_to(route('btx.symbol.set', ['base_currency' => 'BTC']), 'BTC MARKETS', ['class' => '']) !!}
      @else
      {!! link_to(route('btx.symbol.set', ['base_currency' => 'USD']), 'USD MARKETS', ['class' => '']) !!}
      | BTC MARKETS
      @endif
    </div>
  </div>      

  @if (count($fields['symbol']['options']) > 0)

  @foreach ($fields as $key => $field)

  @if($key == 'symbol')

  <div class="form-group row">
    {!! Form::label($key, $field['label'], ['class' => 'col-md-4 col-form-label text-md-right']) !!}

    <div class="col-sm-6 {{ $errors->has($key) ? $field['class'] . ' is-invalid' : '' }}">
      <label style="width: 105px">
        {!! Form::checkbox('all_' . $key , 1, (old($key . '_all') == 1) ? 'true' : '') !!} ALL
      </label>

      @foreach ($fields['symbol']['options'] as $key_s => $val)

      <!-- <div class="checkbox"> -->
        <label style="width: 105px">
          {!! Form::checkbox($key . '[' . $key_s . ']' , 1, (old($key . '[' . $key_s . ']') == 1) ? 'true' : '') !!}
          {!! $val !!}           
        </label>
        <!-- </div> -->
        @endforeach

        @if ($errors->has($key))
        <span class="invalid-feedback" style="display: block;">
          <strong>{{ $errors->first($key) }}</strong>
        </span>
        @endif
      </div>
    </div>

  @else
    @include($partial, ['field' => $field])
  @endif

  @endforeach

@if (Auth::id() == 1)
  <div class="form-group row">
    <div class="col-md-6 offset-md-4">
      {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}  {!! link_to(route('btx.symbol'), 'Back', ['class' => 'btn btn-primary']) !!}
    </div>
  </div>

  <?php 
    $opt = __('btx.options.opt_bb_upper');
    foreach ($opt as $key => $value) {
      $opt['opt_bb_upper_' . $key] = $opt[$key];
      unset($opt[$key]);
    }
  ?>

  @foreach ($opt as $key => $field)
    @include($partial, ['field' => $field])
  @endforeach

  <?php 
    $opt = __('btx.options.opt_bb_lower');
    foreach ($opt as $key => $value) {
      $opt['opt_bb_lower_' . $key] = $opt[$key];
      unset($opt[$key]);
    }
  ?>  

  @foreach ($opt as $key => $field)
    @include($partial, ['field' => $field])
  @endforeach  
@endif

  <div class="form-group row mb-0">
    <div class="col-md-6 offset-md-4">
      {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}  {!! link_to(route('btx.symbol'), 'Back', ['class' => 'btn btn-primary']) !!}
    </div>
  </div>  

  {!! Form::close() !!}
  </div>
  @endif
  @endsection
