@extends('layouts.btx')

@section('content')

@if (Auth::id() == 1)
<div class="card-body"> 
  <div class="form-group row">
    @include('partials.user-list', ['input' => $input, 'userList' => $userList])
  </div>
</div>
@endif

<div class="card-body">
  <span style="color: orange; font-size: 18px;"><strong>Total&nbsp&nbsp&nbsp</strong></span>
  <i class="fab fa-btc" style="color: orange; font-size: 18px;"></i>
  <span style="color: orange; font-size: 18px;"><strong>{{ $totalBTC }}</strong></span>
  &nbsp&nbsp&nbsp
  <i class="fas fa-dollar-sign" style="color: orange; font-size: 18px;"></i>
  <span style="color: orange; font-size: 18px;"><strong>{{ $totalUSD }}</strong></span>
</div>        

<div class="card-body"> 
  <div class="table-responsive-sm">
   {{ $balances->appends($input)->links('partials.pagination.default') }}

   <table class="table table-hover table-bordered">
    <thead>
      <tr>
        <th>{{ __('Symbol') }}</th>
        <th>{{ __('Balance') }}</th>
        <th>{{ __('Est.BTC Value') }}</th>
        <th>{{ __('Pending Deposit') }}</th>
      </tr>
    </thead>

    <tbody>
      @foreach ($balances as $balance)
      <tr>
        <td>
          <strong>{{ $balance['Currency'] }}</strong>
        </td>                
        <td>
          <span style="color: red">{{ $balance['Reserved'] }}</span>
          <br/><span style="color: blue"><strong>{{ $balance['Available'] }}</strong></span>
          <br/><span style="font-size: 18px;"><strong>{{ $balance['Total'] }}</strong></span>
        </td>
        <td>
          <span style="color: red">{{ $balance['Est.BTC']['Reserved'] }}</span>
          <br/><span style="color: blue"><strong>{{ $balance['Est.BTC']['Available'] }}</strong></span>                 
          <br/><span style="font-size: 18px;"><strong>{{ $balance['Est.BTC']['Total'] }}</strong></span>
        </td>     
        <td>
          {{ $balance['Pending'] }}
        </td>             
      </tr>
      @endforeach      
    </tbody>
  </table>

  {{ $balances->appends($input)->links('partials.pagination.default') }}
</div>
</div>
@endsection
