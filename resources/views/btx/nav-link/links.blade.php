<?php
$links = [
    'btx.order-at' => 'Orders',
    'btx.balance' => 'Balances',
    'btx.symbol' => 'Symbols',
    'btx.setting.apikey' => 'ApiKey',
];

if (Auth::id() == 1) {
    $links['btx.setting.symbol'] = 'Symbol Management';
    $links['btx.setting.timeframe'] = 'Timeframe Management';
}
$param = [];
if (isset($input['base_currency'])) {
    $param['base_currency'] = $input['base_currency'];
}

if (isset($input['symbol'])) {
    $param['symbol'] = $input['symbol'];
}

if (isset($input['time_frame'])) {
    $param['time_frame'] = $input['time_frame'];
}
?>

| 
@foreach ($links as $key => $value)
<a href="{{ route($key) }}">{{ $value }}</a> | 
@endforeach

@if (Route::currentRouteName() == 'btx.symbol')
<br> |
{!! link_to(route('btx.symbol.set', $param), 'Set symbols', ['class' => '']) !!} |
{!! link_to('#', 'Set symbols default', ['id'=> 'default_symbols', 'class' => '']) !!} |
@endif