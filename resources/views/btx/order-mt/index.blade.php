@extends('layouts.btx')

@section('content')

<!-- include show message -->
@include('partials.alert')

<div class="card-body">
  <div class="form-group row">
    {!! Form::label('', '', ['class' => 'col-sm-2 col-form-label text-sm-right']) !!}
    <div class="col-sm-2">
    {!! link_to(route('btx.order-mt.edit'), 'New Order', ['class' => 'btn btn-primary']) !!}
    </div>

    <?php
        $key   = 'search_symbol';
        $field = [
            'label'    => 'Symbol',
            'type'     => 'text',
            'class'    => 'form-control',
            'required' => false,
            'default'  => '',
        ];
    ?>
    {!! Form::label($key, $field['label'], ['class' => 'col-sm-2 col-form-label text-sm-right']) !!}

    <div class="col-sm-2">
      {!! Form::text($key, old($key, $input[$key]), ['class' => $errors->has($key) ? $field['class'] . ' is-invalid' : $field['class'], $field['required'] ? 'required' : '', 'id' => $key]) !!}
    </div>
  </div>

  @if (Auth::id() == 1)
  <div class="form-group row">
    @include('partials.user-list', ['input' => $input, 'userList' => $userList])
  </div>
  @endif  

</div>
<div class="card-body">
  <div class="table-responsive-sm">
    {{ $orders->appends($input)->links('partials.pagination.default') }}

    <table class="table table-hover table-bordered">
      <thead>
        <tr>
          <th>{{ __('Date Time') }}</th>
          <th>{{ __('Symbol') }}</th>
          <th>{{ __('Amount') }}<br/></th>
          <th>{{ __('Price') }}</th>
          <th>{{ __('Action') }}</th>
        </tr>
      </thead>

      <tbody>
        @foreach ($orders as $order)
        <tr>
          <td>
           {{ $order->updated_at }}
           <br/>{{ $order->created_at }}
         </td>
         <td>
         {{ substr($order->symbol, 0, 3) }}
         <br/>{{ substr($order->symbol, 4, 10) }}
       </td>
       <td>
         {{ $order->amount }}
       </td>
       <td>
        {{ $order->price }}
      </td>
      <td>
        <p style="word-wrap:break-word; white-space:normal; max-width:200px;">
          @if ($order->status == 1)
          <a href="{{ route('btx.order-mt.cancel', ['id' => $order->id]) }}">
            Cancel
          </a>
          @else
          {{ $order->message }}
          <br/>({{  __('btx.order_status')[$order->status] }})
          @endif
        </p>
      </td>
    </tr>
    @endforeach
  </tbody>
</table>

{{ $orders->appends($input)->links('partials.pagination.default') }}
</div>
</div>
@endsection
