@extends('layouts.btx')

@section('content')

<!-- include show message -->
@include('partials.alert')

<div class="card-body">
  {!! Form::open(['url' => route('btx.order-mt.edit-confirm'), 'method' => 'post']) !!}

  <?php $params = [];?>
  @if (isset($input['user_id']))
  {!! Form::hidden('user_id', $input['user_id']) !!}
  <?php
      $params['user_id'] = $input['user_id'];
  ?>
  @endif

  <?php 
    if ($action == 'edit') {
      $partial = 'partials.form-item.edit';
    } else {
      $partial = 'partials.form-item.create';
    }
  ?>

  @foreach ($fields as $key => $field)
  @include($partial, ['field' => $field])
  @endforeach

  <div class="form-group row mb-0">
    <div class="col-md-6 offset-md-4">
      {!! Form::submit('Confirm', ['class' => 'btn btn-primary']) !!}  {!! link_to(route('btx.order-mt', $params), 'Back', ['class' => 'btn btn-primary']) !!}
    </div>
  </div>
  {!! Form::close() !!}
</div>

@endsection
