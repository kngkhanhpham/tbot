@extends('layouts.btx')

@section('content')

<!-- include show message -->
@include('partials.alert')

<div class="card-body">
  <div class="form-group row mb-0">
    <div class="col-md-6 offset-md-4">
      <span style="color: red; font-size: 18px;">
        Please check confirm your order information before make !
      </span>
    </div>
  </div>
</div>

<div class="card-body">
  {!! Form::open(['url' => route('btx.order-mt.edit-save'), 'method' => 'post']) !!}

  @foreach ($input as $key => $value)
    <?php
        $params[$key] = $value;
    ?>
    {!! Form::hidden($key, $value) !!}
  @endforeach

  <?php
      $partial = 'partials.form-item.edit';
  ?>

  @foreach ($fields as $key => $field)
  @include($partial, ['field' => $field])
  @endforeach

  <?php
      $params = [];
      unset($input['_token']);
  ?>
    

  <div class="form-group row mb-0">
    <div class="col-md-6 offset-md-4">
      {!! Form::submit('Make', ['class' => 'btn btn-primary']) !!}  {!! link_to(route('btx.order-mt.edit', $params), 'Back', ['class' => 'btn btn-primary']) !!}
    </div>
  </div>
  {!! Form::close() !!}
</div>

@endsection
