@extends('layouts.btx')

@section('content')

<!-- include show message -->
@include('partials.alert')

<div class="card-body">
  {!! Form::open(['url' => route('btx.setting.timeframe'), 'method' => 'post']) !!}

  <div class="form-group row">
    <?php $key = 'timeframes' ?>
    {!! Form::label($key, 'Time Frame', ['class' => 'col-md-4 col-form-label text-md-right']) !!}

    <div class="col-sm-6 {{ $errors->has($key) ? $field['class'] . ' is-invalid' : '' }}">

      @foreach ($timeframes as $val)
      <!-- <div class="checkbox"> -->
        <label style="width: 105px">
          <?php $key_t = $val->time_frame; ?>
          {!! Form::checkbox($key . '[' . $key_t . ']' , 1, (old($key . '[' . $key_t . ']', $val->status) == 1) ? 'true' : '') !!}
          {!! $val->time_frame_name !!}           
        </label>
        <!-- </div> -->
        @endforeach

        @if ($errors->has($key))
        <span class="invalid-feedback" style="display: block;">
          <strong>{{ $errors->first($key) }}</strong>
        </span>
        @endif
      </div>
    </div>

    <div class="form-group row mb-0">
      <div class="col-md-6 offset-md-4">
        {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}  {!! link_to(route('btx'), 'Back', ['class' => 'btn btn-primary']) !!}
      </div>
    </div>
    {!! Form::close() !!}
  </div>
  @endsection
