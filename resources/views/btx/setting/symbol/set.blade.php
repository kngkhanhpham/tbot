@extends('layouts.btx')

@section('content')

<!-- include show message -->
@include('partials.alert')

<div class="card-body">
  {!! Form::open(['url' => route('btx.setting.symbol'), 'method' => 'post']) !!}

  @if (isset($input['base_currency']))
  {!! Form::hidden('base_currency', $input['base_currency']) !!}
  @endif 

  <div class="form-group row">
    <div class="col-md-6 offset-md-4">
      @if (isset($input['base_currency']) && $input['base_currency'] == 'USD')
      USD MARKETS | 
      {!! link_to(route('btx.setting.symbol', ['base_currency' => 'BTC']), 'BTC MARKETS', ['class' => '']) !!}
      @else
      {!! link_to(route('btx.setting.symbol', ['base_currency' => 'USD']), 'USD MARKETS', ['class' => '']) !!}
      | BTC MARKETS
      @endif
    </div>
  </div>

  <div class="form-group row">
    <?php $key = 'symbol' ?>
    {!! Form::label($key, 'Symbols', ['class' => 'col-md-4 col-form-label text-md-right']) !!}

    <div class="col-sm-6 {{ $errors->has($key) ? $field['class'] . ' is-invalid' : '' }}">
      <label style="width: 105px">
        {!! Form::checkbox('all_' . $key , 1, (old($key . '_all') == 1) ? 'true' : '') !!} ALL
      </label>      

      @foreach ($symbols as $val)
      <!-- <div class="checkbox"> -->
        <label style="width: 105px">
          <?php $key_t = $val->symbol; ?>
          {!! Form::checkbox($key . '[' . $key_t . ']' , 1, (old($key . '[' . $key_t . ']', $val->status) == 1) ? 'true' : '') !!}
          {!! $val->symbol !!}           
        </label>
        <!-- </div> -->
        @endforeach

        @if ($errors->has($key))
        <span class="invalid-feedback" style="display: block;">
          <strong>{{ $errors->first($key) }}</strong>
        </span>
        @endif
      </div>
    </div>

    <div class="form-group row mb-0">
      <div class="col-md-6 offset-md-4">
        {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}  {!! link_to(route('btx'), 'Back', ['class' => 'btn btn-primary']) !!}
      </div>
    </div>
    {!! Form::close() !!}
  </div>
  @endsection
