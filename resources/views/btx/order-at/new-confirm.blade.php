@extends('layouts.btx')

@section('content')

<!-- include show message -->
@include('partials.alert')

<div class="card-body">
  <div class="form-group row mb-0">
    <div class="col-md-6 offset-md-4">
      <span style="font-size: 18px;">
        Please check confirm your order information before make !
      </span>
    </div>
    <div class="col-md-6 offset-md-4">
      <?php 
        $sData = $input;
        $cl = 'red';
        if ($sData['side'] == 1) {
          $cl = 'green';
        }
        $sData['side'] = $fields['side']['options'][$sData['side']];
      ?>

      <br/><span style="color: {!! $cl !!};"> User : {!! $userList[$sData['user_id']] !!} </span>

      @foreach ($fields as $key => $field)
      <br/><span style="color: {!! $cl !!};"> {!! $field['label'] !!} : {!! $sData[$key] !!} </span>
      @endforeach
      <br/><span style="color: {!! $cl !!};"> Total : {!! round($sData['amount'] * $sData['price'], 8) !!} </span>
    </div>
   
  </div>
</div>

<div class="card-body">
  {!! Form::open(['url' => route('btx.order-at.new-save'), 'method' => 'post']) !!}

  @foreach ($input as $key => $value)
    <?php
        $params[$key] = $value;
    ?>
    {!! Form::hidden($key, $value) !!}
  @endforeach

  <?php
      unset($params['_token']);
      unset($input['_token']);
  ?>
    

  <div class="form-group row mb-0">
    <div class="col-md-6 offset-md-4">
      {!! Form::submit('Make', ['class' => 'btn btn-primary']) !!}  {!! link_to(route('btx.order-at.new', $params), 'Back', ['class' => 'btn btn-primary']) !!}
    </div>
  </div>
  {!! Form::close() !!}
</div>

@endsection
