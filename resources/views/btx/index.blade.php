@extends('layouts.btx')

@section('content')

<div class="card-body"> 
  <div class="form-group row">
    @include('partials.date-filter', ['input' => $input])
  </div>

  @if (Auth::id() == 1)
    <div class="form-group row">
      @include('partials.user-list', ['input' => $input, 'userList' => $userList])
    </div>
  @endif  
</div>

<?php 
$total_color = 'green';
if ($result['USD']['total_profit'] < 0) {
  $total_color = 'red';
}
?>
<div class="card-body">
  <span style="color: black; font-size: 18px;"><strong>USD | Buy : {{ $result['USD']['total_buy'] }} | </strong></span>&nbsp
  <span style="color: black; font-size: 18px;"><strong>Sell : {{ $result['USD']['total_sell'] }} | </strong></span>&nbsp
  <span style="color: black; font-size: 18px;"><strong>Profit : </strong><strong style="color: {{ $total_color }}; font-size: 18px;">{{ number_format($result['USD']['total_profit'], 8, '.', '') }} | </strong></span>&nbsp
  <span style="color: black; font-size: 18px;"><strong>Profit Rate : </strong><strong style="color: {{ $total_color }}; font-size: 18px;">{{ $result['USD']['total_profit_rate'] }}%</strong></span>&nbsp   
  <br>
  <span style="color: black; font-size: 18px;"><strong>BTC | Buy : {{ $result['BTC']['total_buy'] }} | </strong></span>&nbsp
  <span style="color: black; font-size: 18px;"><strong>Sell : {{ $result['BTC']['total_sell'] }} | </strong></span>&nbsp
  <span style="color: black; font-size: 18px;"><strong>Profit : </strong><strong style="color: {{ $total_color }}; font-size: 18px;">{{ number_format($result['BTC']['total_profit'], 8, '.', '') }} | </strong></span>&nbsp
  <span style="color: black; font-size: 18px;"><strong>Profit Rate : </strong><strong style="color: {{ $total_color }}; font-size: 18px;">{{ $result['BTC']['total_profit_rate'] }}%</strong></span>&nbsp   
</div>

<div class="card-body"> 
  <div class="table-responsive-sm">

   <table class="table table-hover table-bordered">
    <thead>
      <tr>
        <th>{{ __('Symbol') }}</th>
        <th>{{ __('Buy') }}</th>
        <th>{{ __('Sell') }}</th>
        <th>{{ __('Profit') }}</th>
        <th>{{ __('Profit Rate') }}</th>
      </tr>
    </thead>

    <tbody>
      @foreach ($result['USD']['items'] as $symbol => $value)
      <?php 
      $sub_total_color = 'green';
      if ($value['sub_profit'] < 0) {
        $sub_total_color = 'red';
      }
      ?>
      <tr>
        <td>
          {{ $symbol }}
        </td>                
        <td>
        {{ number_format($value['sub_total_buy'], 8, '.', '') }}
        </td>
        <td>
        {{ number_format($value['sub_total_sell'], 8, '.', '') }}
        </td>
        <td>
        <span style="color: {{ $sub_total_color }}; font-size: 16px;">{{ number_format($value['sub_profit'], 8, '.', '') }}</span>
        </td>
        <td>
        <span style="color: {{ $sub_total_color }}; font-size: 16px;">{{ round($value['sub_profit_rate'], 2) }}%</span>
        </td>             
      </tr>
      @endforeach    

      @foreach ($result['BTC']['items'] as $symbol => $value)
      <?php 
      $sub_total_color = 'green';
      if ($value['sub_profit'] < 0) {
        $sub_total_color = 'red';
      }
      ?>
      <tr>
        <td>
          {{ $symbol }}
        </td>                
        <td>
        {{ number_format($value['sub_total_buy'], 8, '.', '') }}
        </td>
        <td>
        {{ number_format($value['sub_total_sell'], 8, '.', '') }}
        </td>
        <td>
        <span style="color: {{ $sub_total_color }}; font-size: 16px;">{{ number_format($value['sub_profit'], 8, '.', '') }}</span>
        </td>
        <td>
        <span style="color: {{ $sub_total_color }}; font-size: 16px;">{{ round($value['sub_profit_rate'], 2) }}%</span>
        </td>             
      </tr>
      @endforeach   

    </tbody>
  </table>

</div>
</div>
@endsection
