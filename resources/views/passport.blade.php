@extends('layouts.app')

@section('content')

<!-- Axios -->
<!-- <script src="https://unpkg.com/axios/dist/axios.min.js"></script> -->
<!-- <script>
  axios.defaults.headers.common = {
    'X-Requested-With': 'XMLHttpRequest',
    'X-CSRF-TOKEN': document.head.querySelector('meta[name="csrf-token"]').content,
  };     

  axios.get('/api/user')
  .then(response => {
    console.log('use Axios');
    console.log(response.data);
  });     
</script> -->

<div class="container">
  <div class="row justify-content-center">
    <div class="col-md-8">
      <passport-clients></passport-clients>
      <passport-authorized-clients></passport-authorized-clients>
      <passport-personal-access-tokens></passport-personal-access-tokens>
    </div>
  </div>
</div>

@endsection
