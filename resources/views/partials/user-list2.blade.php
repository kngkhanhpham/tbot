<?php
    $key   = 'user_id';
    $field = [
        'label'    => 'User',
        'type'     => 'select',
        'options'  => $userList,
        'class'    => 'form-control',
        'required' => false,
        'default'  => '',
    ];
?>
{!! Form::label($key, $field['label'], ['class' => 'col-md-4 col-form-label text-md-right']) !!}
<div class="col-md-6">
  {!! Form::select($key, $field['options'], old($key, $input[$key]), ['class' => $errors->has($key) ? $field['class'] . ' is-invalid' : $field['class'], $field['required'] ? 'required' : '', 'id' => $key]) !!}
</div>
