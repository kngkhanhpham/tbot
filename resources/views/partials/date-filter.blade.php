<?php
    $key   = 'year';
    $cYear = date('Y');
    $options = ['' => '----'];
    for ($i = $cYear; $i > $cYear - 5 ; $i --) { 
        $options[$i] = $i;
    }

    $field = [
        'label'    => 'Filter',
        'type'     => 'select',
        'options'  => $options,
        'class'    => 'form-control',
        'required' => false,
        'default'  => '',
    ];
?>
{!! Form::label($key, $field['label'], ['class' => 'col-sm-2 col-form-label text-sm-right']) !!}
<div class="col-sm-2">
  {!! Form::select($key, $field['options'], old($key, $input[$key]), ['class' => $errors->has($key) ? $field['class'] . ' is-invalid' : $field['class'], $field['required'] ? 'required' : '', 'id' => $key]) !!}
</div>

<?php
    $key   = 'month';
    $options = ['' => '----'];
    for ($i = 1; $i <= 12 ; $i ++) { 
        $options[$i] = $i;
    }

    $field = [
        'label'    => '',
        'type'     => 'select',
        'options'  => $options,
        'class'    => 'form-control',
        'required' => false,
        'default'  => '',
    ];
?>
<!-- {!! Form::label($key, $field['label'], ['class' => 'col-sm-2 col-form-label text-sm-right']) !!} -->
<div class="col-sm-2">
  {!! Form::select($key, $field['options'], old($key, $input[$key]), ['class' => $errors->has($key) ? $field['class'] . ' is-invalid' : $field['class'], $field['required'] ? 'required' : '', 'id' => $key]) !!}
</div>


<?php
    $key   = 'day';
    $options = ['' => '----'];
    for ($i = 1; $i <= 31 ; $i ++) { 
        $options[$i] = $i;
    }

    $field = [
        'label'    => '',
        'type'     => 'select',
        'options'  => $options,
        'class'    => 'form-control',
        'required' => false,
        'default'  => '',
    ];
?>
<!-- {!! Form::label($key, $field['label'], ['class' => 'col-sm-2 col-form-label text-sm-right']) !!} -->
<div class="col-sm-2">
  {!! Form::select($key, $field['options'], old($key, $input[$key]), ['class' => $errors->has($key) ? $field['class'] . ' is-invalid' : $field['class'], $field['required'] ? 'required' : '', 'id' => $key]) !!}
</div>