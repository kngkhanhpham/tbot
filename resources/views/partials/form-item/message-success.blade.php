@if (Session::has('message'))
<div class="alert alert-info">{{ Session::get('message') }}</div>
<?php Session::remove('message');?>
@endif

@if (Session::has('error-message'))
<div class="alert alert-danger">{{ Session::get('error-message') }}</div>
<?php Session::remove('error-message');?>
@endif