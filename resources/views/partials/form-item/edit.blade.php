@switch($field['type'])

@case('text')
<div class="form-group row">

  {!! Form::label($key, $field['label'], ['class' => 'col-md-4 col-form-label text-md-right']) !!}

  <div class="col-md-6">
    {!! Form::text($key, old($key, $data->$key), ['class' => $errors->has($key) ? $field['class'] . ' is-invalid' : $field['class'], $field['required'] ? 'required' : '', isset($field['readonly']) && $field['readonly'] ? 'readonly' : '', 'id' => $key]) !!}

    @if ($errors->has($key))
    <span class="invalid-feedback">
      <strong>{{ $errors->first($key) }}</strong>
    </span>
    @endif
  </div>
  @if (isset($field['unit']))
  <div class="col-form-label text-md-right">{{$field['unit']}}</div>
  @endif
</div>
@break

@case('textarea')
<div class="form-group row">
  {!! Form::label($key, $field['label'], ['class' => 'col-md-4 col-form-label text-md-right']) !!}

  <div class="col-md-6">
    {!! Form::textarea($key, old($key, $data->$key), ['class' => $errors->has($key) ? $field['class'] . ' is-invalid' : $field['class'], 'rows' => $field['rows'], $field['required'] ? 'required' : '']) !!}

    @if ($errors->has($key))
    <span class="invalid-feedback">
      <strong>{{ $errors->first($key) }}</strong>
    </span>
    @endif
  </div>
</div>
@break

@case('select')
<?php 
// print_r($field['options']);die;
?> 
<div class="form-group row">
  {!! Form::label($key, $field['label'], ['class' => 'col-md-4 col-form-label text-md-right']) !!}

  <div class="col-md-6">
    {!! Form::select($key, $field['options'], old($key, $data->$key), ['class' => $errors->has($key) ? $field['class'] . ' is-invalid' : $field['class'], $field['required'] ? 'required' : '']) !!}

    @if ($errors->has($key))
    <span class="invalid-feedback">
      <strong>{{ $errors->first($key) }}</strong>
    </span>
    @endif
  </div>
</div>
@break

@case('checkbox')
<div class="form-group row">
  {!! Form::label($key, $field['label'], ['class' => 'col-md-4 col-form-label text-md-right']) !!}

  <div class="col-md-4">
    <div class="checkbox">
      <label>
        {!! Form::checkbox($key, 1, (old($key, $data->$key) == 1) ? 'true' : '') !!}
      </label>
    </div>

    @if ($errors->has($key))
    <span class="invalid-feedback">
      <strong>{{ $errors->first($key) }}</strong>
    </span>
    @endif
  </div>
</div>
@break

@case('radio')

@break

@default

@endswitch