<?php
    $key   = 'user_id';
    $field = [
        'label'    => 'User',
        'type'     => 'select',
        'options'  => $userList,
        'class'    => 'form-control',
        'required' => false,
        'default'  => '',
    ];
?>
{!! Form::label($key, $field['label'], ['class' => 'col-sm-2 col-form-label text-sm-right']) !!}
<div class="col-sm-4">
  {!! Form::select($key, $field['options'], old($key, $input[$key]), ['class' => $errors->has($key) ? $field['class'] . ' is-invalid' : $field['class'], $field['required'] ? 'required' : '', 'id' => $key]) !!}
</div>
