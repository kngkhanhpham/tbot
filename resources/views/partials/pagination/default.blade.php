@if ($paginator->hasPages())
    <ul class="pagination" role="navigation">
        {{-- Previous Page Link --}}
        @if ($paginator->onFirstPage())
            <li class="page-item disabled" aria-disabled="true" aria-label="@lang('pagination.previous')">
                <span class="page-link" aria-hidden="true">&lsaquo;&lsaquo;</span>
            </li>
            <li class="page-item disabled" aria-disabled="true" aria-label="@lang('pagination.previous')">
                <span class="page-link" aria-hidden="true">&lsaquo;</span>
            </li>            
        @else
            <li class="page-item">
                <a class="page-link" href="{{  $paginator->toArray()['first_page_url'] }}" rel="prev" aria-label="@lang('pagination.previous')">&lsaquo;&lsaquo;</a>
            </li>
            <li class="page-item">
                <a class="page-link" href="{{ $paginator->previousPageUrl() }}" rel="prev" aria-label="@lang('pagination.previous')">&lsaquo;</a>
            </li>
        @endif

        <?php
        $link_limit = 5;
        $half_total_links = floor($link_limit / 2);
        $from = ($paginator->currentPage() - $half_total_links) < 1 ? 1 : $paginator->currentPage() - $half_total_links;
        $to = ($paginator->currentPage() + $half_total_links) > $paginator->lastPage() ? $paginator->lastPage() : ($paginator->currentPage() + $half_total_links);
        if ($from > $paginator->lastPage() - $link_limit) {
           $from = ($paginator->lastPage() - $link_limit) + 1;
           $to = $paginator->lastPage();
        }
        if ($to <= $link_limit) {
            $from = 1;
            $to = $link_limit < $paginator->lastPage() ? $link_limit : $paginator->lastPage();
        }
        ?>
        
        @for ($page = $from; $page <= $to; $page++)
            @if ($page == $paginator->currentPage())
                <li class="page-item active" aria-current="page"><span class="page-link">{{ $page }}</span></li>
            @else
                <li class="page-item"><a class="page-link" href="{{ $paginator->url($page) }}">{{ $page }}</a></li>
            @endif                
        @endfor      


        {{-- Next Page Link --}}
        @if ($paginator->hasMorePages())
            <li class="page-item">
                <a class="page-link" href="{{ $paginator->nextPageUrl() }}" rel="next" aria-label="@lang('pagination.next')">&rsaquo;</a>
            </li>
            <li class="page-item">
                <a class="page-link" href="{{ $paginator->toArray()['last_page_url'] }}" rel="next" aria-label="@lang('pagination.next')">&rsaquo;&rsaquo;</a>
            </li>            
        @else
            <li class="page-item disabled" aria-disabled="true" aria-label="@lang('pagination.next')">
                <span class="page-link" aria-hidden="true">&rsaquo;</span>
            </li>
            <li class="page-item disabled" aria-disabled="true" aria-label="@lang('pagination.next')">
                <span class="page-link" aria-hidden="true">&rsaquo;&rsaquo;</span>
            </li>            
        @endif
    </ul>
@endif
