@extends('layouts.app')

@section('main_content')
<div class="container">
  <div class="row justify-content-center">
    <div class="col-md-8">
      <div class="card">
        <div class="card-header">Exchanges</div>
        <div class="card-body">
          <ul>
          @foreach ($exchanges as $key => $value)
            <li>
              <a href="{{ route($key) }}"><img src="{!! $value['logo'] !!}"></a> 
              @if ($value['status'] == 1)
              <i class="far fa-check-circle " style="color:green"></i>
              @else
              <i class="far fa-times-circle" style="color:red"></i>
              @endif
            </li>
          @endforeach
          </ul>
      </div>
    </div>
  </div>
</div>
</div>
@endsection

