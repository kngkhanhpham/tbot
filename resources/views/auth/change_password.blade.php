@extends('layouts.app')

@section('main_content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">{{ __('Change Password') }}</div>

                @if(session('success'))
                <div class="alert alert-success">
                    {{ session('success') }}
                </div>
                @else
                <div class="card-body">
                    {!! Form::open(['method' => 'PATCH', 'route' => ['auth.change_password']]) !!}
                    @csrf

                    @php $key = 'current_password'; @endphp
                    <div class="form-group row">

                      {!! Form::label($key, __('Current Password'), ['class' => 'col-md-4 col-form-label text-md-right']) !!}

                      <div class="col-md-6">
                        {!! Form::password($key, ['class' => $errors->has($key) ? 'form-control is-invalid' : 'form-control', 'required']) !!}

                        @if ($errors->has($key))
                        <span class="invalid-feedback">
                          <strong>{{ $errors->first($key) }}</strong>
                      </span>
                      @endif
                  </div>
              </div>           

              @php $key = 'new_password'; @endphp
              <div class="form-group row">

                  {!! Form::label($key, __('New Password'), ['class' => 'col-md-4 col-form-label text-md-right']) !!}

                  <div class="col-md-6">
                    {!! Form::password($key, ['class' => $errors->has($key) ? 'form-control is-invalid' : 'form-control', 'required']) !!}

                    @if ($errors->has($key))
                    <span class="invalid-feedback">
                      <strong>{{ $errors->first($key) }}</strong>
                  </span>
                  @endif
              </div>
          </div>   

          @php $key = 'new_password_confirmation'; @endphp
          <div class="form-group row">

              {!! Form::label($key, __('New Password Confirm'), ['class' => 'col-md-4 col-form-label text-md-right']) !!}

              <div class="col-md-6">
                {!! Form::password($key, ['class' => $errors->has($key) ? 'form-control is-invalid' : 'form-control', 'required']) !!}

                @if ($errors->has($key))
                <span class="invalid-feedback">
                  <strong>{{ $errors->first($key) }}</strong>
              </span>
              @endif
          </div>
      </div>                                                                        

      <div class="form-group row mb-0">
        <div class="col-md-6 offset-md-4">
            {!! Form::submit(__('Save'), ['class' => 'btn btn-primary']) !!}
        </div>
    </div>
    {!! Form::close() !!}
</div>
@endif            
</div>
</div>
</div>
</div>
@endsection
