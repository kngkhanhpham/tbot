<?php

return [

    'list' => [
        'bfx' => [
            'name' => 'BITFINEX', 
            'logo' => '/img/bfx-logo.svg',
            'status' => '2',
        ],
        'btx' => [
            'name' => 'BITTREX', 
            'logo' => '/img/btx-logo.svg',
            'status' => '2',
        ],
    ],

];
